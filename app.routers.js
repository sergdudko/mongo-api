/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  APP.ROUTES
 *	Приложение express маршрутизирует /v3/ по соответствующим маршрутам
 *
 */

"use strict"

//подгружаемые библиотеки
var EXPRESS = require('express'),
	CONFIG = require('config'),
	ZLIB = require('zlib'),
	PATH = require('path'),
	MARKDOWN = require('markdown-into-html');

//подгружаемые модули
var STATUS = require(PATH.join(__dirname, 'app.routers.status.js')),
	QUERY = require(PATH.join(__dirname, 'app.routers.query.js')).query,
	BIGDATA = require(PATH.join(__dirname, 'app.routers.query.js')).bigdata,
	DATA = require(PATH.join(__dirname, 'app.routers.data.js')),
	ECHO = require(PATH.join(__dirname, 'app.routers.echo.js')),
	RECIPE = require(PATH.join(__dirname, 'app.routers.recipe.js')),
	IPTOBAN = require(PATH.join(__dirname, 'app.iptoban.js')),
	LOGGER = require(PATH.join(__dirname, 'module.logger.js'));

var v3router = EXPRESS.Router();

if(CONFIG.bantimeout)
	v3router.use('*', IPTOBAN.authall);

v3router.use(['/v3/query', '/v3/bigdata'], EXPRESS.json({limit:'1mb'}));	//для query парсим json, для data у нас будет свой потоковый парсер

v3router.use(function(req, res, next){
	//отключение коннектов и стримов
	res.locals.closer = function(){
		try{
			if(res.locals.gzipper){
				res.locals.gzipper.end();
			}
		} catch(err){
			LOGGER.error("Ошибка отключения gzip-stream:" + err);
		}
	}
	//проверка поддержки gzip
	res.locals.acceptgzip = false;
	if(typeof(req.get('accept-encoding')) === 'string'){
		let array = req.get('accept-encoding').replace(/ /g, "").toLowerCase().split(",");
		if(array.indexOf('gzip') !== -1){
			res.locals.acceptgzip = true;
		}
	}
	res.locals.gzippedbody = false;
	if((typeof(req.get('encoding')) === 'string') && (req.get('encoding').replace(/ /gi, "").toLowerCase() === 'gzip')){
		res.locals.gzippedbody = true;
	}
	return next();
});

v3router.use(['/v3/recipe', '/v3/data', '/v1/data', '/v1/recipe', '/v3/echo'], function(req, res, next){	//добавляю поток сжатия для всего, кроме query, bigdata
	if(res.locals.acceptgzip){	//добавляю в res.locals.gzipper поток сжатия
		res.locals.gzipper = ZLIB.createGzip();	//поток сжатия
		res.locals.gzipper.on('error', function(err){
			LOGGER.error("Ошибка обработки gzipper:" + err);
		});
		res.locals.gzipper.pipe(res);
	}
	return next();
});

v3router.all('/v3/query', QUERY); //маршрутизируем query (чтение)
v3router.all('/v3/status', STATUS); //маршрутизируем status
v3router.all(['/v3/data', '/v1/data'], DATA); //маршрутизируем data (запись/удаление)
v3router.all(['/v3/recipe', '/v1/recipe'], RECIPE); //маршрутизируем data (чтение)
v3router.all('/v3/bigdata', BIGDATA); //маршрутизируем aggregation запросы (чтение)
v3router.all('/v3/echo', ECHO); //маршрутизируем эхо-сервер

v3router.all('*', function(req, res){ //маршрутизируем остальные
	return res.status(404).json({status:"error", message:'Not Found'});
});

var readme = EXPRESS.Router();

readme.all('*', function(req, res){
	//проверка поддержки gzip
	let acceptgzip = false;
	if(typeof(req.get('accept-encoding')) === 'string'){
		let array = req.get('accept-encoding').replace(/ /g, "").toLowerCase().split(",");
		if(array.indexOf('gzip') !== -1){
			acceptgzip = true;
		}
	}
	if(acceptgzip){	//добавляю в res.locals.gzipper поток сжатия
		res.locals.gzipper = ZLIB.createGzip();	//поток сжатия
		res.locals.gzipper.pipe(res);
	}
	MARKDOWN({
		path: PATH.join(__dirname, 'README.md')
	}).then(function(html){
		res.append('Content-Type', 'text/html; charset=utf-8');
		if(acceptgzip){
			res.append('Content-Encoding','gzip');
			return res.locals.gzipper.end(html);
		} else {
			return res.end(html);
		}
	}).catch(function(err){
		LOGGER.warn(err);
		return res.status(500).json({status:"error", message:err.message});
	});
});

module.exports.v3router = v3router;
module.exports.readme = readme;
