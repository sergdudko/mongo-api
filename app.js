/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  APP
 *	Приложение express маршрутизирует /v3/* в routers
 *
 */

"use strict"

//подгружаемые библиотеки
var EXPRESS = require('express'),
	FAVICON = require('serve-favicon'),
	CONFIG = require('config'),
	PATH = require('path');
	
//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js')),
	IPTOBAN = require(PATH.join(__dirname, 'app.iptoban.js')),
	ROUTERS = require(PATH.join(__dirname, 'app.routers.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js'));

var app = EXPRESS();

app.use(function (req, res, next) {
  res.setHeader( 'X-Powered-By', 'iRetailCloudServer v'+PROCSTORE.getState().version );
  next();
});

app.use(FAVICON(PATH.join(__dirname, 'public', 'favicon.png')));
app.use(function(req, res, next){
	(new Promise(function (resolve){
		res.locals.mongodb = app.locals.mongodb;	//для передачи в роутеры даю ссылки на соединения
		res.locals.redis = app.locals.redis;
		res.locals.timesectemp = Date.now();
		if(typeof(req.ips) === 'string'){
			res.locals.ip = req.ips;
		} else if((CONFIG.trustedproxy) && (CONFIG.trustedproxy === req.ip) && (req.headers['x-real-ip'])){
			res.locals.ip = req.headers['x-real-ip'];
		} else{
			res.locals.ip = req.ip;
		}
		res.locals.loggerstr = FUNCTIONS.correcterstr(res.locals.ip, 15) +  //для проброса ip через NAT в хидере использовать req.ips
				" | " + FUNCTIONS.correcterstr(req.method, 7) + 
				" | " + FUNCTIONS.correcterstr(req.originalUrl, 40);
		res.locals.logger = function(){
			let _timesecint = ((Date.now() - res.locals.timesectemp) / 1000);
			let timesec = _timesecint + "s";
			res.locals.loggerstr = res.locals.loggerstr + 
				" | " + FUNCTIONS.correcterstr(timesec, 10) + 
				" | " + FUNCTIONS.correcterstr(res.statusCode, 3);
			if(res.locals.cachel){
				res.locals.loggerstr = res.locals.loggerstr +
					" | " + FUNCTIONS.correcterstr(res.locals.cachel, 4);
			} else {
				res.locals.loggerstr = res.locals.loggerstr +
					" | " + FUNCTIONS.correcterstr('', 4);
			}
			if(res.locals.uid){
				res.locals.loggerstr = res.locals.loggerstr +
					" | " + FUNCTIONS.correcterstr(res.locals.uid, 40)
			}
			LOGGER.http(res.locals.loggerstr);
			if((typeof(req.params) === 'object') && (typeof(CONFIG.slowrequest) === 'object')){	//отправка медленных запросов в поток ошибок
				switch(req.params[0]){
					case '/v3/query':
						if(Number.isInteger(CONFIG.slowrequest.v3query) && (_timesecint > CONFIG.slowrequest.v3query)){
							let _body = "";
							try{
								if(typeof(req.body) === 'object'){
									_body = JSON.stringify(req.body);
								} else if(typeof(req.body) === 'string'){
									_body = req.body;
								}
							} catch(e){
								LOGGER.warn(e);
							} finally {
								LOGGER.error('Запрос к '+req.url+'\nс телом '+_body+'\nвыполнялся '+_timesecint+'sec.');
							}
						}
						break;
					case '/v3/bigdata':
						if(Number.isInteger(CONFIG.slowrequest.v3bigdata) && (_timesecint > CONFIG.slowrequest.v3bigdata)){
							let _body = "";
							try{
								if(typeof(req.body) === 'object'){
									_body = JSON.stringify(req.body);
								} else if(typeof(req.body) === 'string'){
									_body = req.body;
								}
							} catch(e){
								LOGGER.warn(e);
							} finally {
								LOGGER.error('Запрос к '+req.url+'\nс телом '+_body+'\nвыполнялся '+_timesecint+'sec.');
							}
						}
						break;
					case '/v1/recipe':
					case '/v3/recipe':
						if(Number.isInteger(CONFIG.slowrequest.v3recipe) && (_timesecint > CONFIG.slowrequest.v3recipe)){
							LOGGER.error('Запрос к '+req.url+'\nвыполнялся '+_timesecint+'sec.');
						}
						break;
					case '/v1/data':
					case '/v3/data':
						if(Number.isInteger(CONFIG.slowrequest.v3data) && (_timesecint > CONFIG.slowrequest.v3data)){
							LOGGER.error('Запрос к '+req.url+'\nвыполнялся '+_timesecint+'sec.');
						}
						break;
				}
			}
		}
		return resolve('next');
	})).then(function(){
		return next();
	}).catch(function(error){
		LOGGER.error('Ошибка работы APP: '+error);
		res.status(500).json({status:"error", message:'Internal Server Error'});
		if(typeof(res.locals.logger) === 'function'){
			res.locals.logger();
		}
		return;
	});
});

app.use('*', function(req, res, next){ //закрываю соединения и вывожу лог, если res финиширует или выпадает в ошибку
	res.on('finish', function(){
		if(res.locals.closer){
			res.locals.closer();
		}
		res.locals.logger();
	});
	res.on('error', function(){
		if(res.locals.closer){
			res.locals.closer();
		}
		res.locals.logger();
	});
	res.locals.enableheader = true;
	return next();
});

app.use('*', IPTOBAN.test);

app.all(['/v3/*', '/v1/*'], ROUTERS.v3router);
app.all('/readme', ROUTERS.readme);		//readme.md to html

app.use(EXPRESS.static('public'));	//директория веб-сайта (статические данные)

app.all('*', function(req, res){ //маршрутизируем остальные
	res.status(404).json({status:"error", message:'Not Found'});
});

app.use(function(err, req, res) { //обработчик ошибок
	LOGGER.warn(err);
	return res.status(500).json({status:"error", message:err.message});
});

module.exports = app;