var HULKCORE = require('@sergdudko/hulk').core,
	LODASH = require('lodash');
//HULKCORE(set_you_link, set_this_data, set_this_method, set_req_total, set_req_in_min, stdout);

//настройка 
var speed = 3000, //запросов в минуту
time = 2;	//минут на тест * количество объектов в массиве links

//начальные данные
var i = 0;
var url = 'http://test:test@10.0.8.1:9999';
var links = [
		{
			'type':'POST',
			'link':'/v3/query',
			'data':{"find":{"type":{"$eq":"Справочник.ДанныеПоСтраховымПолисам"}},"limit":10000, "skip":0}},
		{
			'type':'GET',
			'link':'/v3/query',
			'data':{"find":{"type":{"$eq":"Справочник.ВидыНоменклатуры"}},"limit":1, "skip":0}},
		{
			'type':'GET',
			'link':'/v3/status',
			'data':''
		},
		{
			'type':'GET',
			'link':'/v2/',
			'data':''
		}, 
		{
			'type':'GET',
			'link':'/v3/recipe?id=9112420000000022',
			'data':''
		} 
	];
var summary = {};	
//суммирование
function summ(object){
	var clearobject = JSON.parse(JSON.stringify(object));
	for(const key in clearobject){
		if(typeof(summary[key]) === 'undefined'){
			summary[key] = clearobject[key];
		} else {
			summary[key] = summary[key] + clearobject[key];
		}
	}
}		
function StageOne(){
	if(i !== 0){
		console.log(links[i-1]);
		console.log('Результаты: '+JSON.stringify(summary));
	} else {
		console.log('Старт тестирования');
	}
	summary = {};
	if(i < links.length){
		//запускаем в 100 потоков c разными запросами
		let ctrl = 0;
		const fin = 100;
		for(let n = 0; n < fin; n++){
			setTimeout(function(val){
				let _data = LODASH.clone(links[i]);
				let l;
				switch((_data.link).split('?')[0]){	//смещение
					case '/v3/query':
						_data.data.skip = val;
						_data.data = JSON.stringify(_data.data);
						break;
					case '/v3/recipe':
						l = val.toString();
						_data.link = _data.link.substr(0,_data.link.length-l.length)+l;
						break;
				}
				HULKCORE(url+_data.link, _data.data, _data.type, parseInt(time*speed/100), parseInt(speed/100), function(){}).then(result=>{
					summ(result);
					ctrl++;
					if(ctrl === fin){i++;StageOne();}
				},err=>{
					console.log(err);
					ctrl++;
					if(ctrl === fin){i++;StageOne();}
				});
			}, n*100, n); //разнесем запуск
		}
	} else {
		exit();
	}
}

//конец тестирования
function exit(){
	console.log('Тестирование завершено');
	process.exit(0);
}

StageOne();
