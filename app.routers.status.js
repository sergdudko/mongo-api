/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  APP.ROUTERS.STATUS
 *	Маршрут для /v3/status
 *
 */

"use strict"

//подгружаемые библиотеки
var EXPRESS = require('express'),
	CONFIG = require('config'),
	PATH = require('path');
	
//подгружаемые модули
var PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	LOGGER = require(PATH.join(__dirname, 'module.logger.js'));


var status = EXPRESS.Router();

//маршрут
status.all('*', function (req, res) {
	(new Promise(function (resolve, reject){
		if(CONFIG.bantimeout){
			if(
				(res.locals.permission['all'] && (res.locals.permission['all'].indexOf('r') !== -1)) ||
				(res.locals.permission['status'] && (res.locals.permission['status'].indexOf('r') !== -1))
			){
				return resolve('next');
			} else {
				res.status(403).json({
					status: "error",
					message: "Permission denied"
				});
				return reject('close');
			}
		} else {
			return resolve('next');
		}
	})).then(function(){
		return new Promise(function (resolve){ 
			let _st = {
				iRetailCloudServer: {
					version: PROCSTORE.getState().version,
					status: "ok",
					script: "process.master.js",
					processes: 1,
					workers: {}
				},
				MongoDB: {},
				Redis:{}
			}
			let _count = 1;	//1 процесс мастер
			for(const key in PROCSTORE.getState().stats.workers){
				_count++;
				const _worker = PROCSTORE.getState().stats.workers[key];
				_st.iRetailCloudServer.workers[_worker.pid] = {
					script: _worker.script,
					status: (function(){if(_worker.status){return "ok"} else {return "unreachable";}})()
				}
			}
			_st.iRetailCloudServer.processes = _count;
			for(const _workerid in PROCSTORE.getState().stats.mongodb){
				_st.MongoDB[PROCSTORE.getState().stats.workers[_workerid].pid] = (function(){if(PROCSTORE.getState().stats.mongodb[_workerid]){return "ok"} else {return "unreachable";}})();
			}
			for(const _workerid in PROCSTORE.getState().stats.redis){
				_st.Redis[PROCSTORE.getState().stats.workers[_workerid].pid] = (function(){if(PROCSTORE.getState().stats.redis[_workerid]){return "ok"} else {return "unreachable";}})();
			}
			res.json(_st);
			return resolve('next');
		});
	}).catch(function(error){
		LOGGER.error('Ошибка отдачи статуса: ' + error);
		return res.status(500).json({status:"error", message:'Internal Server Error'});
	});
});

module.exports = status;