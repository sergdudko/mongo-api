# iRetailCloudServer v3

Состоит из нескольких процессов:

- master-процесс (process.master.js)
- web-воркеры (process.worker.web.js=>app.js)
- sub-воркеры (process.worker.sub.js)
- консоль (process.console.js)

Модули проекта:

- app.js, app.ХХХ.js, app.ХХХ.ХХХ.js - маршруты веб-апи
- module.function.js - функции приложения (для вторичного использования)
- module.logger.js - модуль логгирования. logger.error дополнительно формирует email сообщение, а logger.debug выводится только в режими дебага
- module.sendmail.js - модуль отправки сообщений (функция send пишет сообщение в очередь/файл, а worker разгребает очередь и занимается отправкой)
- module.mongodb.js - функционал работы с mongodb (запросы)
- module.redis.js - работа с редис (создание и контроль соединения)
- procstore.js - хранилище redux, используется для синхронизации и хранения состояний процессов приложения по схеме IPC worker=>master=>workers[] (управление в библиотеке redux-cluster)
- module.validators.js - валидаторы структуры (входящего json, записи в БД, наличия признака электронных рецептов и т.д.)
- test.read.js - тест приложения на чтение
- test.write.js - тест приложения на запись в БД (коннект НЕ должен быть на боевую монгу)
- module.ehr.js - работа с сервером электронных рецептов

## Master-процесс

Отвечает за:

- отслеживание состояний воркеров (Если воркер был уничтожен, например съел всю память и умер, то мастер запустит нового.)
- управлением базой данных приложения (управление отдано библиотеке `redux-cluster`)
- распределением нагрузки (автоматически)

## Web-воркеры

Для авторизации на сервере используется Basic HTTP авторизация.  
Cлушают входящие соединения (порт задан в конфиге) и сообщают на них ответы со статусами:

статус |     текст ответа    |                    расшифровка
------ | ------------------- | ----------------------------------------------------------------------
  201  |     json-объект     | Данные изменились, кэш записан и возвращен результат выборки из кэша
  200  |     json-объект     | Данные изменились и возвращен результат выборки из кэша
  204  |      No Content     | Запрос в базу вернулся пустым (данных нет)
  206  |   Partial Content   | Запрос уже выполняется (данные пока не полные)
  208  |   Already Reported  | Данные не изменились и переданы не будут
  404  |      Not Found      | Некоректный запрос (ссылка не обрабатывается)
  405  | Method Not Allowed  | Метод не поддерживается (метод не обрабатывается)
  417  |  Expectation Failed | Запрос в базу данных возвращен с ошибкой
  500  |Internal Server Error| Ошибка сервера (подробнее смотреть в логе сервера)
  
Текстовый ответ заменен на `json` вида (не все клиенты могут воспринимать json, в случае кодов не 200):
```
{
    "status": "error",
    "message": "Not Found"
}
```

- status - статус ответа, ok - хорошо (код 2ХХ) или error - ошибка (коды 4ХХ и 5ХХ)
- message - сообщение ответа  

### Маршрут /v3/query

Поисковый запрос в БД  
Поддерживает gzip сжатие "из коробки" (все данные кэшируются в уже сжатом виде, для обратной совместимости при отсутствии хидера `Accept-Encoding:gzip` ответ расшифровывается)  

#### Обработка данных web-воркером:

Принимает POST-запрос по ссылке /v3/query с `Content-Type` типа `application/json` (хидер критичен) и телом типа:

```
	{
		"find": {
			"type": {
				"$eq": "Справочник.КартыЛояльности"
			}
		},
		"limit": 999999,
		"skip":100,
		"sort":{"version": 1},
		"requires": []
	}
```

- find(object) - запрос в базу mongodb (все тоже самое что в `find()` запросе в базу, синтаксис аналогичен запросам в mongodb). Обязательное поле.
- limit(integer) - предел выборки, по умолчанию 10000 (от 1 до 999999 документов). Не обязательное поле.
- skip(integer) - пропуск документов, по умолчанию 0. Не обязательное поле.
- sort(object) - сортировка документов, синтаксис аналогичен запросам в mongodb. Не обязательное поле.
- requires(array) - Запрашиваемые поля из документа, можно указать как `["requisite.id", "requisite.value"]`. По-умолчанию все. Не обязательное поле.

В запросе могут быть хидеры:

- ics-cache-hash - sha1-хэш запроса
- ics-cache-hashresult - sha1-хэш результата запроса
- ics-cache-timeout - таймаут кэширования в минутах

#### Работа кэша:

1. Если не задан в хидере `ics-cache-timeout` (в минутах), либо меньше, чем указан в конфигурационном файле (в секундах) - будет установлен как в конфигурационном файле (L1 CACHE).
2. Если таймаут кэширования меньше часа и в конфигурационном файле включено использование redis, то кэш пишется в redis и с него же отдается.
3. Если не задан в хидере `ics-cache-timeout` (в минутах), либо он меньше, чем указан в конфигурационном файле (в секундах) - будет установлен как в конфигурационном файле (L2 CACHE).
4. Если таймаут кэширования час и более или в конфигурационном файле выключено использование redis(в данном случае таймаут кэширования час по умолчанию, см.п3), то кэш пишется на диск и с него же отдается.

#### Собственно обработка:

1. При получении запроса сервер собирает хэш запроса (HASH). Если используется redis и он доступен идем в п.2, если нет в п.15.
2. Проверяет наличие документа в redis с ключом HASH-data. И то что срок хранения кэша не истек:	
    - hash - хэш запроса
    - hashresult - хэш результата
	- datetime - метка времени создания хэша
>```
>	{ 
>		"hash" : "f92319e748e075a0a24a7006e2bbef18799508ca", 
>		"hashresult" : "b5a6e42b40787b8cd831805b19b1ab04586b3a68",
>       "datetime" : "313213213213"
>	}
>```
3. Проверяет наличие документа в redis с ключом HASH.
4. Если условия в п.2 и п.3 выполнены идем в п.5, иначе в п.6.
5. Если хэш результата из хидера `ics-cache-hashresult` соответствует хранимому значению в hashresult документа из п.2 идем в п.5.1, иначе в п.5.2.
    1. Возвращаем статус 208 (контент не изменился). Идем в п.5.3.
    2. Возвращаем статус 200 и в потоке отдаем ответ из файла. Идем в п.5.3.
    3. Завершаем обработку.
6. Если в внутренней базе данных приложения запущен процесс формирования кэша идем в п.6.1, иначе в п.6.2.
    1. Возвращаем статус 206 (Частичный контент). Клиент должен повторить запрос позже. Завершаем обработку.
    2. Создаем курсор на коллекцию iRetailCloud\Data (согласно заданным в запросе параметрам).
7. Если количество документов в курсоре больше нуля идем в п.8, иначе в п.7.1.
    1. Возвращаем статус 204 (Нет контента). Обновляем данные в redis HASH-data и завершаем обработку.
8. Создаем поток подсчета хэша результатов.
9. Создаем поток трансформации (для приведения к валидному json) из курсора. В функции трансформации обновляем поток из п.8.
10. Создаем поток записи в redis HASH.
11. Создаем поток сжатия.
12. Если в конфиге задано использование сжатия, то объединяем потоки из п.9-11, иначе потоки из п.9 и п.10.
13. По завершении потока из п.10, уничтожаем потоки из п.8,9 и 11. Шлем мастеру сообщение, что процесс формирования кэша завершен.
14. Отдаем в потоке результат запроса из файла. Обновляем данные в redis HASH-data и завершаем обработку, иначе идем в п.15
15. Проверяет наличие документа в коллекции iRetailCloud\Request с `{"type" : "hash","hash" : "(HASH)"}`. И то что срок хранения кэша не истек. Структура объекта (уникальный индекс hash должен быть создан в коллекции для ускорения поиска):	
    - type - всегда hash
    - hash - хэш запроса и префикс сервера из конфига (нужен для идентификации хэшей на нескольких серверах)
    - timeout - таймаут кэширования данных
    - timestamp - метка времени в epoch
    - hashresult - хэш результата
>```
>	{ 
>		"_id" : ObjectId("5b3652953effee1c936851ae"), 
>		"type" : "hash", 
>		"hash" : "f92319e748e075a0a24a7006e2bbef18799508ca-test", 
>		"timeout" : NumberInt(86400000), 
>		"timestamp" : 1530289944925.0, 
>		"hashresult" : "b5a6e42b40787b8cd831805b19b1ab04586b3a68"
>	}
>```
16. Проверяет наличие кэша в файловой системе (путь указан в конфиге, на папку должны быть полные права), имя файла (HASH).
17. Если условия в п.15 и п.16 выполнены идем в п.18, иначе в п.19.
18. Если хэш результата из хидера `ics-cache-hashresult` соответствует хранимому значению в hashresult документа из п.15 идем в п.18.1, иначе в п.18.2.
    1. Возвращаем статус 208 (контент не изменился). Идем в п.18.3.
    2. Возвращаем статус 200 и в потоке отдаем ответ из файла. Идем в п.18.3.
    3. Если изменился таймаут кэширования обновляем его в коллекции iRetailCloud\Request. Завершаем обработку.
19. Если в внутренней базе данных приложения запущен процесс формирования кэша идем в п.19.1, иначе в п.19.2.
    1. Возвращаем статус 206 (Частичный контент). Клиент должен повторить запрос позже. Завершаем обработку.
    2. Создаем курсор на коллекцию iRetailCloud\Data (согласно заданным в запросе параметрам).
20. Если количество документов в курсоре больше нуля идем в п.21, иначе в п.20.1.
    1. Возвращаем статус 204 (Нет контента). Обновляем данные в коллекции iRetailCloud\Request и завершаем обработку.
21. Создаем поток подсчета хэша результатов.
22. Создаем поток трансформации (для приведения к валидному json) из курсора. В функции трансформации обновляем поток из п.21.
23. Создаем поток записи в файловую систему.
24. Создаем поток сжатия.
25. Если в конфиге задано использование сжатия, то объединяем потоки из п.22-24, иначе потоки из п.22 и п.23.
26. По завершении потока из п.23, уничтожаем потоки из п.21,22 и 24. Шлем мастеру сообщение, что процесс формирования кэша завершен.
27. Отдаем в потоке результат запроса из файла. Обновляем данные в коллекции iRetailCloud\Request и завершаем обработку.

### Маршрут /v3/bigdata

Запрос агрегации в БД  
Поддерживает gzip сжатие "из коробки" (все данные кэшируются в уже сжатом виде, для обратной совместимости при отсутствии хидера `Accept-Encoding:gzip` ответ расшифровывается)  

#### Обработка данных web-воркером:

Принимает POST-запрос по ссылке /v3/bigdata с `Content-Type` типа `application/json` (хидер критичен) и телом вида (см. документацию по [AGGREGATION FRAMEWORK MONGODB](https://docs.mongodb.com/manual/reference/operator/aggregation-pipeline/) ):

```
	[
		{
			"$match": {
				"type": "Документ.Чек",
				"date": {
					"$gt": "2019-05-29",
					"$lt": "2019-05-30"
				},
				"forbidden": {
					"$ne": true
				},
				"requisites": {
					"$elemMatch":{
						"id" : "TradePoint_Holding_ID",
						"value": {
							"$in": [
								"e836984a-ef06-11e2-af2d-782bcb1ff45d"
							]
						}
					}
				},
				"$comment": "iRetailReportService"
			}
		},
		{
			"$project":{
				"date": {"$substr":["$date", 0, 10]},
				"time": {"$substr":["$date", 11, 8]},
				"frnum": "$description",
				"check_uid": "$guid",
				"requisites": { 
					"$arrayToObject": { 
						"$reduce":{
							"input": "$requisites",
							"initialValue": [],
							"in": {
								"$concatArrays": [
									"$$value",
									[{
										"k": "$$this.id", 
										"v": "$$this.value"
									}]
								]
							}
						}
					} 
				},
				"multilines": {
					"$arrayToObject": { 
						"$reduce":{
							"input": "$multilines",
							"initialValue": [],
							"in": {
								"$concatArrays": [
									"$$value",
									[{
										"k": "$$this.id", 
										"v": "$$this.lines"
									}]
								]
							}
						}
					}
				}
			}
		},
		{
			"$project":{
				"date": "$date",
				"time": "$time",
				"tradepoint_uid": "$requisites.TradePoint_Holding_ID",
				"parent_uid": "$requisites.TradePointParentID",
				"frnum": "$description",
				"check_uid": "$check_uid",
				"check_num": "$requisites.NDocKsa",
				"check_return": "$requisites.CheckIsReturn",
				"sellername": "$requisites.SellerName",
				"buyername": "$requisites.BuyerName",
				"positions": {
					"$reduce": {
						"input": "$multilines.ПозицииЧека",
						"initialValue": [],
						"in": {
							"$concatArrays": [
								"$$value",
								[{
								"$arrayToObject": { 
									"$reduce":{
										"input": "$$this.requisites",
										"initialValue": [],
										"in": {
											"$concatArrays": [
												"$$value",
												[{
													"k": "$$this.id", 
													"v": "$$this.value"
												}]
											]
										}
									}
								}
								}]
							]
						}
					}
				}
			}
		},
		{
			"$unwind": "$positions"
		},
		{
			"$project":{
				"date": "$date",
				"time": "$time",
				"tradepoint_uid": "$tradepoint_uid",
				"parent_uid": "$parent_uid",
				"frnum": "$frnum",
				"check_uid": "$check_uid",
				"check_num": "$check_num",
				"check_return": "$check_return",
				"sellername": "$sellername",
				"buyername": "$buyername",
				"position_uid": { "$toLower": "$positions.PositionsUid" },
				"nomenklature_uid": { "$toLower": "$positions.Номенклатура" },
				"nomenklature_series": "$positions.Серия",
				"nomenklature_exprires": "$positions.СрокГодности",
				"price_without_discount": "$positions.ЦенаБезСкидки",
				"price_with_discount": "$positions.ЦенаСоСкидкой",
				"position_count": {
					"$divide": [
						{
							"$sum": [
								{
									"$multiply": [
										"$positions.ЦелоеКоличество", 
										"$positions.ДелимостьНаЧасти"
									]
								},
								"$positions.ДробноеКоличество"
							]
						},
						"$positions.ДелимостьНаЧасти"
					] 
				},
				"sum_without_discount": "$positions.СуммаБезСкидки",
				"sum_with_discount": "$positions.СуммаСоСкидкой",
				"patient": "$positions.PatientFio",
				"sum_from_card": "$positions.SumPaymentCard",
				"sum_from_cert": "$positions.SumPaymentCert",
				"discount_uid": "$positions.Premium_Guid",
				"sum_discount": "$positions.SumPremium",
				"discount_percent": "$positions.PercentPremium"
			}
		}
	]
```

В запросе могут быть хидеры:

- ics-cache-hash - sha1-хэш запроса
- ics-cache-hashresult - sha1-хэш результата запроса
- ics-cache-timeout - таймаут кэширования в минутах

#### Работа кэша:

1. Если не задан в хидере `ics-cache-timeout` (в минутах), либо меньше, чем указан в конфигурационном файле (в секундах) - будет установлен как в конфигурационном файле (L1 CACHE).
2. Если таймаут кэширования меньше часа и в конфигурационном файле включено использование redis, то кэш пишется в redis и с него же отдается.
3. Если не задан в хидере `ics-cache-timeout` (в минутах), либо он меньше, чем указан в конфигурационном файле (в секундах) - будет установлен как в конфигурационном файле (L2 CACHE).
4. Если таймаут кэширования час и более или в конфигурационном файле выключено использование redis(в данном случае таймаут кэширования час по умолчанию, см.п3), то кэш пишется на диск и с него же отдается.

#### Собственно обработка:

1. При получении запроса сервер собирает хэш запроса (HASH). Если используется redis и он доступен идем в п.2, если нет в п.15.
2. Проверяет наличие документа в redis с ключом HASH-data. И то что срок хранения кэша не истек:	
    - hash - хэш запроса
    - hashresult - хэш результата
	- datetime - метка времени создания хэша
>```
>	{ 
>		"hash" : "f92319e748e075a0a24a7006e2bbef18799508ca", 
>		"hashresult" : "b5a6e42b40787b8cd831805b19b1ab04586b3a68",
>       "datetime" : "313213213213"
>	}
>```
3. Проверяет наличие документа в redis с ключом HASH.
4. Если условия в п.2 и п.3 выполнены идем в п.5, иначе в п.6.
5. Если хэш результата из хидера `ics-cache-hashresult` соответствует хранимому значению в hashresult документа из п.2 идем в п.5.1, иначе в п.5.2.
    1. Возвращаем статус 208 (контент не изменился). Идем в п.5.3.
    2. Возвращаем статус 200 и в потоке отдаем ответ из файла. Идем в п.5.3.
    3. Завершаем обработку.
6. Если в внутренней базе данных приложения запущен процесс формирования кэша идем в п.6.1, иначе в п.6.2.
    1. Возвращаем статус 206 (Частичный контент). Клиент должен повторить запрос позже. Завершаем обработку.
    2. Создаем курсор на коллекцию iRetailCloud\Data (согласно заданным в запросе параметрам).
8. Создаем поток подсчета хэша результатов.
9. Создаем поток трансформации (для приведения к валидному json) из курсора. В функции трансформации обновляем поток из п.8.
10. Создаем поток записи в redis HASH.
11. Создаем поток сжатия.
12. Если в конфиге задано использование сжатия, то объединяем потоки из п.11,10 после чего пушим в поток 8 и 11 `[` и пробрасываем буффер из потока 9 в поток 11. При завершении потока 9 пушим в поток 8 и 11 `]` и завершаем поток 11. Если сжатие не указано пушим в поток 8 и 10 `[` и пробрасываем буффер из потока 9 в поток 10. При завершении потока 9 пушим в поток 8 и 10 `]` и завершаем поток 10.
13. По завершении потока из п.10, уничтожаем потоки из п.8,9 и 11. Шлем мастеру сообщение, что процесс формирования кэша завершен. Если ни один документ не обработан в потоке 22, меняем хэш на пустой SHA1 (`""`).
14. Отдаем в потоке результат запроса из файла. Обновляем данные в redis HASH-data и завершаем обработку, иначе идем в п.15
15. Проверяет наличие документа в коллекции iRetailCloud\Request с `{"type" : "hash","hash" : "(HASH)"}`. И то что срок хранения кэша не истек. Структура объекта (уникальный индекс hash должен быть создан в коллекции для ускорения поиска):	
    - type - всегда hash
    - hash - хэш запроса и префикс сервера из конфига (нужен для идентификации хэшей на нескольких серверах)
    - timeout - таймаут кэширования данных
    - timestamp - метка времени в epoch
    - hashresult - хэш результата
>```
>	{ 
>		"_id" : ObjectId("5b3652953effee1c936851ae"), 
>		"type" : "hash", 
>		"hash" : "f92319e748e075a0a24a7006e2bbef18799508ca-test", 
>		"timeout" : NumberInt(86400000), 
>		"timestamp" : 1530289944925.0, 
>		"hashresult" : "b5a6e42b40787b8cd831805b19b1ab04586b3a68"
>	}
>```
16. Проверяет наличие кэша в файловой системе (путь указан в конфиге, на папку должны быть полные права), имя файла (HASH).
17. Если условия в п.15 и п.16 выполнены идем в п.18, иначе в п.19.
18. Если хэш результата из хидера `ics-cache-hashresult` соответствует хранимому значению в hashresult документа из п.15 идем в п.18.1, иначе в п.18.2.
    1. Возвращаем статус 208 (контент не изменился). Идем в п.18.3.
    2. Возвращаем статус 200 и в потоке отдаем ответ из файла. Идем в п.18.3.
    3. Если изменился таймаут кэширования обновляем его в коллекции iRetailCloud\Request. Завершаем обработку.
19. Если в внутренней базе данных приложения запущен процесс формирования кэша идем в п.19.1, иначе в п.19.2.
    1. Возвращаем статус 206 (Частичный контент). Клиент должен повторить запрос позже. Завершаем обработку.
    2. Создаем курсор на коллекцию iRetailCloud\Data (согласно заданным в запросе параметрам).
20. Если количество документов в курсоре больше нуля идем в п.21, иначе в п.20.1.
    1. Возвращаем статус 204 (Нет контента). Обновляем данные в коллекции iRetailCloud\Request и завершаем обработку.
21. Создаем поток подсчета хэша результатов.
22. Создаем поток трансформации (для приведения к валидному json) из курсора. В функции трансформации обновляем поток из п.21.
23. Создаем поток записи в файловую систему.
24. Создаем поток сжатия.
15. Если в конфиге задано использование сжатия, то объединяем потоки из п.24,23 после чего пушим в поток 21 и 24 `[` и пробрасываем буффер из потока 22 в поток 24. При завершении потока 22 пушим в поток 21 и 24 `]` и завершаем поток 24. Если сжатие не указано пушим в поток 21 и 23 `[` и пробрасываем буффер из потока 22 в поток 23. При завершении потока 22 пушим в поток 21 и 23 `]` и завершаем поток 23.
26. По завершении потока из п.23, уничтожаем потоки из п.21,22 и 24. Шлем мастеру сообщение, что процесс формирования кэша завершен. Если ни один документ не обработан в потоке 22, то удаляем файл и меняем хэш на пустой SHA1 (`""`).
27. Отдаем в потоке результат запроса из файла. Обновляем данные в коллекции iRetailCloud\Request и завершаем обработку.

### Маршрут /v3/data

Запись в базу данных, формат позаимствован (с целью совместимости) из iRetailCloudServer v1 (также была реализована уникальность объектов `type-guid` в рамках коллекции средствами БД)
Также реализована обработка чеков с реквизитом `bSaleByE_Prescription` (в зависимости от реквизита `CheckIsReturn` производится регистрация и отмена отоваривания по электронному рецепту).
Поддерживает сжатый формат тела запроса (`Header encoding=gzip` ).

#### Поля Type структуры Object:

- **Строка**
- **Число**
- **Булево** - true / false (defaul)
- **Дата** - формат: гггг-мм-ддTчч:мм:сс
- **Справочник.х** - х - тип справочника
- **Документ.х** - х - тип документа
- **Перечисление.х** - х - тип перечисления

#### Object

Имя поля       | Тип         | Назначение
---------------|-------------|----------
type(*)        | Строка      | Тип объекта (см. Поля type структуры Object)
guid(*)        | Строка      | ID объекта (уникальный в пределах всей базы)
description    | Строка      | Представление объекта (наименование для справочника, номер для документа)
version        | Строка      | Формируется сервером для поддержки версионирования данных.
isdeleted      | Булево      | Пометка на удаление
forbidden      | Булево      | Удален в базе-источнике (игнорируется в POST устанавливается в true сервером в DELETE)
parent         | Строка      | Ссылка (guid) на родительский элемент
isfolder       | Булево      | Элемент является папкой
date           | Дата        | Дата-время документа
isactive       | Булево      | Документ проведен (в нотации 1С)
requisites     | []requisite | Реквизиты
arrays         | []array     | Массивы
multilines     | []multiline | Табличные части
files          | []file      | Набор неиндексируемых полей (для хранения файлов как строки)

#### requisite

Имя поля       | Тип         | Назначение
---------------|-------------|----------
type(*)        | Строка      | Тип реквизита (см. Поля type структуры Object)
id(*)          | Строка      | Идентификатор реквизита (уникальный в пределах экземпляра requisites)
name           | Строка      | Представление реквизита
value          | Любое       | Значение реквизита (например, guid для справочника или значение для простых)

#### array

Имя поля       | Тип         | Назначение
---------------|-------------|----------
type(*)        | Строка      | Тип реквизита (см. Поля type структуры Object)
id(*)          | Строка      | Идентификатор реквизита (уникальный в пределах экземпляра arrays)
name           | Строка      | Представление реквизита
values         | []Любое     | Значения реквизита (например, список штрихкодов)

#### multiline

Имя поля       | Тип         | Назначение
---------------|-------------|----------
id(*)          | Строка      | Идентификатор табличной части (уникальный в пределах экземпляра multilines)
lines          | []line      | Строки табличной части

#### line

Имя поля       | Тип         | Назначение
---------------|-------------|----------
requisites     | []requisite | Реквизиты табличной части
arrays         | []array     | Массивы табличной части

#### file

Имя поля       | Тип         | Назначение
---------------|-------------|----------
name           | Строка      | Имя файла
requisites     | []requisite | Реквизиты файла

**Отправка данных**
Метод: POST  
Формат запроса: []Object  
Пример запроса:  

```json
[
    {
        "type": "Справочник.Номенклатура",
        "guid": "295befda-5cbf-44f5-b932-90abf6de11f2",
        "description": "Аспирин таб. покр. кишечнорастворим. об. 100мг в уп. №14х4",
        "requisites": [
            {
                "type": "Справочник.Производители",
                "id": "Производитель",
                "value": "295befda-5cbf-44f5-b932-90abf6de1375"
            },
            {
                "type": "Строка",
                "id": "Рецептурность",
                "value": "R+"
            },
            {
                "type": "Число",
                "id": "Кратность",
                "value": "5"
            },
            {
                "type": "Справочник.ДействующиеВещества",
                "id": "Действующее вещество",
                "value": "295befda-5cbf-44f5-b932-90abf6de1222"
            }
        ],
        "arrays": [
            {
                "type": "Строка",
                "id": "Штрихкоды",
                "values": [
                    "4008500130407",
                    "4008500120019",
                    "4602809000043"
                ]
            }
        ]
    },
    {
        "type": "Справочник.Партнеры",
        "guid": "295befda-5cbf-44f5-b932-444444",
        "description": "ООО Пупкин и К",
        "requisites": [
            {
                "type": "Справочник.Страны",
                "id": "Старана регистрации",
                "value": "295befda-5cbf-44f5-b932-90abf6de1375"
            },
            {
                "type": "Число",
                "id": "Число сотрудников",
                "value": "117"
            },
            {
                "type": "Справочник.Договора",
                "id": "Основной договор",
                "value": "295befda-5cbf-44f5-b932-90abf6de1222"
            }
        ],
    }
]
```

#### Работа с сервером электронных рецептов

При наличии признака электронных рецептов, на каждую позицию чека с таковым признаком пишется объект в коллекцию iRetailCloud\Request вида:  
```
{ 
    "_id" : ObjectId("5bc96fb73c55ad53db258702"), 
    "hash" : "9f3790a388acc3b338c613b6865eae31c102c157", 
    "type" : "EHRRequest", 
    "check" : "08c68068-d09e-45ac-9c87-555ab8d9c4a4", 
	"position" : "ff9cc51c-1920-4fbb-9ca0-0f67c8e9c214",
	"status" : "query", 
    "dispense" : null,
	"timestamp" : 1539779324000.0,
    "body" : {
        "resourceType" : "Parameters", 
        "parameter" : [
            {
                "name" : "medicationDispense", 
                "resource" : {
                    "resourceType" : "MedicationDispense", 
                    "identifier" : [
                        {
                            "system" : "http://ocean.farmin.by/Документ.Чек", 
                            "value" : "08c68068-d09e-45ac-9c87-555ab8d9c4a4/5FD96AA7-2F9C-43CD-A6EC-47AB6469025A"
                        }
                    ], 
                    "extension" : [
                        {
                            "url" : "http://fhir.org/fhir/StructureDefinition/by-finalDispense", 
                            "valueBoolean" : false
                        }
                    ], 
                    "contained" : [
                        {
                            "id" : "internalMedication1", 
                            "code" : {
                                "coding" : [
                                    {
                                        "system" : "http://www.pharma.by/Справочник.Дозировка", 
                                        "code" : "23861"
                                    }
                                ]
                            }, 
                            "resourceType" : "Medication", 
                            "isBrand" : true
                        }, 
                        {
                            "name" : "Аптека №31", 
                            "address" : {
                                "text" : "г. Минск, ул. Колесникова П.Р., дом № 15, помещение 1", 
                                "extension" : [
                                    {
                                        "url" : "http://fhir.org/fhir/StructureDefinition/by-AddressType", 
                                        "valueCodeableConcept" : {
                                            "coding" : [
                                                {
                                                    "system" : "http://hl7.org/fhir/vs/by-address-type", 
                                                    "code" : "location"
                                                }
                                            ]
                                        }
                                    }, 
                                    {
                                        "url" : "http://fhir.org/fhir/StructureDefinition/by-soatoCode", 
                                        "valueCodeableConcept" : {
                                            "coding" : [
                                                {
                                                    "system" : "http://hl7.org/fhir/vs/by-soato-code", 
                                                    "code" : "5000000000"
                                                }
                                            ]
                                        }
                                    }
                                ]
                            }, 
                            "resourceType" : "Location"
                        }
                    ], 
                    "status" : "completed", 
                    "patient" : {
                        "reference" : "Patient/1cd9d56a-c518-43f3-8092-4241a28dbf54"
                    }, 
                    "authorizingPrescription" : [
                        {
                            "reference" : "MedicationPrescription/7bfb817b-9131-4a29-a949-e1846004d2c0"
                        }
                    ], 
                    "quantity" : {
                        "value" : NumberInt(30)
                    }, 
                    "whenHandedOver" : "2018-10-17", 
                    "medication" : {
                        "reference" : "#internalMedication1"
                    }, 
                    "substitution" : {
                        "type" : {
                            "coding" : [
                                {
                                    "system" : "http://hl7.org/fhir/v3/vs/ActSubstanceAdminSubstitutionCode", 
                                    "code" : "29026"
                                }
                            ]
                        }
                    }
                }
            }
        ]
    }
}
```

- hash - хэш guid-а чека и uid-а позиции чека (это важное поле, по нему производится поиск в core приложения).
- type - всегда EHRRequest
- check - guid чека, для удобства поиска
- position - uid позиции чека, для удобства поиска
- status - статус отправки, может быть:
	- query - задание для воркера, при этом задание автоматически воспринимается как
		- отмена чека, если dispense задан
		- регистрации чека, если dispense не задан (null)
	- error - ошибка при регистрации продажи на сервере EHR
	- completed - продажа зарегистрирована на сервере EHR
	- canceled - продажа отменена на сервере EHR
- dispense - uid MedicationDispense на сервере электронных рецептов
- timestamp - epoch время от даты чека (в перспективе можно затирать body старых чеков)
- body - тело сформированного запроса, именно он будет отправляться на сервер EHR

### Маршрут /v3/echo

Эхо-сервер с валидатором документов (синтаксис см. в маршруте `/v3/data`).

### Маршрут /v3/status

Поддерживает gzip сжатие выводом в поток `res.locals.gzipper` (при установленном хидере `Accept-Encoding:gzip`)  
Возвращает состояние API и версию из `package.json` в виде `json`.  
Этим маршрутом можно также проверять что mongodb и redis живы, а также тест сжатия gzip.  

Формат ответа:
```
{
    "iRetailCloudServer": {
        "version": "3.0.10",
        "status": "ok",
        "script": "process.master.js",
        "processes": 4,
        "workers": {
            "15231": {
                "script": "./process.worker.web.js",
                "status": "ok"
            },
            "15238": {
                "script": "./process.worker.sub.js",
                "status": "ok"
            },
            "15501": {
                "script": "./process.worker.web.js",
                "status": "ok"
            }
        }
    },
    "MongoDB": {
        "15231": "ok",
        "15238": "ok",
        "15501": "ok"
    },
    "Redis": {
        "15231": "ok",
        "15501": "ok"
    }
}
```

- iRetailCloudServer - приложение сервера
	- version - версия приложения сервера
	- status - состояние приложения, ok - хорошо и unreachable - не доступен
	- script - скрипт запуска
	- processes - количество запущенных процессов
	- workers - дочерние процессы
		- 15231 - идентификатор процесса PID
		- script - скрипт запуска
		- status - состояние приложения, ok - хорошо и unreachable - не доступен
- MongoDB - состояние базы данных Mongo (отображаются только процессы с установленным требуемым соединением)
	- 15231 - идентификатор процесса PID
		- ok - хорошо и unreachable - не доступен, состояние соединения с бд
- Redis - состояние базы данных Redis (отображаются только процессы с установленным требуемым соединением)
	- 15231 - идентификатор процесса PID
		- ok - хорошо и unreachable - не доступен, состояние соединения с redis

### Маршрут /v3/recipe

Поддерживает gzip сжатие выводом в поток `res.locals.gzipper` (при установленном хидере `Accept-Encoding:gzip`)  
Проксирует запрос на сервер электронных рецептов и возвращает отформатированный ответ.  

#### Собственно обработка:
1. Получен запрос вида `/v3/recipe?id=9112420000000022`, где id - номер карты пациента
2. Происходит авторизация на сервере EHR (получение токена)
3. Отправляется запрос на поиск пациента
4. Ответ сервера форматируется и выдается в поток ответа. Если пациент не найден, отдаю статус 204 (Согласно документации, сервер EHR должен вернуть статус 404).

Формат ответа:
```
{
    "Patient": {
        "Id": "1cd9d56a-c518-43f3-8092-4241a28dbf54",
        "Name": "Кац Игорь Езефович",
        "Gender": "male",
        "BirthDate": "1986-07-13",
		"HomeAddress": "Республика Беларусь, 247210, г.Жлобин, ул.Ленинградская, дом № 9Б, кв.3"
    },
    "Prescriptions": [
        {
            "Id": "42c90e56-0972-48e3-816d-041f8ea32bb9",
            "DateWritten": "2015-06-15",
            "Quantity": 10,
            "DosageInstruction": [
                "По 1 таблетке 2 раза в день"
            ],
            "Medication": {
                "Code": "",
                "MNN": "402019"
            },
            "Dispenses": [
                {
                    "Date": "2015-06-20",
                    "Quantity": 10,
                    "Medication": {
                        "Code": "39696",
                        "MNN": ""
                    }
                },
                {
                    "Date": "2015-06-20",
                    "Quantity": 10,
                    "Medication": {
                        "Code": "39696",
                        "MNN": ""
                    }
                },
                {
                    "Date": "2015-06-20",
                    "Quantity": 10,
                    "Medication": {
                        "Code": "39696",
                        "MNN": ""
                    }
                },
                {
                    "Date": "2015-06-20",
                    "Quantity": 10,
                    "Medication": {
                        "Code": "39696",
                        "MNN": ""
                    }
                }
            ],
			Signature: true
        }
	]
}
```
- Patient - данные пациента
- Prescriptions - описание рецепта
	- Dispenses - продажи по рецепту
	- Medication - выписанные лс
	- Signature - наличие ЭЦП


## Sub-воркеры
- проверяет папку с кэшами в файловой системе и что кэши не истекли (данные из коллекции iRetailCloud\Request)
- если файловый кэш истек - уничтожает его
- отправляет сообщения из папки `mail` (туда попадают, например, ошибки из `logger.js`)
- обрабатывает запросы в сервер электронных рецептов из коллекции iRetailCloud\Request
- проверяет ошибки отправки на сервер электронных рецептов в коллекции iRetailCloud\Request
- очищает коллекцию iRetailCloud\Request от устаревших хэшей

## Конфигурация:
Файл конфигурации находится в `./config/NAME.properties`, где `NAME` - соответствует `NODE_ENV`. Т.е. для production это файл `./config/production.properties`. По дефолту `default.properties` (его не используем).

```
	# Globals
	#режим дебага (выводит сообщения LOGGER.debug)
	debug = false
	#использовать компрессию
	usegzip = true
	#использовать редис
	useredis = true
	#директория для файлового кэша (используйте, например, tmpfs)
	cachedir = /var/run/ics/cache/
	#минимальный таймаут кэширования в сек
	cache = 600
	#должен быть установлен в true на первичном сервере (чтобы исключить задваивание операций, например запуск ehr воркера и сборка мусора в служебной коллекции)
	primary = true
	#имя сервера, для идентификации
	servername = cs2
	#таймаут блакировки в минутах, при неверном вводе логина/пароля (если включен - используется Basic авторизация)
	bantimeout = 15
	#количество попыток неверного ввода логина/пароля перед блокировкой
	banattemp = 5
	#прокси сервер, для проброса реального ip в хидере X-Real-IP например для NGINX "proxy_set_header  X-Real-IP  $remote_addr;"
	trustedproxy = 0.0.0.0
	

	# Server Settings
	#порт веб-апи
	server.port = 9999
	#настройка https
	server.ssl.crt = 
	server.ssl.key = 
	server.ssl.ca =

	# Database Settings
	#строка подключения к mongodb
	mongodb.url = mongodb://LOGIN:PASSWORD@CLUSTER1:27017,CLUSTER2:27017,CLUSTER3:27017/DATABASE?replicaSet=REPLICA_NAME&readPreference=secondaryPreferred
	#используемая база данных
	mongodb.database = DATABASE

	# Redis Settings
	#настройки редис
	redis.url = redis://LOGIN:PASSWORD@localhost:6379/DB_NUMBER

	# Email Settings
	#настройки email
	email.host = email.farmin.by
	email.port = 25
	email.login = логин_отправителя
	email.password = пароль_отправителя
	email.mailto = адрес_получателя
	
	# EHR Settings
	#при прямом соединении с платформой (используется сервер авторизации, запросы подписываются токеном)
	ehr.server = http://api.ehr.bas-net.by/fhir
	ehr.auth = http://auth.ehr.bas-net.by/connect/token
	ehr.login = логин_учетной_записи_ehr
	ehr.password = пароль_учетной_записи_ehr
	#при использовании НЦЭУ (не используется сервер авторизации, запросы не подписываются токеном)
	ehr.server = http://000.000.000.000:0000/FHIR_Sync/rest/FHIR
	ehr.usenceu = true
	#ссылка на собственный сервер (указывается при регистрации продажи в EHR)
	#ehr.myserver = https://cs1.farmin.by
	
	# Timings Settings
	#Отправка медленных запросов в поток ошибок (отправляет сообщение, если запрос выполняется дольше заданного кол-ва секунд)
	slowrequest.v3query = 120
	slowrequest.v3bigdata = 1800
	slowrequest.v3data = 300
	slowrequest.v3recipe = 60
```

## Настройка прав доступа:
В служебной коллекции iRetailCloud\Auth хранятся объекты типа:
```
{ 
    "_id" : ObjectId("5cb8170df6406d088f9e2c1b"), 
    "username" : "dsfdsfdsfdsfsdfsdfs", 
    "password" : "fdfdsfsdfsdfsd", 
    "permission" : {
        "all" : "r",
		"custom": {
			"w": [
				"Документ.Чек",
				"Документ.ЭлектроннаяНакладная"
			]
		}
    }, 
    "creator" : "sergei.dudko"
}

```

- username - имя пользователя (MD5)
- password - пароль пользователя (SHA512)
- creator - кем был создан пользователь
- permission - права доступа пользователя
  - all - права в отношении всего проекта, доступны опции "r", "w" и "rw"
  - query - права в отношении маршрута /v3/query, доступны опции "r", "w"(не имеет смысла) и "rw"
  - data - права в отношении маршрута /v3/data и /v1/data, доступны опции "r"(не имеет смысла), "w" и "rw"
  - recipe - права в отношении маршрута /v3/recipe и /v1/recipe, доступны опции "r", "w"(не имеет смысла) и "rw"
  - bigdata - права в отношении маршрута /v3/bigdata, доступны опции "r", "w"(не имеет смысла) и "rw"
  - status - права в отношении маршрута /v3/status, доступны опции "r", "w"(не имеет смысла) и "rw"
  - custom - индивидуальные права доступа с разделением по type документов
    - r - индивидуальные права доступа по маршруту /v3/query, массив типов документов
	- w - индивидуальные права доступа по маршруту /v3/data и /v1/data, массив типов документов
  