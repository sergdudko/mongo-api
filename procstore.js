/**
 *	iRetailCloudServer
 *	(c) 2019 by Siarhei Dudko.
 *
 *	MODULE
 *  PROCSTORE
 *	Хранилище состояний процессов
 *  движок вынес в либу redux-cluster
 */

"use strict"

//подгружаемые библиотеки
var REDUXCLUSTER = require('redux-cluster'),
	LODASH = require('lodash'),
	PATH = require('path');
	
//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js'));

//инициализируем хранилище
var ProcessStorage = REDUXCLUSTER.createStore(editProcessStorage);
ProcessStorage.mode = "action";
ProcessStorage.resync = 1000;
ProcessStorage.stderr = LOGGER.error;

//редьюсер
function editProcessStorage(state = {
	tasks:[],
	requests:{},
	workers:{}, 
	stats:{
		mongodb:{},
		redis:{},
		workers:{}
	}, 
	version:'',
	iptoban: {},
	authdb: {}
}, action){ 
	let state_new = LODASH.clone(state);
	let _worker;
	try {
		switch (action.type){
			case 'ADD_TASK':	//запуск кэширования
				if(state_new.tasks.indexOf(action.payload.hash) === -1){
					state_new.tasks.push(action.payload.hash);
				}
				_worker = action.payload.worker.toString();
				if(typeof(state_new.workers[_worker]) !== 'object'){
					state_new.workers[_worker] = [];
				}
				if(state_new.workers[_worker].indexOf(action.payload.hash) === -1){
					state_new.workers[_worker].push(action.payload.hash);
				}
				state_new.requests[action.payload.hash] = LODASH.clone(action.payload.request);
				setTimeout(delTaskOnTimeout, 600000, action.payload.hash, action.payload.worker);
				return state_new;
			case 'DEL_TASK':	//завершение кэширования
				if(state_new.tasks.indexOf(action.payload.hash) !== -1){
					state_new.tasks.splice(state_new.tasks.indexOf(action.payload.hash),1);
				}
				_worker = action.payload.worker.toString();
				if(typeof(state_new.workers[_worker]) === 'object'){
					let _index = state_new.workers[_worker].indexOf(action.payload.hash);
					if(_index !== -1){
						state_new.workers[_worker].splice(_index,1);
					}
				}
				if(typeof(state_new.requests[action.payload.hash]) !== 'undefined')
					delete state_new.requests[action.payload.hash];
				return state_new;
			case 'SET_VERSION':	//установка версии приложения
				state_new.version = action.payload;
				return state_new;
			case 'CLEAR_TASK':	//воркер умер
				for(const key in state_new.workers){
					if(action.payload.allworkers.indexOf(key) === -1){
						for(const task in state_new.workers[key]){
							if(state_new.tasks.indexOf(state_new.workers[key][task]) !== -1){
								state_new.tasks.splice(state_new.tasks.indexOf(state_new.workers[key][task]),1);
							}
						}
						delete state_new.workers[key];
					}
				}
				for(const key in state_new.stats.mongodb){
					if(action.payload.allworkers.indexOf(key) === -1){
						delete state_new.stats.mongodb[key];
					}
				}
				for(const key in state_new.stats.redis){
					if(action.payload.allworkers.indexOf(key) === -1){
						delete state_new.stats.redis[key];
					}
				}
				for(const key in state_new.stats.workers){
					if(action.payload.allworkers.indexOf(key) === -1){
						delete state_new.stats.workers[key];
					}
				}
				return state_new;
			case 'STATUS_MONGO':	//статус соединения с mongodb
				_worker = action.payload.worker.toString();
				state_new.stats.mongodb[_worker] = action.payload.status;
				return state_new;
			case 'STATUS_REDIS':	//статус соединения с redis
				_worker = action.payload.worker.toString();
				state_new.stats.redis[_worker] = action.payload.status;
				return state_new;
			case 'STATUS_WORKERS':	//информация о воркерах
				for(const key in state_new.stats.workers){
					if(action.payload.allworkers.indexOf(key) === -1){
						delete state_new.stats.workers[key];
					}
				}
				for(const key in state_new.workers){
					if(action.payload.allworkers.indexOf(key) === -1){
						delete state_new.workers[key];
					}
				}
				for(const key in state_new.stats.redis){
					if(action.payload.allworkers.indexOf(key) === -1){
						delete state_new.stats.redis[key];
					}
				}
				for(const key in state_new.stats.mongodb){
					if(action.payload.allworkers.indexOf(key) === -1){
						delete state_new.stats.mongodb[key];
					}
				}
				_worker = action.payload.id.toString();
				state_new.stats.workers[_worker] = {
					id:action.payload.id, 
					pid: action.payload.pid, 
					script:action.payload.script,
					status:action.payload.status
				};
				return state_new;
			case 'WRONG_PASS':
				if((typeof(state_new.iptoban[action.payload.address]) !== 'object') || ((state_new.iptoban[action.payload.address].datetime + action.payload.bantimeout) < Date.now())){
					state_new.iptoban[action.payload.address] = {};
					state_new.iptoban[action.payload.address].attemp = 0;
				}
				state_new.iptoban[action.payload.address].datetime = Date.now();
				state_new.iptoban[action.payload.address].attemp = state_new.iptoban[action.payload.address].attemp + 1;
				for(const ip in state_new.iptoban){
					if(((state_new.iptoban[ip].datetime + action.payload.bantimeout) < Date.now()))
						delete state_new.iptoban[ip];
				}
				return state_new;
			case 'WRONG_PASS_CLEAR':
				if(state_new.iptoban[action.payload.address])
					delete state_new.iptoban[action.payload.address];
				return state_new;
			case 'AUTHDB_ADD':
				state_new.authdb[action.payload.username] = {password: action.payload.password, permission: action.payload.permission, datetime: Date.now()};
				return state_new;
			case 'AUTHDB_DEL':
				if(typeof(state_new.authdb[action.payload.username]) !== 'undefined')
					delete state_new.authdb[action.payload.username];
				return state_new;
			default:
				return state_new;
		}
	} catch(e){
		LOGGER.error('REDUXCLUSTER->' + (function(){ if(action && action.type){ return ' ' + action.type; }})() + ' Error:' + e);
	}
	return state_new;
}

function delTaskOnTimeout(_uid, _id){
	ProcessStorage.dispatch({type:'DEL_TASK', payload:{hash: _uid, worker: _id}});
}

module.exports = ProcessStorage;