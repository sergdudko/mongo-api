/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  VALIDATORS
 *	Валидаторы запросов
 *
 */

"use strict"

//подгружаемые библиотеки
var ObjectID = require('mongodb').ObjectID,
	LODASH = require('lodash'),
	CONFIG = require('config'),
	PATH = require('path');

//подгружаемые модули
var FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js'));

//валидатор маршрута /v3/query
function QueryValidator(data){
	try {
		let _dataobject;
		if(typeof(data) === 'object'){
			_dataobject = LODASH.clone(data);
		} else {
			_dataobject = JSON.parse(data.toString());
		}
		let datavalid = {find:{}, sort:{"_id": 1}, limit:10000, skip:0, requires:[]};
		let dataobject = {};
		for(const key in _dataobject){
			dataobject[key.toLowerCase()] = _dataobject[key];
		}
		if(typeof(dataobject.find) === 'object'){
			datavalid.find = dataobject.find;
			if(typeof(datavalid.find["_id"]) === 'object'){
				for(const k in datavalid.find["_id"]){
					datavalid.find["_id"][k] = ObjectID(datavalid.find["_id"][k]);
				}
			} else if (typeof(datavalid.find["_id"]) === 'string'){
				datavalid.find["_id"] = ObjectID(datavalid.find["_id"]);
			}
			//datavalid.find.forbidden = {"$ne":true}; //deprecated
			if(typeof(dataobject.sort) === 'object'){
				datavalid.sort = dataobject.sort;
			}
			if((typeof(dataobject.limit) === 'number') && !isNaN(dataobject.limit) && (dataobject.limit < 1000000)){ //установливаю предел выборки в 1млн
				datavalid.limit = dataobject.limit;
			}
			if((typeof(dataobject.skip) === 'number') && !isNaN(dataobject.skip)){ 
				datavalid.skip = dataobject.skip;
			}
			if(Array.isArray(dataobject.requires)){ 
				datavalid.requires = dataobject.requires;
			}
			return datavalid;
		} else {
			return 'err';
		}
	} catch(e){
		return 'err';
	}
}

//валидатор маршрута /v3/bigdata
function BigDataValidator(data){
	try {
		let _dataobject;
		if(typeof(data) === 'object'){
			_dataobject = LODASH.clone(data);
		} else {
			_dataobject = JSON.parse(data.toString());
		}
		if(Array.isArray(_dataobject) && (_dataobject.length > 0)){
			for(let i = 0; i < _dataobject.length; i++){
				if(typeof(_dataobject[i]) !== 'object'){
					return 'err';
				}
			}
			return _dataobject;
		} else {
			return 'err';
		}
	} catch(e){
		return 'err';
	}
}

//валидатор маршрута /v3/data
function DataValidator(data){
	try {
		let _dataobject;
		if(typeof(data) === 'object'){
			_dataobject = LODASH.clone(data);
		} else {
			_dataobject = JSON.parse(data.toString());
		}
		return ObjectShema(_dataobject);	
	} catch(e){
		return(e.message);
	}
}

//формат объекта БД
var _object_db = {
	type:null, 
	guid:null, 
	description:null, 
	version:null, 
	isdeleted:null, 
	forbidden:null, 
	parent:null, 
	isfolder:null, 
	date:null, 
	isactive:null, 
	requisites:null, 
	arrays:null, 
	multilines:null,
	files: null
};

//разрешенные типы и их валидация
var types = {
	'Строка': function(_s){
		if(typeof(_s) === 'string'){
			return true;
		} else {
			return false;
		}
	},
	'Число': function(_s){
		if(typeof(_s) === 'number'){
			return true;
		} else {
			return false;
		}
	},
	'Дата': function(_s){
		if((typeof(_s) === 'string') && (/^\d\d\d\d-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])((.\d\d\dZ)|())$/.test(_s))){
			return true;
		} else {
			return false;
		}
	},
	'Булево': function(_s){
		if(typeof(_s) === 'boolean'){
			return true;
		} else {
			return false;
		}
	},
	'Справочник': function(_s){
		if(typeof(_s) === 'string'){
			return true;
		} else {
			return false;
		}
	},
	'Документ': function(_s){
		if(typeof(_s) === 'string'){
			return true;
		} else {
			return false;
		}
	},
	'Перечисление': function(_s){
		if(typeof(_s) === 'string'){
			return true;
		} else {
			return false;
		}
	}
};

//схема объекта бд, вложенные функции валидации синхронны и ловятся в try..catch данной функции
function ObjectShema(d){
	try{
		let _d = LODASH.clone(_object_db);
		if(typeof(d) !== 'object'){
			throw new Error('OBJECT->Тип не является object');
		}
		d.version = FUNCTIONS.version();
		for(const key in d){
			switch(key.toLowerCase()){
				case 'type':
					if(typeof(d[key]) !== 'string'){
						throw new Error('OBJECT->Тип поля '+key+'('+d[key]+') не является string');
					}
					if(typeof(types[d[key].split('.')[0]]) !== 'function'){
						throw new Error('OBJECT->Разрешены только типы: '+(LODASH.keys(types)).join(', '));
					}
					_d[key.toLowerCase()] = d[key];
					break;
				case 'guid':
					if(typeof(d[key]) !== 'string'){
						throw new Error('OBJECT->Тип поля '+key+'('+d[key]+') не является string');
					}
					_d[key.toLowerCase()] = d[key];
					break;
				case 'description':
					if(typeof(d[key]) !== 'string'){
						throw new Error('OBJECT->Тип поля '+key+'('+d[key]+') не является string');
					}
					_d[key.toLowerCase()] = d[key];
					break;
				case 'version':
					if(typeof(d[key]) !== 'string'){
						throw new Error('OBJECT->Тип поля '+key+'('+d[key]+') не является string');
					}
					_d[key.toLowerCase()] = d[key];
					break;
				case 'isdeleted':
					if(typeof(d[key]) !== 'boolean'){
						throw new Error('OBJECT->Тип поля '+key+'('+d[key]+') не является boolean');
					}
					_d[key.toLowerCase()] = d[key];
					break;
				case 'forbidden':
					if(typeof(d[key]) !== 'boolean'){
						throw new Error('OBJECT->Тип поля '+key+'('+d[key]+') не является boolean');
					}
					_d[key.toLowerCase()] = d[key];
					break;
				case 'parent':
					if(typeof(d[key]) !== 'string'){
						throw new Error('OBJECT->Тип поля '+key+'('+d[key]+') не является string');
					}
					_d[key.toLowerCase()] = d[key];
					break;
				case 'isfolder':
					if(typeof(d[key]) !== 'boolean'){
						throw new Error('OBJECT->Тип поля '+key+'('+d[key]+') не является boolean');
					}
					_d[key.toLowerCase()] = d[key];
					break;
				case 'date':
					if(!/(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})/.test(d[key])){
						throw new Error('OBJECT->Тип поля '+key+'('+d[key]+') не является YYYY-MM-DDTHH:MM:SS string');
					}
					_d[key.toLowerCase()] = d[key];
					break;
				case 'isactive':
					if(typeof(d[key]) !== 'boolean'){
						throw new Error('OBJECT->Тип поля '+key+'('+d[key]+') не является boolean');
					}
					_d[key.toLowerCase()] = d[key];
					break;
				case 'requisites': 
					if((typeof(d[key]) !== 'object') || (!Array.isArray(d[key]))){
						throw new Error('OBJECT->Тип поля '+key+'('+d[key]+') не является array');
					} else {
						_d[key.toLowerCase()] = RequisiteShema(d[key]);
					}
					break;
				case 'arrays':
					if((typeof(d[key]) !== 'object') || (!Array.isArray(d[key]))){
						throw new Error('OBJECT->Тип поля '+key+'('+d[key]+') не является array');
					} else {
						_d[key.toLowerCase()] = ArrayShema(d[key]);
					}
					break;
				case 'multilines':
					if((typeof(d[key]) !== 'object') || (!Array.isArray(d[key]))){
						throw new Error('OBJECT->Тип поля '+key+'('+d[key]+') не является array');
					} else {
						_d[key.toLowerCase()] = MultilineShema(d[key]);
					}
					break;
				case 'files': 
					if((typeof(d[key]) !== 'object') || (!Array.isArray(d[key]))){
						throw new Error('OBJECT->Тип поля '+key+'('+d[key]+') не является array');
					} else {
						_d[key.toLowerCase()] = FilesShema(d[key]);
					}
					break;
				default:
					throw new Error('OBJECT->Неизвестное поле объекта '+key);
			}
		}
		for(const key in _d){
			if(_d[key] === null){
				delete _d[key];
			}
		}
		if((typeof(_d.type) === 'undefined') || (typeof(_d.guid) === 'undefined')){
			throw new Error('OBJECT->Не задано обязательное поле type/guid!');
		}
		if(_d.forbidden === true){	//удаляю forbidden документы
			_d = {
				type:_d.type, 
				guid:_d.guid,
				version:_d.version,
				forbidden:_d.forbidden
			};
		}
		return _d; 
	} catch(e){
		return e.message;
	}
}

//type files
function FilesShema(d){
	let _d = [];
	if(d.length !== 0){
		for(let i=0; i < d.length; i++){
			let _tmp = {
				name: null,
				requisites: null
			};
			if(typeof(d[i]) !== 'object'){
				throw new Error('FILES->Тип не является object');
			}
			for(const key in d[i]){
				switch(key.toLowerCase()){
					case 'name':
						if(typeof(d[i][key]) !== 'string'){
							throw new Error('FILES->Тип поля '+key+'('+d[i][key]+') не является string');
						}
						_tmp[key.toLowerCase()] = d[i][key];
						break;
					case 'requisites':
						if((typeof(d[i][key]) !== 'object') || (!Array.isArray(d[i][key]))){
							throw new Error('FILES->Тип поля '+key+'('+d[i][key]+') не является array');
						} else {
							_tmp[key.toLowerCase()] = RequisiteShema(d[i][key]);
						}
						break;
					default:
						throw new Error('FILES->Неизвестное поле объекта '+key);
				}
			}
			for(const key in _tmp){
				if(_tmp[key] === null){
					delete _tmp[key];
				}
			}
			if((typeof(_tmp.name) === 'undefined') || (typeof(_tmp.requisites) === 'undefined')){
				throw new Error('FILES->Не задано обязательное поле name/requisites!');
			}
			_d.push(_tmp);
		}
		return _d;
	} else {
		throw new Error('FILES->Передан пустой массив!');
	}
}

//type requisites
function RequisiteShema(d){
	let _d = [];
	let t_id = [];	//гарант уникальности id в пределах объекта requisites
	if(d.length !== 0){
		for(let i=0; i < d.length; i++){
			let _tmp = {
				type: null,
				id: null,
				name: null,
				value: null
			};
			if(typeof(d[i]) !== 'object'){
				throw new Error('REQUISITE->Тип не является object');
			}
			for(const key in d[i]){
				let _test;
				switch(key.toLowerCase()){
					case 'type':
						if(typeof(d[i][key]) !== 'string'){
							throw new Error('REQUISITE->Тип поля '+key+'('+d[i][key]+') не является string');
						}
						if(typeof(types[d[i][key].split('.')[0]]) !== 'function'){
							throw new Error('REQUISITE->Разрешены только типы: '+(LODASH.keys(types)).join(', '));
						}
						_tmp[key.toLowerCase()] = d[i][key];
						break;
					case 'id':
						if(typeof(d[i][key]) !== 'string'){
							throw new Error('REQUISITE->Тип поля '+key+'('+d[i][key]+') не является string');
						}
						if(t_id.indexOf(d[i][key]) !== -1){
							throw new Error('REQUISITE->Тип поля '+key+'('+d[i][key]+') не является уникальным!');
						} else {
							t_id.push(d[i][key]);
						}
						_tmp[key.toLowerCase()] = d[i][key];
						break;
					case 'name':
						if(typeof(d[i][key]) !== 'string'){
							throw new Error('REQUISITE->Тип поля '+key+'('+d[i][key]+') не является string');
						}
						_tmp[key.toLowerCase()] = d[i][key];
						break;
					case 'value':
						_test = types[_tmp.type.split('.')[0]](d[i][key]);
						if(!_test){
							throw new Error('REQUISITE->Тип поля '+key+'('+d[i][key]+') не соответствует заданному type');
						}
						_tmp[key.toLowerCase()] = d[i][key];
						break;
					default:
						throw new Error('REQUISITE->Неизвестное поле объекта '+key);
				}
			}
			for(const key in _tmp){
				if(_tmp[key] === null){
					delete _tmp[key];
				}
			}
			if((typeof(_tmp.type) === 'undefined') || (typeof(_tmp.id) === 'undefined')){
				throw new Error('REQUISITE->Не задано обязательное поле type/id!');
			}
			_d.push(_tmp);
		}
		return _d;
	} else {
		throw new Error('REQUISITE->Передан пустой массив!');
	}
}

//type arrays
function ArrayShema(d){
	let _d = [];
	let t_id = [];	//гарант уникальности id в пределах объекта arrays
	if(d.length !== 0){
		for(let i=0; i < d.length; i++){
			let _tmp = {
				type: null,
				id: null,
				name: null,
				values: null
			};
			if(typeof(d[i]) !== 'object'){
				throw new Error('ARRAY->Тип не является object');
			}
			for(const key in d[i]){
				switch(key.toLowerCase()){
					case 'type':
						if(typeof(d[i][key]) !== 'string'){
							throw new Error('ARRAY->Тип поля '+key+'('+d[i][key]+') не является string');
						}
						if(typeof(types[d[i][key].split('.')[0]]) !== 'function'){
							throw new Error('ARRAY->Разрешены только типы: '+(LODASH.keys(types)).join(', '));
						}
						_tmp[key.toLowerCase()] = d[i][key];
						break;
					case 'id':
						if(typeof(d[i][key]) !== 'string'){
							throw new Error('ARRAY->Тип поля '+key+'('+d[i][key]+') не является string');
						}
						if(t_id.indexOf(d[i][key]) !== -1){
							throw new Error('ARRAY->Тип поля '+key+'('+d[i][key]+') не является уникальным!');
						} else {
							t_id.push(d[i][key]);
						}
						_tmp[key.toLowerCase()] = d[i][key];
						break;
					case 'name':
						if(typeof(d[i][key]) !== 'string'){
							throw new Error('ARRAY->Тип поля '+key+'('+d[i][key]+') не является string');
						}
						_tmp[key.toLowerCase()] = d[i][key];
						break;
					case 'values':
						if(!Array.isArray(d[i][key])){
							throw new Error('ARRAY->Тип поля '+key+'('+d[i][key]+') не является array');
						}
						for(let j=0; j<d[i][key].length; j++){
							const _test = types[_tmp.type.split('.')[0]](d[i][key][j]);
							if(!_test){
								throw new Error('ARRAY->Тип поля '+key+'('+d[i][key][j]+') не соответствует заданному type');
							}
						}
						_tmp[key.toLowerCase()] = d[i][key];
						break;
					default:
						throw new Error('ARRAY->Неизвестное поле объекта '+key);
				}
			}
			for(const key in _tmp){
				if(_tmp[key] === null){
					delete _tmp[key];
				}
			}
			if((typeof(_tmp.type) === 'undefined') || (typeof(_tmp.id) === 'undefined')){
				throw new Error('ARRAY->Не задано обязательное поле type/id!');
			}
			_d.push(_tmp);
		}
		return _d;
	} else {
		throw new Error('ARRAY->Передан пустой массив!');
	}
}

//type multilines
function MultilineShema(d){
	let _d = [];
	let t_id = [];	//гарант уникальности id в пределах объекта multilines
	if(d.length !== 0){
		for(let i=0; i < d.length; i++){
			let _tmp = {
				id: null,
				lines: null
			};
			if(typeof(d[i]) !== 'object'){
				throw new Error('MULTILINE->Тип не является object');
			}
			for(const key in d[i]){
				switch(key.toLowerCase()){
					case 'id':
						if(typeof(d[i][key]) !== 'string'){
							throw new Error('MULTILINE->Тип поля '+key+'('+d[i][key]+') не является string');
						}
						if(t_id.indexOf(d[i][key]) !== -1){
							throw new Error('MULTILINE->Тип поля '+key+'('+d[i][key]+') не является уникальным!');
						} else {
							t_id.push(d[i][key]);
						}
						_tmp[key.toLowerCase()] = d[i][key];
						break;
					case 'lines':
						if(!Array.isArray(d[i][key])){
							throw new Error('MULTILINE->Тип поля '+key+'('+d[i][key]+') не является array');
						}
						_tmp[key.toLowerCase()] = LineShema(d[i][key]);
						break;
					default:
						throw new Error('MULTILINE->Неизвестное поле объекта '+key);
				}
			}
			for(const key in _tmp){
				if(_tmp[key] === null){
					delete _tmp[key];
				}
			}
			if(typeof(_tmp.id) === 'undefined'){
				throw new Error('MULTILINE->Не задано обязательное поле id!');
			}
			_d.push(_tmp);
		}
		return _d;
	} else {
		throw new Error('MULTILINE->Передан пустой массив!');
	}
}

//type lines
function LineShema(d){
	let _d = [];
	if(d.length !== 0){
		for(let i=0; i < d.length; i++){
			let _tmp = {
				requisites: null,
				arrays: null
			};
			if(typeof(d[i]) !== 'object'){
				throw new Error('LINE->Тип не является object');
			}
			for(const key in d[i]){
				switch(key.toLowerCase()){
					case 'requisites':
						if(!Array.isArray(d[i][key])){
							throw new Error('LINE->Тип поля '+key+'('+d[i][key]+') не является array');
						}
						_tmp[key.toLowerCase()] = RequisiteShema(d[i][key]);
						break;
					case 'arrays':
						if(!Array.isArray(d[i][key])){
							throw new Error('LINE->Тип поля '+key+'('+d[i][key]+') не является array');
						}
						_tmp[key.toLowerCase()] = ArrayShema(d[i][key]);
						break;
					default:
						throw new Error('LINE->Неизвестное поле объекта '+key);
				}
			}
			for(const key in _tmp){
				if(_tmp[key] === null){
					delete _tmp[key];
				}
			}
			_d.push(_tmp);
		}
		return _d;
	} else {
		throw new Error('LINE->Передан пустой массив!');
	}
}

//поля, которые нужно обнулить в объекте
function Unsetter(data){
	let result = {};
	for(const key in _object_db){
		if(typeof(data[key]) === 'undefined'){
			result[key] = null;
		}
	}
	if(LODASH.isEqual(result, {})){
		return;
	} else {
		return result;
	}
}

//проверка наличия электронного рецепта в чеке 
function UseEHR(data){
	if(data.type !== "Документ.Чек"){
		return false;
	} else if(CONFIG.ehr && CONFIG.ehr.server){
		for(const key in data.requisites){
			if(data.requisites[key].id === 'bSaleByE_Prescription'){
				if(data.requisites[key].value === true){
					return true;
				} else {
					return false;
				}
			}
		}
		return false;
	}else {
		return false;
	}
}

//проверка на то, что чек возвратный 
function CheckIsReturn(data){
	for(const key in data.requisites){
		if(data.requisites[key].id === 'CheckIsReturn'){
			if(data.requisites[key].value === true){
				return true;
			} else {
				return false;
			}
		}
	}
	return false;
}

module.exports.query = QueryValidator;
module.exports.data = DataValidator;
module.exports.unset = Unsetter;
module.exports.useehr = UseEHR;
module.exports.isreturn = CheckIsReturn;
module.exports.bigdata = BigDataValidator;
