/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  APP.ROUTERS.RECIPE
 *	Маршрут для /v3/recipe
 *
 */

"use strict"

//подгружаемые библиотеки
var EXPRESS = require('express'),
	CONFIG = require('config'),
	PATH = require('path');
	
//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	EHR = require(PATH.join(__dirname, 'module.ehr.js'));


var recipe = EXPRESS.Router();

recipe.all('*', function (req, res){
	//проверяю что параметры переданы
	(new Promise(function (resolve, reject){
		if(CONFIG.bantimeout){
			if(
				(res.locals.permission['all'] && (res.locals.permission['all'].indexOf('r') !== -1)) ||
				(res.locals.permission['recipe'] && (res.locals.permission['recipe'].indexOf('r') !== -1))
			){
				return resolve('next');
			} else {
				res.status(403).json({
					status: "error",
					message: "Permission denied"
				});
				return reject('close');
			}
		} else {
			return resolve('next');
		}
	})).then(function(){
		return new Promise(function (resolve, reject){
			if(CONFIG.ehr && CONFIG.ehr.server){
				return resolve('next');
			} else {
				res.status(500).json({
					status: "error",
					message: "The server is not configured in the settings."
				});
				return reject('close');
			}
		});
	}).then(function(){
		return new Promise(function (resolve, reject){ 
			if(typeof(req.query.id) === 'undefined'){
				res.status(400).json({status:"error", message:'Bad Request'});
				return reject('close');
			} else {
				return resolve('next');
			}
		});
	}).then(function(){
		//авторизация
		return new Promise(function (resolve, reject){
			EHR.auth(res.locals).then(function(value){
				return resolve(value);
			}).catch(function(error){
				if(error === 'close'){
					res.status(500).json({status:"error", message:'Server EHR authorization error!'});
				}
				return reject(error);
			});
		});
	}).then(function(){
		//поиск пациента
		return new Promise(function (resolve, reject){
			EHR.find(res.locals, req.query.id).then(function(value){
				return resolve(value);
			}).catch(function(error){
				if(error === 'close'){
					res.status(500).json({status:"error", message:'Server EHR finder error!'});
				}
				return reject(error);
			});
		});
	}).then(function(val){ 
		//ответ сервера
		if(val === 'NotFound'){ //не найден
			return res.status(204).send();
		} else {
			if(res.locals.acceptgzip){ 
				res.set({'Content-Type': 'application/json; charset=utf-8','Content-Encoding': 'gzip'});
				return res.locals.gzipper.end(res.locals.ehranswer);
			} else{
				res.set({'Content-Type': 'application/json; charset=utf-8'});
				return res.status(200).send(res.locals.ehranswer);
			}
		}
	}).catch(function(error){
		if(error !== 'close'){
			LOGGER.error('Неизвестная ошибка запроса: '+error);
			return res.status(500).json({status:"error", message:'Internal Server Error'});
		}
		return;
	});
}); 

module.exports = recipe;