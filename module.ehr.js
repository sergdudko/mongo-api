/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  APP.EHR
 *	Работа с сервером электронных рецептов
 *
 */

"use strict"

//подгружаемые библиотеки
var CONFIG = require('config'),
	LODASH = require('lodash'),
	PATH = require('path');

//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js')),
	MONGODB = require(PATH.join(__dirname, 'module.mongodb.js')),
	VALIDATORS = require(PATH.join(__dirname, 'module.validators.js')),
	SENDMAIL = require(PATH.join(__dirname, 'module.sendmail.js'));

//глобальные переменные	
var run = 0;
var token = {
	key: null,
	expire: null,
	timestamp: null
};

//авторизация в EHR, вернет в res.locals.ehrkey и resolve('next') токен авторизации при успехе, при неудаче reject('close')
function auth(locals){
	return new Promise(function (resolve, reject){
		if(CONFIG.ehr.usenceu){	//при использовании НЦЭУ авторизацию опускаем
			resolve('next');
		}else if( (typeof(token) === 'object') &&	//проверяем, что токен не истек (на всякий случай от expire отниму минуту)
			(typeof(token.key) === 'string') && 
			(typeof(token.expire) === 'number') && 
			(typeof(token.timestamp) === 'number') && 
			((token.timestamp + ((token.expire - 60)*1000)) > Date.now())
		){
			if(typeof(locals) === 'object'){
				locals.ehrkey = token.key;
			}
			resolve('next');
		} else {
			if(CONFIG.ehr.login && CONFIG.ehr.password && CONFIG.ehr.server && CONFIG.ehr.auth){
				let data = "grant_type=client_credentials&client_id="+CONFIG.ehr.login+"&client_secret="+CONFIG.ehr.password;
				let headers = { "Content-Type": "application/x-www-form-urlencoded" };
				FUNCTIONS.rest({url:CONFIG.ehr.auth, method:"POST", data:data, datatype:"string", headers:headers}).then(function(_resolve){
					if(_resolve[0] === 200){
						try{
							let result = JSON.parse(_resolve[1]);
							token = {
								key: result.access_token,
								expire: result.expires_in,
								timestamp: Date.now()
							};
							if(typeof(locals) === 'object'){
								locals.ehrkey = result.access_token;
							}
							resolve('next');
						} catch(e){
							LOGGER.error('EHR->Ошибка преобразования: '+e);
							token = {key: null, expire: null, timestamp: null}; //обнуляю токен на всякий случай
							reject('close');
						}
					} else {
						LOGGER.error('EHR->Сервер авторизации недоступен, статус: '+_resolve[1]);
						token = {key: null, expire: null, timestamp: null}; //обнуляю токен на всякий случай
						reject('close');
					}
				}).catch(function(error){
					LOGGER.error('EHR->Ошибка REST-запроса: '+error);
					token = {key: null, expire: null, timestamp: null}; //обнуляю токен на всякий случай
					reject('close');
				});
			} else {
				LOGGER.error('EHR->Некорректные настройки сервера!');
				reject('close');
			}
		}
	});
}

//поиск пациента "Выполнение операции поиска рецептов с указанием статуса"
function find(locals, id){
	return new Promise(function (resolve, reject){
		let headers;
		if(CONFIG.ehr.usenceu){
			headers = { "Content-Type": "application/json" };
		} else {
			headers = { "Content-Type": "application/json", "Authorization":"Bearer "+locals.ehrkey };
		}
		let link = CONFIG.ehr.server+"/MedicationPrescription/$by-card?patientIdentifier="+id+"&status=active,completed";
		FUNCTIONS.rest({url:link, method:"GET", headers:headers}).then(function(_resolve){
			switch(_resolve[0]){
				case 200:
					try{ LOGGER.debug(_resolve[1]);
						normaliseFindResult(JSON.parse(_resolve[1])).then(function(val){
							locals.ehranswer = JSON.stringify(val);
							resolve('next');
						}).catch(function(_reject){
							LOGGER.warn('Ошибка преобразования ответа EHR(find): '+_reject);
							reject('close');
						});
					} catch(e){
						LOGGER.error('EHR->Ошибка преобразования: '+e);
						reject('close');
					}
					break;
				case 404:
					resolve('NotFound');
					break;
				default:
					LOGGER.error('EHR->Сервер недоступен, статус: '+_resolve[0]+' '+_resolve[1]);
					reject('close');
					break;
			}
		}).catch(function(error){
			LOGGER.error('EHR->Ошибка REST-запроса: '+error);
			reject('close');
		});
	});
}

//приводим объект к нормальному (старому) виду
function normaliseFindResult(data){
	return new Promise(function (resolve, reject){
		FUNCTIONS.fhirparser(data).then(function(val){
			if((typeof(val.Patient) === 'object') && (val.Patient.active === true)){
				let obj = {	//пациент
					"Patient": {
						"Id": "",
						"Name": "",
						"Gender": "",
						"BirthDate": "",
						"HomeAddress": ""
					},
					"Prescriptions": []
				}
				if(typeof(val.Patient.id) === 'string'){ obj.Patient.Id = val.Patient.id; }
				if(typeof(val.Patient.gender) === 'string'){ obj.Patient.Gender = val.Patient.gender; }
				if(typeof(val.Patient.BirthDate) === 'string'){ obj.Patient.BirthDate = val.Patient.BirthDate; }
				if(Array.isArray(val.Patient.name)){
					for(let i = 0; i < val.Patient.name.length; i++){
						if(val.Patient.name[i].use === 'official'){
							if(Array.isArray(val.Patient.name[i].family)){
								obj.Patient.Name = val.Patient.name[i].family.join(" ") + " ";
							}
							if(Array.isArray(val.Patient.name[i].given)){
								obj.Patient.Name += val.Patient.name[i].given.join(" ")
							}
						}
					}
				}
				if(Array.isArray(val.Patient.address)){
					for(let i = 0; i < val.Patient.address.length; i++){
						if(val.Patient.address[i].use === 'home'){
							if(typeof(val.Patient.address[i].text) === 'string'){
								obj.Patient.HomeAddress = val.Patient.address[i].text;
							}
						}
					}
				}
				let tempDispense = {};	//формируем объект продаж с ключом MedicationPrescription/uid_рецепта
				if(typeof(val["Bundle.MedicationDispense"]) === 'object'){
					for(const uid in val["Bundle.MedicationDispense"]){
						if(
							(typeof(val["Bundle.MedicationDispense"][uid]) === 'object') && 
							(Array.isArray(val["Bundle.MedicationDispense"][uid].authorizingPrescription)) && 
							(typeof(val["Bundle.MedicationDispense"][uid].authorizingPrescription[0]) === 'object') &&
							(typeof(val["Bundle.MedicationDispense"][uid].authorizingPrescription[0].reference) === 'string')
						){
							let t = val["Bundle.MedicationDispense"][uid].authorizingPrescription[0].reference.split('/');
							if(!Array.isArray(tempDispense[t[1]])){
								tempDispense[t[1]] = [];
							}
							let _data = {
								"Date": "",
								"Quantity": "",
								"Medication": {
									"Code": "",
									"MNN": ""
								}
							};
							if(typeof(val["Bundle.MedicationDispense"][uid].whenHandedOver) === 'string') { _data.Date = val["Bundle.MedicationDispense"][uid].whenHandedOver; }
							if((typeof(val["Bundle.MedicationDispense"][uid].quantity) === 'object') && (typeof(val["Bundle.MedicationDispense"][uid].quantity.value) === 'number')) { _data.Quantity = val["Bundle.MedicationDispense"][uid].quantity.value; }
							if(typeof(val["Bundle.MedicationDispense"][uid].contained) === 'object'){
								for(const i in val["Bundle.MedicationDispense"][uid].contained){
									if((typeof(val["Bundle.MedicationDispense"][uid].contained[i]) === 'object') && (val["Bundle.MedicationDispense"][uid].contained[i].resourceType === "Medication")){
										if(
											(typeof(val["Bundle.MedicationDispense"][uid].contained[i].code) === 'object') &&
											(Array.isArray(val["Bundle.MedicationDispense"][uid].contained[i].code.coding)) &&
											(typeof(val["Bundle.MedicationDispense"][uid].contained[i].code.coding[0]) === 'object')
										){
											if(val["Bundle.MedicationDispense"][uid].contained[i].code.coding[0].system === "http://www.pharma.by/Справочник.МеждународноеНаименование") {
												_data.Medication.MNN = val["Bundle.MedicationDispense"][uid].contained[i].code.coding[0].code;
											}
											if(val["Bundle.MedicationDispense"][uid].contained[i].code.coding[0].system === "http://www.pharma.by/Справочник.Дозировка"){
												_data.Medication.Code = val["Bundle.MedicationDispense"][uid].contained[i].code.coding[0].code;
											}
										} 
									}
								}
							}
							tempDispense[t[1]].push(_data);
						}
					}
				}
				let tempSignature = {}; //объект подписей ЭЦП
				if(typeof(val["Bundle.Provenance"]) === 'object'){
					for(const uid in val["Bundle.Provenance"]){
						if(
							Array.isArray(val["Bundle.Provenance"][uid].signature) && 
							Array.isArray(val["Bundle.Provenance"][uid].target) && 
							(typeof(val["Bundle.Provenance"][uid].target[0]) === 'object') && 
							(typeof(val["Bundle.Provenance"][uid].target[0].reference) === 'string')
						){
							let t = val["Bundle.Provenance"][uid].target[0].reference.split('/');
							tempSignature[t[1]] = true;
						}
					}
				}
				if(typeof(val["Bundle.MedicationPrescription"]) === 'object'){
					for(const uid in val["Bundle.MedicationPrescription"]){	//формируем Prescriptions
						if((typeof(val["Bundle.MedicationPrescription"][uid]) === 'object') && (val["Bundle.MedicationPrescription"][uid].status === 'active')){
							let _data = {
								Id:"",
								DateWritten:"",
								Quantity:"",
								DosageInstruction:[],
								Medication:{
									Code:'',
									MNN:'',
								},
								Dispenses:[],
								Signature: false
							};
							if(typeof(val["Bundle.MedicationPrescription"][uid].id) === 'string'){ _data.Id = val["Bundle.MedicationPrescription"][uid].id; }
							if(typeof(val["Bundle.MedicationPrescription"][uid].dateWritten) === 'string'){ _data.DateWritten = val["Bundle.MedicationPrescription"][uid].dateWritten; }
							if(
								(typeof(val["Bundle.MedicationPrescription"][uid].dispense) === 'object') &&
								(typeof(val["Bundle.MedicationPrescription"][uid].dispense.quantity) === 'object') &&
								(typeof(val["Bundle.MedicationPrescription"][uid].dispense.quantity.value) === 'number')
							){ _data.Quantity = val["Bundle.MedicationPrescription"][uid].dispense.quantity.value; }
							if(tempSignature[uid] === true){ _data.Signature = true; }
							if(typeof(val["Bundle.MedicationPrescription"][uid].contained) === 'object'){
								for(const i in val["Bundle.MedicationPrescription"][uid].contained){
									if(
										(typeof(val["Bundle.MedicationPrescription"][uid].contained[i]) === 'object') && 
										(val["Bundle.MedicationPrescription"][uid].contained[i].resourceType === "Medication")
									){
										if(
											(typeof(val["Bundle.MedicationPrescription"][uid].contained[i].code) === 'object') &&
											(Array.isArray(val["Bundle.MedicationPrescription"][uid].contained[i].code.coding)) &&
											(typeof(val["Bundle.MedicationPrescription"][uid].contained[i].code.coding[0]) === 'object')
										){
											if(val["Bundle.MedicationPrescription"][uid].contained[i].code.coding[0].system === "http://www.pharma.by/Справочник.МеждународноеНаименование"){
												_data.Medication.MNN = val["Bundle.MedicationPrescription"][uid].contained[i].code.coding[0].code;
											}
											if(val["Bundle.MedicationPrescription"][uid].contained[i].code.coding[0].system === "http://www.pharma.by/Справочник.Дозировка"){
												_data.Medication.Code = val["Bundle.MedicationPrescription"][uid].contained[i].code.coding[0].code;
											}
										}
									}
								}
							}
							if(typeof(val["Bundle.MedicationPrescription"][uid].dosageInstruction) === 'object'){
								for(const i in val["Bundle.MedicationPrescription"][uid].dosageInstruction){
									if(
										(typeof(val["Bundle.MedicationPrescription"][uid].dosageInstruction[i]) === 'object') && 
										(typeof(val["Bundle.MedicationPrescription"][uid].dosageInstruction[i].text) === 'string')
									){
										_data.DosageInstruction.push(val["Bundle.MedicationPrescription"][uid].dosageInstruction[i].text);
									}
								}
							}
							if(Array.isArray(tempDispense[uid])){
								_data.Dispenses = LODASH.clone(tempDispense[uid]);
							}
							obj.Prescriptions.push(_data);
						}
					}
				}
				resolve(LODASH.clone(obj));
			} else {
				resolve('Not Found');
			}
		}).catch(function(error){
			reject(new Error('EHR->FhirParser error: '+error));
		});
	});
}

//генерация линка для EHR
function linkForEhrGenerator(){
	if(CONFIG.ehr.myserver){
		return CONFIG.ehr.myserver+'/Документ.Чек';
	}else{
		return 'https://itpharma.by/Документ.Чек';
	}
}

//приводим объект чека к виду, требуемому ehr
function normaliseSendData(data){
	return new Promise(function(resolve){
		let _param = {}; //объект для извлечения данных, необходимых для передачи ehr, из чека
		_param.guid = data.guid;
		_param.saledate = data.date.substr(0,10);
		for(const key in data.requisites){
			switch (data.requisites[key].id){
				case 'Location_address':
					_param.locationaddr = data.requisites[key].value;
					break;
				case 'Location_name':
					_param.locationname = data.requisites[key].value;
					break;
				case 'Location_SOATO':
					_param.soato = data.requisites[key].value;
					break;
			}
		}
		_param.array = [];
		_param.arrayFinal = [];
		function startAnalize(object){
			let _tmp = {};
			for(const key in object){
				switch (object[key].id){
					case 'PrescriptionId':
						_tmp.prescriptionid = object[key].value;
						break;
					case 'PatientId':
						_tmp.patientid = object[key].value;
						break;
					case 'MedicationCode':
						_tmp.medicationcode = object[key].value;
						break;
					case 'MedicationCode_ReallySale':
						_tmp.medicationcodereally = object[key].value;
						break;
					case 'QtyDose':
						_tmp.quantity = object[key].value;
						break;
					case 'IsFullSaleDose':
						_tmp.isfullsaledose = object[key].value;
						break;
					case 'PositionsUid':
						_tmp.positionuid = object[key].value;
						break;
				}
			}
			if( (typeof(_tmp.prescriptionid) !== 'undefined') &&
				(typeof(_tmp.patientid) !== 'undefined') &&
				(typeof(_tmp.medicationcode) !== 'undefined') &&
				(typeof(_tmp.quantity) !== 'undefined') &&
				(typeof(_tmp.positionuid) !== 'undefined')
			){
				if(_tmp.isfullsaledose === true){	//костыль для сортировки очередности (сначала регистрируем обычные позиции, потом с блокировкой рецепта)
					_param.arrayFinal.push(LODASH.clone(_tmp));
				} else {
					_param.array.push(LODASH.clone(_tmp));
				}
			}
			_tmp = null;
		}
		for(const key in data.multilines){
			switch (data.multilines[key].id){
				case 'ПозицииЧека':
					for(const key2 in data.multilines[key].lines){
						startAnalize(data.multilines[key].lines[key2].requisites);
					}
					break;
			}
		}
		//объединяем 2 массива в верной очередности
		_param.array = _param.array.concat(_param.arrayFinal);
		let _finalobj = {};
		for(let i=0; i < _param.array.length; i++){
			//стандартный документ
			let _data = {
				"resourceType": "Parameters",
				"parameter": [
					{
						"name": "medicationDispense",
						"resource": {
							"resourceType": "MedicationDispense",
							"identifier": [
								{
									"system": linkForEhrGenerator(),
									"value": _param.guid+'/'+_param.array[i].positionuid
								}
							],
							"extension": [
								{
									"url": "http://fhir.org/fhir/StructureDefinition/by-finalDispense",
									"valueBoolean": (function(){ if(_param.array[i].isfullsaledose === true){return true;} else{return false;} })()
								}
							],
							"contained": [
								{
									"id": "internalMedication1",
									"code": {
										"coding": [
											{
												"system": "http://www.pharma.by/Справочник.Дозировка",
												"code": _param.array[i].medicationcode
											}
										]
									},
									"resourceType": "Medication",
									"isBrand": true
								},
								{
									"name": _param.locationname,
									"address": {
										"text": _param.locationaddr,
										"extension": [
											{
												"url": "http://fhir.org/fhir/StructureDefinition/by-AddressType",
												"valueCodeableConcept": {
													"coding": [
														{
															"system": "http://hl7.org/fhir/vs/by-address-type",
															"code": "location"
														}
													]
												}
											},
											{
												"url": "http://fhir.org/fhir/StructureDefinition/by-soatoCode",
												"valueCodeableConcept": {
													"coding": [
														{
															"system": "http://hl7.org/fhir/vs/by-soato-code",
															"code": _param.soato
														}
													]
												}
											}
										]
									},
									"resourceType": "Location"
								}
							],
							"status": "completed",
							"patient": {
								"reference": "Patient/"+_param.array[i].patientid
							},
							"authorizingPrescription": [
								{
									"reference": "MedicationPrescription/"+_param.array[i].prescriptionid
								}
							],
							"quantity": {
								"value": _param.array[i].quantity
							},
							"whenHandedOver": _param.saledate,
							"medication": {
								"reference": "#internalMedication1"
							}
						}
					}
				]
			};
			//добавляю код замены, если существует
			if(_param.array[i].medicationcodereally){
				_data.parameter[0].resource.substitution = {
					"type": {
						"coding": [
							{
								"system": "http://hl7.org/fhir/v3/vs/ActSubstanceAdminSubstitutionCode",
								"code": _param.array[i].medicationcodereally
							}
						]
					}
				};
			}
			_finalobj[_param.array[i].positionuid] = LODASH.clone(_data);
			_data = null;
		}
		resolve(_finalobj);
	});
}

//поиск карты клиента по идентификатору продажи
function checkCardnumOnDatabase(client, checkAndPositionUid = ''){
	return new Promise(function(res, rej){
		let checkUid = checkAndPositionUid.split('/')[0];
		let posUid = checkAndPositionUid.split('/')[1];
		MONGODB.find(client.db(CONFIG.mongodb.database).collection('Data'), VALIDATORS.query({find:{type: "Документ.Чек", guid:checkUid}})).then(function(_data){
			try{
				if((typeof(_data) === 'object') && (typeof(_data[0]) === 'object') && Array.isArray(_data[0].multilines)){
					for(let i = 0; i < _data[0].multilines.length; i++){
						if(_data[0].multilines[i].id === 'ПозицииЧека'){
							let lines = _data[0].multilines[i].lines;
							if(Array.isArray(lines)){
								for(let j = 0; j < lines.length; j++){
									let requisites = lines[j].requisites;
									if(Array.isArray(requisites)){
										let flg = false;
										let cardId;
										for(let k = 0; k < requisites.length; k++){
											if((typeof(requisites[k]) === 'object') && (requisites[k].id === 'PositionsUid') && (requisites[k].value === posUid)){
												flg = true;
												if((flg === true) && cardId){
													return res(cardId);
												}
											}else if((typeof(requisites[k]) === 'object') && (requisites[k].id === 'MedCardId')){
												cardId = requisites[k].value;
												if((flg === true) && cardId){
													return res(cardId);
												}
											}
										}
									}
								}
							}
						}
					}
				}
				return res();
			}catch(err){
				rej(err);
			}
		}).catch(rej);
	});
}

//поиск продажи по её идентификатору и номеру карты клиента (вернет undefined или уид продажи)
function checkDispenseOnEhr(clientCard = '', checkAndPositionUid = ''){
	return new Promise(function(res, rej){
		(new Promise(function (resolve, reject){
			auth().then(function(value){
				resolve(value);
			}).catch(function(error){
				reject(error);
			});
		})).then(function(){
			return new Promise(function(resolve, reject){
				let headers;
				if(CONFIG.ehr.usenceu){
					headers = { "Content-Type": "application/json" };
				} else {
					headers = { "Content-Type": "application/json", "Authorization":"Bearer "+token.key };
				}
				let link = CONFIG.ehr.server+"/MedicationPrescription/$by-card?patientIdentifier="+clientCard+"&status=active,completed,entered-in-error";
				FUNCTIONS.rest({url:link, method:"GET", headers:headers}).then(function(_resolve){
					switch(_resolve[0]){
						case 200:
							FUNCTIONS.fhirparser(JSON.parse(_resolve[1])).then(function(val){
								resolve(val);
							}).catch(function(err){
								reject(new Error('Ошибка преобразования ответа: '+err));
							});
							break;
						default:
							reject(new Error('Сервер недоступен, статус: '+_resolve[1]));
							break;
					}
				}).catch(function(error){
					reject(new Error('Ошибка REST-запроса: '+error));
				});
			});
		}).then(function(val){
			return new Promise(function(resolve){
				if(typeof(val['Bundle.MedicationDispense']) === 'object'){
					let dispenseUids = Object.keys(val['Bundle.MedicationDispense']);
					for(let i = 0; i < dispenseUids.length; i++){
						let dispenseUid = dispenseUids[i];
						if(
							(typeof(val['Bundle.MedicationDispense'][dispenseUid].identifier) === 'object') && 
							(val['Bundle.MedicationDispense'][dispenseUid].identifier.system === linkForEhrGenerator()) && 
							(val['Bundle.MedicationDispense'][dispenseUid].identifier.value === checkAndPositionUid) &&
							(val['Bundle.MedicationDispense'][dispenseUid].status === 'completed')
						){
							return resolve(dispenseUid);
						}
					}
				}
				return resolve();
			});
		}).then(function(val){
			res(val);
		}).catch(function(error){
			rej(error);
		});
	});
}

//обработка очереди отправки продаж в ehr
function SendDispense(data){
	return new Promise(function(res, rej){
		(new Promise(function (resolve, reject){
			auth().then(function(value){
				resolve(value);
			}).catch(function(error){
				reject(error);
			});
		})).then(function(){
			return new Promise(function(resolve, reject){
				let headers;
				if(CONFIG.ehr.usenceu){
					headers = { "Content-Type": "application/json" };
				} else {
					headers = { "Content-Type": "application/json", "Authorization":"Bearer "+token.key };
				}
				let link = CONFIG.ehr.server+"/MedicationDispense/$register";
				FUNCTIONS.rest({url:link, method:"POST", headers:headers, datatype:'object', data:data}).then(function(_resolve){
					data = null;
					switch(_resolve[0]){
						case 200:
							FUNCTIONS.fhirparser(JSON.parse(_resolve[1])).then(function(val){
								resolve(val.MedicationDispense.id);
							}).catch(function(err){
								reject(new Error('Ошибка преобразования ответа: '+err));
							});
							break;
						default:
							reject(new Error('Сервер недоступен, статус: '+_resolve[1]));
							break;
					}
				}).catch(function(error){
					reject(new Error('Ошибка REST-запроса: '+error));
				});
			});
		}).then(function(val){
			res(val);
		}).catch(function(error){
			rej(error);
		});
	});
}

//обработка очереди отмены продаж в ehr
function CancelDispense(dispense){
	return new Promise(function(res, rej){
		(new Promise(function (resolve, reject){
			auth().then(function(value){
				resolve(value);
			}).catch(function(error){
				reject(new Error(error));
			});
		})).then(function(){
			return new Promise(function(resolve, reject){
				let headers;
				if(CONFIG.ehr.usenceu){
					headers = { "Content-Type": "application/json" };
				} else {
					headers = { "Content-Type": "application/json", "Authorization":"Bearer "+token.key };
				}
				let link = CONFIG.ehr.server+"/MedicationDispense/"+dispense+"/$set-as-error";
				FUNCTIONS.rest({url:link, method:"POST", headers:headers}).then(function(_resolve){
					switch(_resolve[0]){
						case 200:
							FUNCTIONS.fhirparser(JSON.parse(_resolve[1])).then(function(val){
								if(val.MedicationDispense.status === 'entered-in-error'){
									resolve(val.MedicationDispense.id);
								} else {
									reject(new Error('MedicationDispense id='+val.MedicationDispense.id+' в статусе '+val.MedicationDispense.status));
								}
							}).catch(function(err){
								reject(new Error('Ошибка преобразования ответа: '+err));
							});
							break;
						default:
							reject(new Error('Сервер недоступен, статус: '+_resolve[1]));
							break;
					}
				}).catch(function(error){
					reject(new Error('Ошибка REST-запроса: '+error));
				});
			});
		}).then(function(val){
			res(val);
		}).catch(function(error){
			rej(error);
		});
	});
}

//парсит очередь сообщений в ehr
function goQUERY(client){
	if((run+300000) < Date.now()){
		run = Date.now();
		MONGODB.findcursor(client.db(CONFIG.mongodb.database).collection('Request'), VALIDATORS.query({find:{type: "EHRRequest", status:"query"}})).then(function(cursor){
			let closecursor = function(){
				try{
					if(cursor && (typeof(cursor.close) === 'function')){
						cursor.close().then(function(){ 
							LOGGER.debug('CURSOR CLOSED'); 
						}).catch(function(){ 
							LOGGER.debug('CURSOR NOT CLOSED'); 
						});
					}
				} catch(_e){
					LOGGER.debug('CURSOR NOT CLOSED: '+_e); 
				}
			}
			cursor.count(function(err2, count) {
				if(err2){
					LOGGER.error('EHR->Ошибка обработки курсора: '+err2);
					closecursor();
					run = 0;
				} else if(count === 0){
					closecursor();
					run = 0;
				} else {
					let _count = 0;
					cursor.forEach(function(doc){
						if((doc.status === "query") && (doc.dispense === null)){
							let checkAndPosId = doc.check+'/'+doc.position;
							checkCardnumOnDatabase(client, checkAndPosId).then(function(medCardId){
								if(medCardId !== undefined){
									return checkDispenseOnEhr(medCardId, checkAndPosId);
								} else {
									return new Promise(function(resolve){resolve();});
								}
							}).then(function(dispenseUid){
								if(dispenseUid){
									return new Promise(function(resolve){resolve(dispenseUid);});
								} else {
									return SendDispense(doc.body);
								}
							}).then(function(resolve){
								let _ehrDoc = {
									status: "completed",
									dispense: resolve
								}
								let _findDoc = {
									type: doc.type,
									hash: doc.hash
								}
								MONGODB.update(client.db(CONFIG.mongodb.database).collection('Request'), _findDoc, _ehrDoc).then(function(){
									LOGGER.log('EHR-> регистрация продажи '+resolve+' по чеку '+doc.check);
								}).catch(function(err4){
									LOGGER.error('EHR-> ошибка смены статуса документа '+doc.hash+': '+err4);
								}).finally(function(){
									_count++;
									if(_count === count){
										closecursor();
										run = 0;
									}
								});
							}).catch(function(err3){
								LOGGER.error('EHR-> ошибка обработки сообщения из очереди, документ '+doc.hash+' отброшен: '+err3);
								let _ehrDoc = {
									status: "error"
								}
								let _findDoc = {
									type: doc.type,
									hash: doc.hash
								}
								MONGODB.update(client.db(CONFIG.mongodb.database).collection('Request'), _findDoc, _ehrDoc).then(function(){}).catch(function(err4){
									LOGGER.error('EHR-> ошибка смены статуса документа '+doc.hash+': '+err4);
								}).finally(function(){
									_count++;
									if(_count === count){
										closecursor();
										run = 0;
									}
								});
							});
						} else if((doc.status === "query") && (doc.dispense !== null)){
							CancelDispense(doc.dispense).then(function(resolve){
								let _ehrDoc = {
									status: "canceled"
								}
								let _findDoc = {
									type: doc.type,
									hash: doc.hash
								}
								MONGODB.update(client.db(CONFIG.mongodb.database).collection('Request'), _findDoc, _ehrDoc).then(function(){
									LOGGER.log('EHR-> отмена продажи '+resolve+' по чеку '+doc.check);
								}).catch(function(err4){
									LOGGER.error('EHR-> ошибка смены статуса документа '+doc.hash+': '+err4);
								}).finally(function(){
									_count++;
									if(_count === count){
										closecursor();
										run = 0;
									}
								});
							}).catch(function(err3){
								LOGGER.error('EHR-> ошибка обработки сообщения из очереди, документ '+doc.hash+' отброшен: '+err3);
								let _ehrDoc = {
									status: "error"
								}
								let _findDoc = {
									type: doc.type,
									hash: doc.hash
								}
								MONGODB.update(client.db(CONFIG.mongodb.database).collection('Request'), _findDoc, _ehrDoc).then(function(){}).catch(function(err4){
									LOGGER.error('EHR-> ошибка смены статуса документа '+doc.hash+': '+err4);
								}).finally(function(){
									_count++;
									if(_count === count){
										closecursor();
										run = 0;
									}
								});
							});
						} else {
							_count++;
							if(_count === count){
								closecursor();
								run = 0;
							}
						}
					});
				}
			});
		}).catch(function(err){
			LOGGER.error('EHR->Ошибка создания курсора: '+err);
			run = 0;
		});
	}
}

//поиск ошибок в ehr-запросах
function getErrors(client){
	client.db(CONFIG.mongodb.database).collection('Request').aggregate([{
		$match:{type:"EHRRequest", status: "error"}
	},
	{
		$group:{_id:"removable", result:{$push:"$hash"}}
	}
	],
	{ allowDiskUse: true }).toArray().then(function(_errors){
		if((typeof(_errors) === 'object') && (typeof(_errors[0]) === 'object') && (typeof(_errors[0].result) === 'object') && (_errors[0].result.length > 0)){
			client.db(CONFIG.mongodb.database).collection('Request').updateMany({hash:{"$in":_errors[0].result}, dispense: null, status:"error"}, {"$set":{status:"query"}}).catch(function(err){
				LOGGER.error('EHR-> Не могу сбросить статус запроса в query: '+err);
			});
			SENDMAIL.send({
				message:'Обнаружены ошибки EHR-запросов, запрос для проверки : db.Request.find({hash:{"$in":["'+_errors[0].result.join('",\n"')+'"]}})', 
				theme:'Отчет об ошибках EHR'
			});
		}
	}).catch(function(err2){
		LOGGER.error('EHR-> Не могу проверить ошибки запросов EHR: '+err2);
	});
}

//воркер
var SendEHRWorker = function(client){
	getErrors(client);
	setInterval(goQUERY, 15000, client);	//15 сек
	setInterval(getErrors, 3600000, client);	//час
}

//добавляет документ в очередь на отправку
function SendToQuery(database, data){
	return new Promise(function(resolve, reject){
		if(VALIDATORS.isreturn(data)){
			let realCheckGuid = "";
			for(const key in data.requisites){
				if(data.requisites[key].id === 'CheckSaleForCheckReturn'){
					realCheckGuid = data.requisites[key].value;
				}
			}
			let positionarr = [];
			if((typeof(data.multilines) === 'object') && Array.isArray(data.multilines)){
				for(let _mid = 0; _mid < data.multilines.length; _mid++){
					let _multiline = data.multilines[_mid];
					if(_multiline.id === 'ПозицииЧека'){
						if((typeof(_multiline.lines) === 'object') && Array.isArray(_multiline.lines)){
							for(let _lid = 0; _lid < _multiline.lines.length; _lid++){
								let _line = _multiline.lines[_lid];
								if((typeof(_line.requisites) === 'object') && Array.isArray(_line.requisites)){
									for(let _rid = 0; _rid < _line.requisites.length; _rid++){
										let _requisite = data.multilines[_mid].lines[_lid].requisites[_rid];
										if((typeof(_requisite) === 'object') && (_requisite.id === 'PositionsUid') && (typeof(_requisite.value) === 'string')){
											positionarr.push(_requisite.value);
										}
									}
								}
							}
						}
					}
				}
			}
			data = null;
			let _count = 0;
			let _lcount = 0;
			for(let _index = 0; _index < positionarr.length; _index++){
				let _positionuid = positionarr[_index];
				let _hash = FUNCTIONS.hasher(realCheckGuid+_positionuid);
				let _findDoc = VALIDATORS.query({find:{type: "EHRRequest", hash: _hash}});
				MONGODB.find(database.collection('Request'), _findDoc).then(function(_data){
					if(_data.length > 0){
						_lcount = _lcount + _data.length;
						for(let i = 0; i < _data.length; i++){
							if((_data[i].dispense !== null) && (_data[i].status === 'completed')){
								let _ehrDoc = {
									status: "query"
								}
								let _findDoc = {
									type: _data[i].type,
									hash: _data[i].hash
								}
								MONGODB.update(database.collection('Request'), _findDoc, _ehrDoc).then(function(){
									_count++;
									if(_count === _lcount){
										resolve('next');
									}
								}).catch(function(err){
									reject(new Error('EHR-> ошибка смены статуса документа '+_data[i].hash+': '+err));
								});	
							} else {
								switch(_data[i].status){
									case 'query':
										reject(new Error('EHR-> ошибка смены статуса документа '+_data[i].hash+': чек еще обрабатывается.'));
										break;
									case 'canceled':
										reject(new Error('EHR-> ошибка смены статуса документа '+_data[i].hash+': чек отменен.'));
										break;
									case 'error':
										reject(new Error('EHR-> ошибка смены статуса документа '+_data[i].hash+': при отправке чека была ошибка и она не обработана.'));
										break;
									default:
										reject(new Error('EHR-> ошибка смены статуса документа '+_data[i].hash+': неизвестная ошибка.'));
										break;
								}
							}
						}						
					} else {
						reject(new Error('Чек '+realCheckGuid+' не отправлялся на сервер EHR.'));
					}
				}).catch(function(err){
					data = null;
					reject(err);
				});
			}
		} else {
			normaliseSendData(data).then(function(_resolve){	//приводим док к валидному виду, вернет объект запросов к ehr (т.к. ограничено число отовариваний в запросе 1 шт, а в чеке м.б. больше)
				if((typeof(_resolve) === 'object') && !LODASH.isEqual(_resolve, {})){
					let count = 0;
					let _length = Object.keys(_resolve).length;
					for(const positionuid in _resolve){
						let _guid = FUNCTIONS.hasher(data.guid+positionuid);
						let _checkdate = Date.now();
						try{
							_checkdate = (new Date(data.date)).getTime();
						}catch(e){
							LOGGER.warn(e);
						}
						let _ehrDoc = {
							type: "EHRRequest",
							hash: _guid,
							check: data.guid,
							position:positionuid,
							status: "query",	//query - в очередь (dispense = null означает очередь на отправку, dispense !== null означает очередь на отмену), completed - отправлен, error - ошибка, canceled - отменен
							dispense: null,
							timestamp: _checkdate,
							body: LODASH.clone(_resolve[positionuid])
						};
						let _findDoc = {
							type: "EHRRequest", 
							hash: _guid,
							"$or":[
								{
									status:"query"	//документ еще не отправлен
								},
								{
									dispense:{"$exists":false} //документа нет в БД
								}
							]
						};
						_resolve[positionuid] = null;
						MONGODB.update(database.collection('Request'), _findDoc, _ehrDoc).then(function(){
							count++;
							if(count === _length){
								data = null;
								resolve('next');
							}
						}).catch(function(error){
							data = null;
							reject(error);
						}).finally(function(){
							_ehrDoc = null;
							_findDoc = null;
						});
					}
				} else {
					LOGGER.error('Проверьте чек '+ data.guid + ', т.к. он имеет признак EHR, но необходимые поля отсутствуют.');
					data = null;
					resolve('next');
				}
			}).catch(function(_reject){
				data = null;
				reject(_reject);
			});
		}
	});
}

module.exports.auth = auth;
module.exports.find = find;
module.exports.send = SendToQuery;
module.exports.worker = SendEHRWorker;
module.exports.test = checkDispenseOnEhr;