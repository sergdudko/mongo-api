/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  REDIS
 *	Работа с redis
 *
 */

"use strict"

//подгружаемые библиотеки
var REDIS = require('redis'),
	CLUSTER = require('cluster'),
	CONFIG = require('config'),
	PATH = require('path');
	require('redis-streams')(REDIS);

//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js'));

function connect(){
	let redis = REDIS.createClient({
		url:CONFIG.redis.url,
		detect_buffers: false, 
		return_buffers: true
	});
	redis.on("error", function (err) {
		LOGGER.error(err);
	});
	redis.on("connect", function () {
		LOGGER.log('MONGODB-> Соединение с Redis установлено!');
		PROCSTORE.dispatch({type:'STATUS_REDIS', payload:{status: true, worker: CLUSTER.worker.id}});
	});
	redis.on("end", function () {
		LOGGER.warn('MONGODB-> Соединение с Redis потеряно!');
		PROCSTORE.dispatch({type:'STATUS_REDIS', payload:{status: false, worker: CLUSTER.worker.id}});
	});
	return redis;
}

module.exports = connect();