/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	PROCESS
 *  WEB-WORKER
 *  Экземпляер процесса веб-сервера, запускает app
 *
 */

"use strict"

//подгружаемые библиотеки
var CONFIG = require('config'),
	HTTP = require('http'),
	HTTPS = require('https'),
	FS = require('fs'),
	PATH = require('path');
	
	
//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	MONGODB = require(PATH.join(__dirname, 'module.mongodb.js')),
	APP = require(PATH.join(__dirname, 'app.js')),
	REDIS = require(PATH.join(__dirname, 'module.redis.js'));

//глобальные переменные	
var timeout = 600000;	//таймаут соединения в мс

function startWebWorker(){
	MONGODB.client().then(function(client){
		APP.locals.mongodb = client;
		if(CONFIG.useredis){
			APP.locals.redis = REDIS;
		}
		if(CONFIG.server.ssl && CONFIG.server.ssl.crt && CONFIG.server.ssl.ca && CONFIG.server.ssl.key){ //выбираю тип сервера
			let options = {
				key: ''+FS.readFileSync(CONFIG.server.ssl.key),
				cert: FS.readFileSync(CONFIG.server.ssl.crt),
				ca: FS.readFileSync(CONFIG.server.ssl.ca),
				minVersion: 'TLSv1'
			};
			HTTPS.createServer(options, APP).setTimeout(timeout).listen(CONFIG.server.port, '0.0.0.0');
			LOGGER.log('https-сервер запущен на порту:' + CONFIG.server.port);
		} else { 
			HTTP.createServer(APP).setTimeout(timeout).listen(CONFIG.server.port, '0.0.0.0');
			LOGGER.log('http-сервер запущен на порту:' + CONFIG.server.port);
		}
	}).catch(function(err){
		LOGGER.error('Ошибка подключения к mongodb (сл.попытка через 30 сек): '+err);
		setTimeout(startWebWorker, 30000);
	});
}

startWebWorker();