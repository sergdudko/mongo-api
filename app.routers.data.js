/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  APP.ROUTERS.DATA
 *	Маршрут для /v3/data
 *
 */

"use strict"

//подгружаемые библиотеки
var EXPRESS = require('express'),
	CONFIG = require('config'),
	OBJECTSTREAM = require('@sergdudko/objectstream'),
	EVENTSTREAM = require('event-stream'),
	CLUSTER = require('cluster'),
	PATH = require('path'),
	STREAM = require('stream'),
	ZLIB = require('zlib');
	
//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	VALIDATORS = require(PATH.join(__dirname, 'module.validators.js')),
	MONGODB = require(PATH.join(__dirname, 'module.mongodb.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	EHR = require(PATH.join(__dirname, 'module.ehr.js'));


var data = EXPRESS.Router();

//post (заливка в базу)
data.all('*', function (req, res){
	(new Promise(function (resolve, reject){
		if(CONFIG.bantimeout){
			if(
				(res.locals.permission['all'] && (res.locals.permission['all'].indexOf('w') !== -1)) ||
				(res.locals.permission['data'] && (res.locals.permission['data'].indexOf('w') !== -1))
			){
				return resolve('next');
			} else if(res.locals.permission['custom'] && (Array.isArray(res.locals.permission['custom']['w']))){
				res.locals.permcustom = res.locals.permission['custom']['w'];
				return resolve('next');
			} else {
				res.status(403).json({
					status: "error",
					message: "Permission denied"
				});
				return reject('close');
			}
		} else {
			return resolve('next');
		}
	})).then(function(){
		return new Promise(function (resolve, reject){ 
			if((req.method === 'POST') || (req.method === 'DELETE')){
				return resolve('next');
			} else{
				res.status(405).json({status:"error", message:'Method Not Allowed'});
				return reject('close');
			}
		});
	}).then(function(){
		return new Promise(function (resolve, reject){
			if(PROCSTORE.getState().stats.mongodb[CLUSTER.worker.id]){
				return resolve('next');
			} else {
				res.status(500).json({status:"error", message:'Database connection error'});
				return reject('close');
			}
		});
	}).then(function(){
		return new Promise(function (resolve, reject){
			//обработка входящего потока
			let count = 0;
			const parser = new OBJECTSTREAM.Parser('[',',',']'); //поток парсинга json-документов из массива
			const mbstring = new STREAM.Transform({	//обработка мультибайтовых символов без присвоенной кодировки может срабатывать некорректно
				transform(_buffer, encoding, callback) {
					this.push(_buffer)
					return callback();
				}
			});
			mbstring.setEncoding('utf8');
			const event = EVENTSTREAM.map(function (doc, next1) {	//поток обработки документов
				if(req.method === 'DELETE'){
					doc.forbidden = true;
				}
				let _doc = VALIDATORS.data(doc);	//валидатор объекта
				let _unset = VALIDATORS.unset(_doc);	//обнуляемые поля
				doc = null;
				if(typeof(_doc) === 'object'){
					if(Array.isArray(res.locals.permcustom) && (res.locals.permcustom.indexOf(_doc.type) === -1)){
						req.unpipe();
						event.destroy();
						parser.destroy();
						res.status(403).json({status:"error", message:'Permission denied'});
						return reject('close');
					}
					const _find = {"type":_doc.type, "guid":_doc.guid};
					MONGODB.update(res.locals.mongodb.db(CONFIG.mongodb.database).collection('Data'), _find, _doc, _unset).then(function(){
						count++;
						if(VALIDATORS.useehr(_doc)){	//если документ чек с признаком ЭР, ставлю в очередь на отправку в ehr
							EHR.send(res.locals.mongodb.db(CONFIG.mongodb.database), _doc).catch(function(rej){
								closer(rej);
							}).finally(function(){
								return next1();
							});
						} else {
							_doc = null;
							return next1();
						}
					}).catch(function(error){
						closer(error);
						_doc = null;
						return next1();
					});
				} else {
					closer(_doc);
				}
			});
			const gunzipper = ZLIB.createGunzip();	//декомпрессия входящего потока
			let openedStream = true;
			const closer = function(err){	//закрытие потоков
				if(openedStream){
					openedStream = false;
					LOGGER.log('Обновлено записей: ' + count);
					req.unpipe();
					gunzipper.unpipe();
					mbstring.unpipe();
					parser.unpipe();
					event.destroy(err);
					if((gunzipper._readableState.destroyed !== true) || (gunzipper._writableState.destroyed !== true)) {
						gunzipper.destroy(err);
					}
					if((mbstring._readableState.destroyed !== true) || (mbstring._writableState.destroyed !== true)) {
						mbstring.destroy(err);
					}
					if((parser._readableState.destroyed !== true) || (parser._writableState.destroyed !== true)) {
						parser.destroy(err);
					}
					if(err){
						res.status(500).json({status:"error", message:'Ошибка обработки JSON: '+err});
						return reject('close');
					} else {
						return resolve('next');
					}
				}
			}
			gunzipper.on('error',function(err){
				closer(err);
			});
			if(res.locals.gzippedbody){
				req.on('error',function(err){	//объединяем потоки
					closer(err);
				}).pipe(gunzipper).pipe(mbstring).on('error',function(err){
					closer(err);
				}).pipe(parser).on('error',function(err){
					closer(err);
				}).pipe(event).on('error',function(err){
					closer(err);
				}).on('end',function(){
					closer();
				});	
			} else {
				req.on('error',function(err){	//объединяем потоки
					closer(err);
				}).pipe(mbstring).on('error',function(err){
					closer(err);
				}).pipe(parser).on('error',function(err){
					closer(err);
				}).pipe(event).on('error',function(err){
					closer(err);
				}).on('end',function(){
					closer();
				});	
			}
		});
	}).then(function(){
		return res.status(200).json({status:"ok", message:'completed'});
	}).catch(function(error){
		if(error !== 'close'){
			LOGGER.error('Неизвестная ошибка запроса: '+error);
			return res.status(500);//.json({status:"error", message:'Internal Server Error'}); //после отправки хидера вызывает эксепшн, поэтому без кода
		}
		return;
	});
});

module.exports = data;