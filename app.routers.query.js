/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  APP.ROUTERS.QUERY
 *	APP.ROUTERS.BIGDATA
 *	Маршрут для /v3/query и /v3/bigdata
 *
 */

"use strict"

//подгружаемые библиотеки
var EXPRESS = require('express'),
	CONFIG = require('config'),
	LODASH = require('lodash'),
	FS = require('fs'),
	CRYPTO = require('crypto'),
	ZLIB = require('zlib'),
	CLUSTER = require('cluster'),
	PATH = require('path'),
	OBJECTSTREAM = require('@sergdudko/objectstream');
	
//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	VALIDATORS = require(PATH.join(__dirname, 'module.validators.js')),
	FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js')),
	MONGODB = require(PATH.join(__dirname, 'module.mongodb.js'));

function transformDocs(_doc){	//обработка документов перед записью в файл
	let doc = LODASH.clone(_doc);
	doc["_id"] = doc["_id"].toString();
	return doc;
}
function Query(_type){
	if(['query', 'bigdata'].indexOf(_type) === -1)
		throw new Error('Query require string argument of query or bigdata!');
	let self = this;
	self.type = _type;
	self.query = EXPRESS.Router();
	self.query.all('*', function (req, res){
		//проверяю метод запроса
		(new Promise(function (resolve, reject){
			if(CONFIG.bantimeout){
				if(self.type === 'query'){
					if(
						(res.locals.permission['all'] && (res.locals.permission['all'].indexOf('r') !== -1)) ||
						(res.locals.permission['query'] && (res.locals.permission['query'].indexOf('r') !== -1))
					){
						return resolve('next');
					} else if(res.locals.permission['custom'] && (Array.isArray(res.locals.permission['custom']['r']))){
						res.locals.permcustom = res.locals.permission['custom']['r'];
						return resolve('next');
					} else {
						res.status(403).json({
							status: "error",
							message: "Permission denied"
						});
						return reject('close');
					}
				} else if(self.type === 'bigdata'){
					if(
						(res.locals.permission['all'] && (res.locals.permission['all'].indexOf('r') !== -1)) ||
						(res.locals.permission['bigdata'] && (res.locals.permission['bigdata'].indexOf('r') !== -1))
					){
						resolve('next');
					} else {
						try{
							res.set({'Content-Type': 'application/json; charset=utf-8'});
						}catch(err){
							LOGGER.warn(err);
						}finally{
							res.status(403).end(JSON.stringify({
								status: "error",
								message: "Permission denied"
							}));
							reject('close');
						}
					}
				}
			} else {
				return resolve('next');
			}
		})).then(function(){
			return new Promise(function (resolve, reject){ 
				if(req.method !== 'POST'){
					res.status(405).json({status:"error", message:'Method Not Allowed'});
					return reject('close');
				} else{
					return resolve('next');
				}
			});
		}).then(function(){
		//проверяю валидность json
			if(self.type === 'query'){
				return new Promise(function (resolve, reject){ 
					res.locals.json = VALIDATORS.query(req.body);
					if(res.locals.json === 'err'){
						res.status(400).json({status:"error", message:'Invalid JSON data'});
						return reject('close');
					} else if(Array.isArray(res.locals.permcustom) && (res.locals.permcustom.indexOf(res.locals.json.find.type) === -1)){
						res.status(403).json({status:"error", message:'Permission denied'});
						return reject('close');
					} else {
						res.locals.uid = FUNCTIONS.hasher(JSON.stringify(res.locals.json.find) + res.locals.json.limit.toString() + JSON.stringify(res.locals.json.sort) + res.locals.json.skip.toString() + JSON.stringify(res.locals.json.requires));
						if(typeof(res.locals.json.find["$comment"]) === 'string'){
							res.locals.json.find["$comment"] = res.locals.json.find["$comment"] + ' | Request from: '+res.locals.ip;
						} else {
							res.locals.json.find["$comment"] = 'Request from: '+res.locals.ip;
						}
						return resolve('next');
					}
				});
			} else if(self.type === 'bigdata'){
				return new Promise(function (resolve, reject){
					res.locals.json = VALIDATORS.bigdata(req.body);
					if(res.locals.json === 'err'){
						try{
							res.set({'Content-Type': 'application/json; charset=utf-8'});
						}catch(err){
							LOGGER.warn(err);
						}finally{
							res.status(400).send(JSON.stringify({status:"error", message:'Invalid JSON data'}));
							res.locals.closer();
							reject('close');
						}
					} else {
						res.locals.uid = FUNCTIONS.hasher(JSON.stringify(res.locals.json));
						if((typeof(res.locals.json[0]) === 'object') && (typeof(res.locals.json[0]["$match"]) === 'object')){
							if(typeof(res.locals.json[0]["$match"]["$comment"]) === 'string'){
								res.locals.json[0]["$match"]["$comment"] = res.locals.json[0]["$match"]["$comment"] + ' | Request from: '+res.locals.ip;
							} else {
								res.locals.json[0]["$match"]["$comment"] = 'Request from: '+res.locals.ip;
							}
						}
						resolve('next');
					}
				});
			}
		}).then(function(){
		//проверяю, что кэширование не выполняется
			return new Promise(function (resolve, reject){
				if(PROCSTORE.getState().tasks.indexOf(res.locals.uid) !== -1){
					res.status(206).json({status:"ok", message:'Partial Content'});
					return reject('close');
				} else {
					return resolve('next');
				}			
			});
		}).then(function(){
		//проверяю что ключа нет в redis, CACHE LVL 1 (REDIS)
			return new Promise(function (resolve, reject){
				if(CONFIG.useredis){
					res.locals._timeout2 = CONFIG.cache;	//в секундах
					try{
						let _timeout = parseInt(req.get('ics-cache-timeout'), 10) * 60;	//минуты в секунды
						if(_timeout > CONFIG.cache){
							res.locals._timeout2 = _timeout;
						}
					} catch(e){
						LOGGER.warn(e);
					}
					if(PROCSTORE.getState().stats.redis[CLUSTER.worker.id]){
						res.locals.redis.get(res.locals.uid+'-data', function (err, reply) {
							if(err || !reply){
								return resolve('next');
							} else {
								let _reply = JSON.parse(reply.toString());
								if((_reply.datetime+(res.locals._timeout2*1000)) > Date.now()){
									if(_reply.hashresult === FUNCTIONS.hasher("")){	//хэш от пустого массива (конец задания)
										res.set({'Content-Type': 'application/json; charset=utf-8', 
													'ics-cache-hash': _reply.hash,
													'ics-cache-hashresult': _reply.hashresult});
										res.status(204).send();
										res.locals.cachel = 'L1';
										return reject('close');
									} else {
										res.locals.redis.strlen(res.locals.uid, function(err, length) { 
											if(err || (length === 0)){
												return resolve('next');
											} else {
												if(_reply.hashresult === req.get('ics-cache-hashresult')){	//данные не изменились
													res.set({
														'Content-Type': 'application/json; charset=utf-8',
														'ics-cache-hash': _reply.hash,
														'ics-cache-hashresult': _reply.hashresult
													});
													res.status(208).json({status:"ok", message:'Already Reported'});
													res.locals.cachel = 'L1';
													return reject('close');
												} else {
													let headers = {
														'Content-Type': 'application/json; charset=utf-8',
														'ics-cache-hash': _reply.hash,
														'ics-cache-hashresult': _reply.hashresult
													};
													if(CONFIG.usegzip && res.locals.acceptgzip){
														headers['Content-Encoding'] = 'gzip';
													}
													if(!(CONFIG.usegzip && !res.locals.acceptgzip)){
														headers['Content-Length'] = length;
													}
													res.set(headers);
													let readerStream = res.locals.redis.readStream(res.locals.uid);
													const gunzipper = ZLIB.createGunzip();	//поток декомпрессии
													let openedStream = true;
													const closerErrStream = function(err){ //обработка ошибок
														if(openedStream){
															openedStream = false;
															readerStream.unpipe();
															gunzipper.unpipe();
															if((gunzipper._readableState.destroyed !== true) || (gunzipper._writableState.destroyed !== true)){
																gunzipper.destroy(err);
															}
															if(readerStream._readableState.destroyed !== true){
																readerStream.destroy(err);
															}
															if(err){
																LOGGER.warn(err);
																res.status(500);
																res.locals.cachel = 'L1';
																return reject('close');
															} else {
																res.locals.cachel = 'L1';
																return reject('close');
															}
														}
													}
													if(CONFIG.usegzip && !res.locals.acceptgzip){
														readerStream.on('error', closerErrStream).pipe(gunzipper).on('end', () => {
															closerErrStream();
														}).on('error', closerErrStream).pipe(res);
													} else{
														readerStream.on('error', closerErrStream).on('end', () => {
															closerErrStream();
														}).pipe(res);
													}
												}
											}
										});
									}
								} else {
									return resolve('next');
								}
							}
						});
					}else {
						return resolve('next');
					}
				} else {
					return resolve('next');
				}
			});
		}).then(function(){
		//кэширую данные в redis, если таймаут кэширования меньше 60 минут или такого хидера нет
			return new Promise(function (resolve, reject){
				if(CONFIG.useredis){
					if((typeof(res.locals._timeout2) !== 'number') || isNaN(res.locals._timeout2) || (res.locals._timeout2 < 3600)){	//в redis ложу данные до 1 часа
						if(PROCSTORE.getState().stats.mongodb[CLUSTER.worker.id] && PROCSTORE.getState().stats.redis[CLUSTER.worker.id]){
							PROCSTORE.dispatch({type:'ADD_TASK', payload:{hash: res.locals.uid, worker: CLUSTER.worker.id, request:res.locals.json}});
							(function(){
								if(self.type === 'query'){
									return MONGODB.findcursor(res.locals.mongodb.db(CONFIG.mongodb.database).collection('Data'), res.locals.json, res.locals.uid);
								} else if(self.type === 'bigdata'){
									return MONGODB.bigdatacursor(res.locals.mongodb.db(CONFIG.mongodb.database).collection('Data'), res.locals.json, res.locals.uid);
								}
							})().then(function(cursor){
								LOGGER.debug('CURSOR STARTED '+res.locals.uid); 
								let closecursor = function(){ 
									if(cursor && (typeof(cursor.close) === 'function')){
										cursor.close().then(function(){ 
											LOGGER.debug('CURSOR CLOSED '+res.locals.uid); 
										}).catch(function(){ 
											LOGGER.debug('CURSOR NOT CLOSED'+res.locals.uid); 
										});
									}
								}
								try{
									let filestream = res.locals.redis.writeStream(res.locals.uid, res.locals._timeout2);	//поток поток записи в redis
									const hash = CRYPTO.createHash('sha1');	//поток генерации хэша
									const tempstream = cursor.stream({transform: x => transformDocs(x)});	//поток чтения данных из базы
									const gzipper = ZLIB.createGzip();	//поток сжатия
									const stringifer = new OBJECTSTREAM.Stringifer('[', ',', ']'); //поток преобразования object->string
									let openedStream = true;
									const closerErrStream = function(err){ //обработка ошибок в 3-х потоках
										if(openedStream){
											openedStream = false;
											tempstream.unpipe(); //отвязываем потоки
											stringifer.unpipe();
											gzipper.unpipe();
											if((tempstream._readableState.destroyed !== true) || (tempstream._writableState.destroyed !== true)){	//уничтожаем потоки, если существуют
												tempstream.destroy(err);
											}
											if((stringifer._readableState.destroyed !== true) || (stringifer._writableState.destroyed !== true)){
												stringifer.destroy(err);
											}
											if((hash._readableState.destroyed !== true) || (hash._writableState.destroyed !== true)) {
												hash.destroy(err);
											}
											if((gzipper._readableState.destroyed !== true) || (gzipper._writableState.destroyed !== true)){
												gzipper.destroy(err);
											}
											if(filestream._writableState.destroyed !== true){
												filestream.destroy(err);
											}
											if(err){
												LOGGER.warn('Ошибка записи результатов запроса в redis: ' + err + ' запрос к API: ' + JSON.stringify(req.body));
												PROCSTORE.dispatch({type:'DEL_TASK', payload:{hash: res.locals.uid, worker: CLUSTER.worker.id}});
												res.status(500).json({status:"error", message:'Internal Server Error'});
												res.locals.cachel = 'L1';
												closecursor();
												return reject('close');
											} else {
												res.locals.hashresult = hash.digest('hex');
												if(res.locals.hashresult === FUNCTIONS.hasher("[]")){
													res.locals.hashresult = FUNCTIONS.hasher("");
												}
												res.locals.redis.set(res.locals.uid+'-data', JSON.stringify({"hash":res.locals.uid, "hashresult":res.locals.hashresult, "datetime":Date.now()}), "EX", res.locals._timeout2, function (err) {
													if(err){
														LOGGER.error('Ошибка записи в redis: '+err);
													}
													PROCSTORE.dispatch({type:'DEL_TASK', payload:{hash: res.locals.uid, worker: CLUSTER.worker.id}});
													closecursor();
													return resolve('next');
												});
											}
										}
									};
									cursor.on('error', closerErrStream);
									hash.on('error', closerErrStream);
									if(CONFIG.usegzip){
										tempstream.on("error", closerErrStream).pipe(stringifer).on('data', function(string){
											hash.update(string);
										}).on("error", closerErrStream).pipe(gzipper).on("error", closerErrStream).pipe(filestream).on("error", closerErrStream);
									} else {
										tempstream.on("error", closerErrStream).pipe(stringifer).on('data', function(string){
											hash.update(string);
										}).on("error", closerErrStream).pipe(filestream).on("error", closerErrStream);
									}
									filestream.on("finish", function(){ //в случае успешной записи возвращаем 201 и новый hash
										closerErrStream();
									});					
								} catch(e){
									res.status(417).json({status:"error", message:'Expectation Failed'});
									PROCSTORE.dispatch({type:'DEL_TASK', payload:{hash: res.locals.uid, worker: CLUSTER.worker.id}});
									res.locals.cachel = 'L1';
									closecursor();
									return reject('close');
								}
							}).catch(function(e){
								res.status(417).json({status:"error", message:'Expectation Failed'});
								LOGGER.error('Ошибка создания курсора: '+e);
								PROCSTORE.dispatch({type:'DEL_TASK', payload:{hash: res.locals.uid, worker: CLUSTER.worker.id}});
								res.locals.cachel = 'L1';
								return reject('close');
							});
						} else {
							res.status(500).json({status:"error", message:'Database or Redis connection error, operation is not possible'});
							return reject('close');
						}
					} else{
						return resolve('next');
					} 
				} else {
					return resolve('next');
				}
			}); 
		}).then(function(){
		//отдаю данные из redis
			return new Promise(function (resolve, reject){
				if(CONFIG.useredis){
					if(PROCSTORE.getState().stats.redis[CLUSTER.worker.id]){
						res.locals.redis.get(res.locals.uid+'-data', function (err, reply) {
							if(err || !reply){
								return resolve('next');
							} else {
								let _reply = JSON.parse(reply.toString());
								if((_reply.datetime+(res.locals._timeout2*1000)) > Date.now()){
									if(_reply.hashresult === FUNCTIONS.hasher("")){	//хэш от пустого массива (конец задания)
										res.set({'Content-Type': 'application/json; charset=utf-8', 
													'ics-cache-hash': _reply.hash,
													'ics-cache-hashresult': _reply.hashresult});
										res.status(204).send();
										res.locals.cachel = 'L1';
										return reject('close');
									} else {
										res.locals.redis.strlen(res.locals.uid, function(err, length) { 
											if(err || (length === 0)){
												return resolve('next');
											} else {
												if(_reply.hashresult === req.get('ics-cache-hashresult')){	//данные не изменились
													res.set({
														'Content-Type': 'application/json; charset=utf-8',
														'ics-cache-hash': _reply.hash,
														'ics-cache-hashresult': _reply.hashresult
													});
													res.status(208).json({status:"ok", message:'Already Reported'});
													res.locals.cachel = 'L1';
													return reject('close');
												} else {
													let headers = {
														'Content-Type': 'application/json; charset=utf-8',
														'ics-cache-hash': _reply.hash,
														'ics-cache-hashresult': _reply.hashresult
													};
													if(CONFIG.usegzip && res.locals.acceptgzip){
														headers['Content-Encoding'] = 'gzip';
													}
													if(!(CONFIG.usegzip && !res.locals.acceptgzip)){
														headers['Content-Length'] = length;
													}
													try{
														res.set(headers);
													}catch(err){
														LOGGER.warn(err);
													}finally{
														let readerStream = res.locals.redis.readStream(res.locals.uid);
														const gunzipper = ZLIB.createGunzip();	//поток декомпрессии
														let openedStream = true;
														const closerErrStream = function(err){ //обработка ошибок
															if(openedStream){
																openedStream = false;
																readerStream.unpipe();
																gunzipper.unpipe();
																if((gunzipper._readableState.destroyed !== true) || (gunzipper._writableState.destroyed !== true)){
																	gunzipper.destroy(err);
																}
																if(readerStream._readableState.destroyed !== true){
																	readerStream.destroy(err);
																}
																if(err){
																	LOGGER.warn(err);
																	res.status(500);
																	res.locals.cachel = 'L1';
																	return reject('close');
																} else {
																	res.locals.cachel = 'L1';
																	return reject('close');
																}
															}
														}
														if(CONFIG.usegzip && !res.locals.acceptgzip){
															readerStream.on('error', closerErrStream).pipe(gunzipper).on('end', () => {
																closerErrStream();
															}).on('error', closerErrStream).pipe(res.status(201));
														} else{
															readerStream.on('error', closerErrStream).on('end', () => {
																closerErrStream();
															}).pipe(res.status(201));
														}
													}
												}
											}
										});
									}
								} else {
									return resolve('next');
								}
							}
						});
					} else {
						return resolve('next');
					}
				} else {
					return resolve('next');
				}
			});
		}).then(function(){
		//проверяю, что хэш в бд актуален, CACHE LVL 2 (DRIVE)
			return new Promise(function (resolve, reject){
				if(PROCSTORE.getState().stats.mongodb[CLUSTER.worker.id]){
					res.locals.temptimeout = CONFIG.cache*1000;	
					MONGODB.find(res.locals.mongodb.db(CONFIG.mongodb.database).collection('Request'), {"find":{"type":"hash", "hash":res.locals.uid+'-'+CONFIG.servername}, sort:{"_id": 1}, limit:1, skip:0}).then(function(data){
						if(data.length !== 0){
							let timeout = data[0].timeout;
							res.locals._timeout = timeout;	//таймаут из бд в миллисекундах
							res.locals.temptimeout = timeout;	//эталонный таймаут в миллисекундах (сначала = CONFIG.cache(задан в секундах), далее равен бд (задан в миллисекундах), далее равен ics-cache-timeout (задан в минутах), если он больше CONFIG.cache)
							try{
								let _timeout = parseInt(req.get('ics-cache-timeout'), 10) * 60 * 1000;	//в миллисекундах
								if((typeof(_timeout) === 'number') && !isNaN(_timeout) && (_timeout >= (CONFIG.cache*1000))){	//если передан таймаут, проверяем что он число и больше CONFIG.cache секунд
									res.locals.temptimeout = _timeout;
								}
							} catch (e){
								LOGGER.warn(e);
							}
							if(((Date.now() - res.locals.temptimeout) < data[0].timestamp) && ((((new Date(data[0].timestamp)).getHours() >= 6) && ((new Date(data[0].timestamp)).getDay() === (new Date(Date.now())).getDay())) || ((new Date(Date.now())).getHours() < 6))){ //(проверяем что хэш не истек и час старше 6 (примерный период обменов) или текущее время меньше 6)
								let _tmp = LODASH.clone(data[0]);
								_tmp.hash = _tmp.hash.replace('-'+CONFIG.servername, '');	//удаляю servername
								res.locals.hash = _tmp;
							}
						}
						return resolve('next');
					}).catch(function(){
						res.status(417).json({status:"error", message:'Expectation Failed'});
						res.locals.cachel = 'L2';
						return reject('close');
					});
				} else {
					res.status(500).json({status:"error", message:'Database connection error'});
					return reject('close');
				}
			});
		}).then(function(){
		//проверяю что файл хэша присутствует и отдаю данные, если это так отдаю его в потоке
			return new Promise(function (resolve, reject){
				if(typeof(res.locals.hash) === 'object'){
					if(res.locals.hash.hashresult === FUNCTIONS.hasher("")){	//хэш от пустого массива (конец задания)
						res.set({'Content-Type': 'application/json; charset=utf-8', 
									'ics-cache-hash': res.locals.hash.hash,
									'ics-cache-hashresult': res.locals.hash.hashresult,
									'ics-cache-timeout': FUNCTIONS.mstomin(res.locals.temptimeout)});
						res.status(204).send();
						//если таймаут изменился - обновляю его
						if((typeof(res.locals._timeout) === 'number') && (res.locals._timeout !== res.locals.temptimeout)){
							MONGODB.updhash(res.locals.mongodb.db(CONFIG.mongodb.database).collection('Request'), res.locals.hash.hash+'-'+CONFIG.servername, res.locals.temptimeout).catch(function(){}).finally(function(){
								res.locals.cachel = 'L2';
								return reject('close');
							});
						} else {
							res.locals.cachel = 'L2';
							return reject('close');
						}
					} else {
						FS.stat(PATH.join(CONFIG.cachedir, res.locals.hash.hash), function(err, stats){
							try{
								if(err){
									throw err;
								} else {
									if(stats.isFile()){
										if(res.locals.hash.hashresult === req.get('ics-cache-hashresult')){	//данные не изменились
											res.set({'Content-Type': 'application/json; charset=utf-8', 
														'ics-cache-hash': res.locals.hash.hash,
														'ics-cache-hashresult': res.locals.hash.hashresult,
														'ics-cache-timeout': FUNCTIONS.mstomin(res.locals.temptimeout)});
											res.status(208).json({status:"ok", message:'Already Reported'});
											//если таймаут изменился - обновляю его
											if((typeof(res.locals._timeout) === 'number') && (res.locals._timeout !== res.locals.temptimeout)){
												MONGODB.updhash(res.locals.mongodb.db(CONFIG.mongodb.database).collection('Request'), res.locals.hash.hash+'-'+CONFIG.servername, res.locals.temptimeout).catch(function(){}).finally(function(){
													res.locals.cachel = 'L2';
													return reject('close');
												});
											} else {
												res.locals.cachel = 'L2';
												return reject('close');
											}
										} else {
											const readerStream = FS.createReadStream(PATH.join(CONFIG.cachedir, res.locals.hash.hash));
											let headers = {'Content-Type': 'application/json; charset=utf-8', 
														'ics-cache-hash': res.locals.hash.hash,
														'ics-cache-hashresult': res.locals.hash.hashresult,
														'ics-cache-timeout': FUNCTIONS.mstomin(res.locals.temptimeout)};
											if(CONFIG.usegzip && res.locals.acceptgzip){
												headers['Content-Encoding'] = 'gzip';
											}
											if(!(CONFIG.usegzip && !res.locals.acceptgzip)){
												headers['Content-Length'] = stats.size;
											}
											try{
												res.set(headers);
											}catch(err){
												LOGGER.warn(err);
											}finally{
												const gunzipper = ZLIB.createGunzip();	//поток декомпрессии
												let openedStream = true;
												const closerErrStream = function(err){ //обработка ошибок
													if(openedStream){
														openedStream = false;
														readerStream.unpipe();
														gunzipper.unpipe();
														if((gunzipper._readableState.destroyed !== true) || (gunzipper._writableState.destroyed !== true)){
															gunzipper.destroy(err);
														}
														if(readerStream._readableState.destroyed !== true){
															readerStream.destroy(err);
														}
														if(err){
															LOGGER.warn(err);
															res.status(500);
															res.locals.cachel = 'L2';
															return reject('close');
														} else {
															res.locals.cachel = 'L2';
															//если таймаут изменился - обновляю его
															if((typeof(res.locals._timeout) === 'number') && (res.locals._timeout !== res.locals.temptimeout)){
																MONGODB.updhash(res.locals.mongodb.db(CONFIG.mongodb.database).collection('Request')+'-'+CONFIG.servername, res.locals.hash.hash, res.locals.temptimeout).catch(function(){}).finally(function(){
																	return reject('close');
																});
															} else {
																return reject('close');
															}
														}
													}
												}
												if(CONFIG.usegzip && !res.locals.acceptgzip){
													readerStream.on('error', closerErrStream).pipe(gunzipper).on('end', () => {
														closerErrStream();
													}).on('error', closerErrStream).pipe(res);
												} else{
													readerStream.on('error', closerErrStream).on('end', () => {
														closerErrStream();
													}).pipe(res);
												}
											}
										}
									} else {
										return resolve('next');
									}
								}
							} catch(e){
								return resolve('next');
							}
						});
					}
				} else {
					return resolve('next');
				}
			});
		}).then(function(){
		//отправляю запрос в бд
			return new Promise(function (resolve, reject){
				if(PROCSTORE.getState().stats.mongodb[CLUSTER.worker.id]){
					PROCSTORE.dispatch({type:'ADD_TASK', payload:{hash: res.locals.uid, worker: CLUSTER.worker.id, request:res.locals.json}}); //старт кэширования
					(function(){
						if(self.type === 'query'){
							return MONGODB.findcursor(res.locals.mongodb.db(CONFIG.mongodb.database).collection('Data'), res.locals.json, res.locals.uid);
						} else if(self.type === 'bigdata'){
							return MONGODB.bigdatacursor(res.locals.mongodb.db(CONFIG.mongodb.database).collection('Data'), res.locals.json, res.locals.uid);
						}
					})().then(function(cursor){
						LOGGER.debug('CURSOR STARTED '+res.locals.uid); 
						let closecursor = function(){
							if(cursor && (typeof(cursor.close) === 'function')){
								cursor.close().then(function(){ 
									LOGGER.debug('CURSOR CLOSED '+res.locals.uid); 
								}).catch(function(){ 
									LOGGER.debug('CURSOR NOT CLOSED'+res.locals.uid); 
								});
							}
						}
						try{
							const filestream = FS.createWriteStream(PATH.join(CONFIG.cachedir, res.locals.uid));	//поток записи в файл
							const hash = CRYPTO.createHash('sha1');	//поток генерации хэша
							const stringifer = new OBJECTSTREAM.Stringifer('[', ',', ']'); //поток преобразования object->string
							const tempstream = cursor.stream({transform: x => transformDocs(x)});	//поток чтения данных из базы
							const gzipper = ZLIB.createGzip();	//поток сжатия
							let openedStream = true;
							const closerErrStream = function(err){ //обработка ошибок в 3-х потоках
								if(openedStream){
									openedStream = false;
									tempstream.unpipe(); //отвязываем потоки
									stringifer.unpipe();
									gzipper.unpipe();
									if((tempstream._readableState.destroyed !== true) || (tempstream._writableState.destroyed !== true)){	//уничтожаем потоки, если существуют
										tempstream.destroy(err);
									}
									if((stringifer._readableState.destroyed !== true) || (stringifer._writableState.destroyed !== true)){
										stringifer.destroy(err);
									}
									if((hash._readableState.destroyed !== true) || (hash._writableState.destroyed !== true)) {
										hash.destroy(err);
									}
									if((gzipper._readableState.destroyed !== true) || (gzipper._writableState.destroyed !== true)){
										gzipper.destroy(err);
									}
									if(filestream._writableState.destroyed !== true){
										filestream.destroy(err);
									}
									if(err){
										FS.unlink(PATH.join(CONFIG.cachedir, res.locals.uid), (error) => {	//удаляем файл
											if (!error) {
												LOGGER.warn('Удален некорректный файл: ' + res.locals.uid);
											}
										});
										LOGGER.warn('Ошибка записи результатов запроса в файл: ' + err + ' запрос к API: ' + JSON.stringify(req.body));
										PROCSTORE.dispatch({type:'DEL_TASK', payload:{hash: res.locals.uid, worker: CLUSTER.worker.id}});
										res.status(500).json({status:"error", message:'Internal Server Error'}); 	//после отправки хидера вызывает эксепшн, поэтому без кода
										res.locals.cachel = 'L2';
										closecursor();
										return reject('close');
									} else {
										res.locals.hashresult = hash.digest('hex');
										if(res.locals.hashresult === FUNCTIONS.hasher("[]")){
											res.locals.hashresult = FUNCTIONS.hasher("");
											FS.unlink(PATH.join(CONFIG.cachedir, res.locals.uid), (error) => {	//удаляем файл
												if (!error) {
													LOGGER.debug('Удален пустой файл: ' + res.locals.uid);
												}
											});
										}
										MONGODB.update(res.locals.mongodb.db(CONFIG.mongodb.database).collection('Request'), {"type":"hash", "hash":res.locals.uid+'-'+CONFIG.servername}, {"type":"hash", "hash":res.locals.uid+'-'+CONFIG.servername,"timeout":res.locals.temptimeout,"timestamp":Date.now(),"hashresult":res.locals.hashresult}).catch(function(){}).finally(function(){
											PROCSTORE.dispatch({type:'DEL_TASK', payload:{hash: res.locals.uid, worker: CLUSTER.worker.id}});
											closecursor();
											return resolve('next');
										});
									}
								}
							};
							cursor.on('error', closerErrStream);
							hash.on('error', closerErrStream);
							if(CONFIG.usegzip){
								tempstream.on("error", closerErrStream).pipe(stringifer).on('data', function(string){
									hash.update(string);
								}).on("error", closerErrStream).pipe(gzipper).on("error", closerErrStream).pipe(filestream).on("error", closerErrStream);
							} else {
								tempstream.on("error", closerErrStream).pipe(stringifer).on('data', function(string){
									hash.update(string);
								}).on("error", closerErrStream).pipe(filestream).on("error", closerErrStream);
							}
							filestream.on("finish", function(){ //в случае успешной записи возвращаем 201 и новый hash
								closerErrStream();
							});
						} catch(e){ 
							res.status(417).json({status:"error", message:'Expectation Failed'});
							PROCSTORE.dispatch({type:'DEL_TASK', payload:{hash: res.locals.uid, worker: CLUSTER.worker.id}});
							res.locals.cachel = 'L2';
							closecursor();
							return reject('close');
						}
					}).catch(function(e){
						res.status(417).json({status:"error", message:'Expectation Failed'});
						LOGGER.error('Ошибка создания курсора: '+e);
						PROCSTORE.dispatch({type:'DEL_TASK', payload:{hash: res.locals.uid, worker: CLUSTER.worker.id}});
						res.locals.cachel = 'L2';
						return reject('close');
					});		
				} else {
					res.status(500).json({status:"error", message:'Database connection error'});
					return reject('close');
				}
			});
		}).then(function(){
		//отдаю файл из кэша
			return new Promise(function (resolve, reject){
				if(res.locals.hashresult === FUNCTIONS.hasher("")){
					res.set({
						'Content-Type': 'application/json; charset=utf-8', 
						'ics-cache-hash': res.locals.uid,
						'ics-cache-hashresult': res.locals.hashresult,
						'ics-cache-timeout': FUNCTIONS.mstomin(res.locals.temptimeout)
					});
					res.status(204).send();
					res.locals.cachel = 'L2';
					reject('close');
				} else {
					FS.stat(PATH.join(CONFIG.cachedir, res.locals.uid), function(err, stats){
						try{
							if(err){
								throw err;
							} else {
								if(stats.isFile()){
									if(res.locals.hashresult === req.get('ics-cache-hashresult')){	//данные не изменились
										res.set({
											'Content-Type': 'application/json; charset=utf-8',
											'ics-cache-hash': res.locals.uid,
											'ics-cache-hashresult': res.locals.hashresult,
											'ics-cache-timeout': FUNCTIONS.mstomin(res.locals.temptimeout)
										});
										res.status(208).json({status:"ok", message:'Already Reported'});
										res.locals.cachel = 'L2';
										return reject('close');
									} else { 
										const readerStream = FS.createReadStream(PATH.join(CONFIG.cachedir, res.locals.uid));
										let headers = {
											'Content-Type': 'application/json; charset=utf-8', 
											'ics-cache-hash': res.locals.uid,
											'ics-cache-hashresult': res.locals.hashresult,
											'ics-cache-timeout': FUNCTIONS.mstomin(res.locals.temptimeout)
										};
										if(CONFIG.usegzip && res.locals.acceptgzip){
											headers['Content-Encoding'] = 'gzip';
										}
										if(!(CONFIG.usegzip && !res.locals.acceptgzip)){
											headers['Content-Length'] = stats.size;
										}
										try{
											res.set(headers);
										}catch(err){
											LOGGER.warn(err);
										}finally{
											const gunzipper = ZLIB.createGunzip();	//поток декомпрессии
											let openedStream = true;
											const closerErrStream = function(err){ //обработка ошибок
												if(openedStream){
													openedStream = false;
													readerStream.unpipe();
													gunzipper.unpipe();
													if((gunzipper._readableState.destroyed !== true) || (gunzipper._writableState.destroyed !== true)){
														gunzipper.destroy(err);
													}
													if(readerStream._readableState.destroyed !== true){
														readerStream.destroy(err);
													}
													if(err){
														LOGGER.warn(err);
														res.status(500);
														res.locals.cachel = 'L2';
														return reject('close');
													} else {
														res.locals.cachel = 'L2';
														return reject('close');
													}
												}
											}
											if(CONFIG.usegzip && !res.locals.acceptgzip){
												readerStream.on('error', closerErrStream).pipe(gunzipper).on('end', () => {
													closerErrStream();
												}).on('error', closerErrStream).pipe(res.status(201));
											} else{
												readerStream.on('error', closerErrStream).on('end', () => {
													closerErrStream();
												}).pipe(res.status(201));
											}
										}
									}
								}
							}
						} catch(e){
							res.status(500).json({status:"error", message:'Internal Server Error'});
							res.locals.cachel = 'L2';
							return reject('close');
						}
					});
				}
			});
		}).catch(function(error){
			if(error !== 'close'){
				LOGGER.error('Неизвестная ошибка запроса: '+error);
				return res.status(500).json({status:"error", message:'Internal Server Error'});
			}
			return;
		});
	}); 	
	return self.query;
}

module.exports.query = new Query('query');
module.exports.bigdata = new Query('bigdata');