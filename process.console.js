/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	CONSOLE
 *	Консоль сервера (включая установку службы)
 *
 */
 
 "use strict"

if(parseInt(process.version.split(".")[0].substr(1,2), 10) < 10){
	Promise = require("bluebird");
	require("console.table");
	console.tablecustom = console.table;
	console.table = function(a){ console.tablecustom(a); };
}

//подгружаемые библиотеки
var READLINE = require('readline'),
	PATH = require('path'),
	OS = require('os');
	
	
var NW;
if(OS.platform() === 'win32'){	
	NW = require('node-windows');
} else {
	NW = {
		isAdminUser: function(callback){ callback(true); }
	};
}
	
//подгружаемые модули	
var SERVICE = require(PATH.join(__dirname, 'settings.service.js')),
	SERVICEREALNAME = require(PATH.join(__dirname, 'settings.parametrs.js')).servicerealname,
	SERVICENAME = require(PATH.join(__dirname, 'settings.parametrs.js')).servicename;

function help(arr){
	console.table(arr, ['команда', 'значение']);
}
NW.isAdminUser(function(isAdmin){
	if (isAdmin) {
		
		let stdout = console;
		
		SERVICE.on('install',function(){
			if((SERVICE.exists && (OS.platform() === 'win32')) || ((typeof(SERVICE.exists) === 'function') && SERVICE.exists() && (OS.platform() === 'linux'))){
				stdout.log('Установка службы '+SERVICENAME+' выполнена.');
			}
		});
		SERVICE.on('uninstall',function(){
			if(((!SERVICE.exists) && (OS.platform() === 'win32')) || ((typeof(SERVICE.exists) === 'function') && (!SERVICE.exists()) && (OS.platform() === 'linux'))){
				stdout.log('Удаление службы '+SERVICENAME+' выполнено.');
			}
		});
		SERVICE.on('start',function(){
			stdout.log('Запуск службы '+SERVICENAME+' выполнен.');
		});
		SERVICE.on('stop',function(){
			stdout.log('Остановка службы '+SERVICENAME+' выполнена.');
		});
		let arr = [];
		let commands = {
			"--help": {
				title: "Вызов текущей справки.",
				exec: function(){ help(arr);}
			},
			"--install": {
				title: "Установка службы "+SERVICENAME+".",
				exec: function(){
					if(((!SERVICE.exists) && (OS.platform() === 'win32')) || ((typeof(SERVICE.exists) === 'function') && (!SERVICE.exists()) && (OS.platform() === 'linux'))){
						SERVICE.install();
					}else if((OS.platform() === 'win32') || (OS.platform() === 'linux')){
						stdout.error("Служба "+SERVICENAME+" уже существует!");
					} else {
						stdout.error("Не поддерживаемый тип платформы!");
					}
				}
			},
			"--remove": {
				title: "Удаление службы "+SERVICENAME+".",
				exec: function(){
					if((SERVICE.exists && (OS.platform() === 'win32')) || ((typeof(SERVICE.exists) === 'function') && SERVICE.exists() && (OS.platform() === 'linux'))){
						SERVICE.uninstall();
					}else if((OS.platform() === 'win32') || (OS.platform() === 'linux')){
						stdout.error("Службы "+SERVICENAME+" не существует!");
					} else {
						stdout.error("Не поддерживаемый тип платформы!");
					}
				}
			},
			"--restart": {
				title: "Перезапуск службы "+SERVICENAME+".",
				exec: function(){
					if((SERVICE.exists && (OS.platform() === 'win32')) || ((typeof(SERVICE.exists) === 'function') && SERVICE.exists() && (OS.platform() === 'linux'))){
					//новый вариант (с нормальной поддержкой windows)
						if(OS.platform() === 'win32'){
							NW.elevate('TASKKILL /FI "SERVICES EQ '+SERVICEREALNAME+'" /T /F&&EXIT 0');
							setTimeout(stdout.log, 1000, 'Остановка службы '+SERVICENAME+' выполнена.');
						} else {
							SERVICE.stop();
						}
						setTimeout(function(){
							SERVICE.start();
						}, 5000);
					}else if((OS.platform() === 'win32') || (OS.platform() === 'linux')){
						stdout.error("Службы "+SERVICENAME+" не существует!");
					} else {
						stdout.error("Не поддерживаемый тип платформы!");
					}
				}
			},
			"--start": {
				title: "Запуск службы "+SERVICENAME+".",
				exec: function(){
					if((SERVICE.exists && (OS.platform() === 'win32')) || ((typeof(SERVICE.exists) === 'function') && SERVICE.exists() && (OS.platform() === 'linux'))){
						SERVICE.start();
					}else if((OS.platform() === 'win32') || (OS.platform() === 'linux')){
						stdout.error("Службы "+SERVICENAME+" не существует!");
					} else {
						stdout.error("Не поддерживаемый тип платформы!");
					}
				}
			},
			"--stop": {
				title: "Остановка службы "+SERVICENAME+".",
				exec: function(){
					if((SERVICE.exists && (OS.platform() === 'win32')) || ((typeof(SERVICE.exists) === 'function') && SERVICE.exists() && (OS.platform() === 'linux'))){
					//новый вариант (с нормальной поддержкой windows)
						if(OS.platform() === 'win32'){
							NW.elevate('TASKKILL /FI "SERVICES EQ '+SERVICEREALNAME+'" /T /F');
							setTimeout(stdout.log, 1000, 'Остановка службы '+SERVICENAME+' выполнена.');
						} else {
							SERVICE.stop();
						}
					}else if((OS.platform() === 'win32') || (OS.platform() === 'linux')){
						stdout.error("Службы "+SERVICENAME+" не существует!");
					} else {
						stdout.error("Не поддерживаемый тип платформы!");
					}
				}
			}
		};
		for(const key in commands){
			arr.push({"команда":key, "значение":commands[key].title});
		}
		let flag = false;
		for(let i =0; i<process.argv.length; i++){
			if(typeof(commands[process.argv[i]]) !== 'undefined'){
				commands[process.argv[i]].exec();
				flag = true;
			}
		}
		if(!flag){
			require(PATH.join(__dirname, 'module.logger.js'));
			help(arr);
			const rl = READLINE.createInterface({
				input: process.stdin,
				output: process.stdout,
				prompt: SERVICENAME+'> '
			});	
			rl.prompt();
			rl.on('line', (line) => {
				try{
					if(typeof(commands[line]) !== 'undefined'){
						commands[line].exec();
					} else {
						let _tmp = eval(line);
						if(typeof(_tmp) !== 'undefined'){
							console.log(_tmp);
						}
					}
				} catch(err) {
					stdout.error("Ошибка команды: "+err);
					help(arr);
				}
				rl.prompt();
			}).on('close', () => {
				stdout.log(SERVICENAME+' disconnected!');
				process.exit(0);
			});
		}
		
	} else {
		console.error('Запуск требует административных привелегий.');
	}
});