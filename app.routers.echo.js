/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  APP.ROUTERS.ECHO
 *	Маршрут для /v3/echo (ЭХО-сервер)
 *
 */

"use strict"

//подгружаемые библиотеки
var EXPRESS = require('express'),
	CONFIG = require('config'),
	OBJECTSTREAM = require('@sergdudko/objectstream'),
	PATH = require('path'),
	STREAM = require('stream'),
	ZLIB = require('zlib');
	
//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	VALIDATORS = require(PATH.join(__dirname, 'module.validators.js'));


var echo = EXPRESS.Router();

echo.all('*', function (req, res){
	(new Promise(function (resolve, reject){
		if(CONFIG.bantimeout){
			if(
				(res.locals.permission['all'] && (res.locals.permission['all'].indexOf('w') !== -1)) ||
				(res.locals.permission['echo'] && (res.locals.permission['echo'].indexOf('w') !== -1))
			){
				return resolve('next');
			} else if(res.locals.permission['custom'] && (Array.isArray(res.locals.permission['custom']['w']))){
				res.locals.permcustom = res.locals.permission['custom']['w'];
				return resolve('next');
			} else {
				res.status(403).json({
					status: "error",
					message: "Permission denied"
				});
				return reject('close');
			}
		} else {
			return resolve('next');
		}
	})).then(function(){
		return new Promise(function (resolve, reject){ 
			if((req.method === 'POST') || (req.method === 'DELETE')){
				return resolve('next');
			} else{
				res.status(405).json({status:"error", message:'Method Not Allowed'});
				return reject('close');
			}
		});
	}).then(function(){
		return new Promise(function (resolve, reject){
			//обработка входящего потока
			const parser = new OBJECTSTREAM.Parser('[',',',']'); //поток парсинга json-документов из массива
			const mbstring = new STREAM.Transform({	//обработка мультибайтовых символов без присвоенной кодировки может срабатывать некорректно
				transform(_buffer, encoding, callback) {
					this.push(_buffer)
					return callback();
				}
			});
			mbstring.setEncoding('utf8');
			const stringifer = new OBJECTSTREAM.Stringifer('[',',',']'); //поток парсинга json-документов в строку
			const gunzipper = ZLIB.createGunzip();	//декомпрессия входящего потока
			let openedStream = true;
			const closer = function(err){	//закрытие потоков
				if(openedStream){
					openedStream = false;
					req.unpipe();
					gunzipper.unpipe();
					mbstring.unpipe();
					parser.unpipe();
					stringifer.unpipe();
					if((gunzipper._readableState.destroyed !== true) || (gunzipper._writableState.destroyed !== true)) {
						gunzipper.destroy(err);
					}
					if((mbstring._readableState.destroyed !== true) || (mbstring._writableState.destroyed !== true)) {
						mbstring.destroy(err);
					}
					if((parser._readableState.destroyed !== true) || (parser._writableState.destroyed !== true)) {
						parser.destroy(err);
					}
					if((stringifer._readableState.destroyed !== true) || (stringifer._writableState.destroyed !== true)) {
						stringifer.destroy(err);
					}
					if(err){
						res.status(500).end(JSON.stringify({status:"error", message: err.toString()}));
						LOGGER.warn('ECHO-> '+err);
						return reject('close');
					} else {
						return resolve('next');
					}
				}
			}
			gunzipper.on('error',function(err){
				closer(err);
			});
			if(res.locals.acceptgzip){
				if(res.locals.gzippedbody){
					res.set({'Content-Type': 'application/json; charset=utf-8', 'Content-Encoding':'gzip'});
					req.on('error',function(err){	//объединяем потоки
						closer(err);
					}).on('close', function(){	//клиент уничтожил запрос
						closer(new Error('Client destroyed connection!'));
					}).pipe(gunzipper).pipe(mbstring).on('error',function(err){
						closer(err);
					}).pipe(parser).on('error',function(err){
						closer(err);
					}).on('data', function(d){
						let _doc = VALIDATORS.data(d);
						if(typeof(_doc) !== 'object'){
							closer(_doc);
						}
					}).pipe(stringifer).on('error',function(err){
						closer(err);
					}).pipe(res.locals.gzipper);
				} else {
					res.set({'Content-Type': 'application/json; charset=utf-8', 'Content-Encoding':'gzip'});
					req.on('error',function(err){	//объединяем потоки
						closer(err);
					}).on('close', function(){	//клиент уничтожил запрос
						closer(new Error('Client destroyed connection!'));
					}).pipe(mbstring).on('error',function(err){
						closer(err);
					}).pipe(parser).on('error',function(err){
						closer(err);
					}).on('data', function(d){
						let _doc = VALIDATORS.data(d);
						if(typeof(_doc) !== 'object'){
							closer(_doc);
						}
					}).pipe(stringifer).on('error',function(err){
						closer(err);
					}).pipe(res.locals.gzipper);
				}					
			} else {
				if(res.locals.gzippedbody){
					res.set({'Content-Type': 'application/json; charset=utf-8'});
					req.on('error',function(err){	//объединяем потоки
						closer(err);
					}).on('close', function(){	//клиент уничтожил запрос
						closer(new Error('Client destroyed connection!'));
					}).pipe(gunzipper).pipe(mbstring).on('error',function(err){
						closer(err);
					}).pipe(parser).on('error',function(err){
						closer(err);
					}).on('data', function(d){
						let _doc = VALIDATORS.data(d);
						if(typeof(_doc) !== 'object'){
							closer(_doc);
						}
					}).pipe(stringifer).on('error',function(err){
						closer(err);
					}).pipe(res.status(200));
				} else {
					res.set({'Content-Type': 'application/json; charset=utf-8'});
					req.on('error',function(err){	//объединяем потоки
						closer(err);
					}).on('close', function(){	//клиент уничтожил запрос
						closer(new Error('Client destroyed connection!'));
					}).pipe(mbstring).on('error',function(err){
						closer(err);
					}).pipe(parser).on('error',function(err){
						closer(err);
					}).on('data', function(d){
						let _doc = VALIDATORS.data(d);
						if(typeof(_doc) !== 'object'){
							closer(_doc);
						}
					}).pipe(stringifer).on('error',function(err){
						closer(err);
					}).pipe(res.status(200));
				}
			}				
		});
	}).catch(function(error){
		if(error !== 'close'){
			LOGGER.error('Неизвестная ошибка запроса: '+error);
			return res.status(500);
		}
		return;
	});
});

module.exports = echo;