/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  MONGODB
 *	Функции MongoDB
 *
 */

"use strict"

//подгружаемые библиотеки
var LODASH = require('lodash'),
	MONGOCLIENT = require('mongodb').MongoClient,
	CLUSTER = require('cluster'),
	CONFIG = require('config'),
	PATH = require('path');
	
//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	SENDMAIL = require(PATH.join(__dirname, 'module.sendmail.js')),
	FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js'));

//поиск в Mongodb, возвращает всю выборку, но жрет много памяти (грязная, за счет курсора collection)
function MongoDBFind(collection, parsedData){
	let parsedDataclear = LODASH.clone(parsedData);	//чистим объекты, т.е. превращаем ссылки на объекты в новые объекты на всякий случай 
	let requires = {};
	if(Array.isArray(parsedDataclear.requires)){
		for(let i = 0; i < parsedDataclear.requires.length; i++){
			requires[parsedDataclear.requires[i]] = true;
		}
	}
	return new Promise(function (resolve, reject){
		try {
			collection.find(parsedDataclear.find, {projection:requires}).sort(parsedDataclear.sort).limit(parsedDataclear.limit).skip(parsedDataclear.skip).maxTimeMS(300000).toArray(function(err, docs) {  //таймаут поиска 5 минут
				try{
					if (err) {
						throw err;
					} else {
						resolve(LODASH.clone(docs));
					}
				} catch(e){
					LOGGER.error("Ошибка поискового запроса к mongodb:" + e);
					reject(e);
				}
			});
		} catch (e){
			LOGGER.error("Ошибка поискового запроса к mongodb:" + e);
			reject(e);
		}
	});
}

//поиск в Mongodb, возвращает курсор (грязная, за счет курсора collection)
function MongoDBFindCursor(collection, parsedData, requesthash){
	let parsedDataclear = LODASH.clone(parsedData);	//чистим объекты, т.е. превращаем ссылки на объекты в новые объекты на всякий случай 
	let requires = {};
	if(Array.isArray(parsedDataclear.requires)){
		for(let i = 0; i < parsedDataclear.requires.length; i++){
			requires[parsedDataclear.requires[i]] = true;
		}
	}
	return new Promise(function (resolve, reject){
		try {
			if(parsedDataclear.sort.version === 1){	//задана сортировка по версии
				let _ltVersion = FUNCTIONS.version(Date.now() - 2*60*1000); //отодвигаю на 2 минуты назад
				let _addLTVersion = function(object){
					let _object = LODASH.clone(object);
					if((typeof(_object) === 'object') && (_object !== null)){
						if((typeof(_object.version) === 'object') && ((typeof(_object.version["$gt"]) === 'string') || (typeof(_object.version["$lt"]) === 'string'))){
							if((typeof(_object.version["$gt"]) === 'string') && (typeof(_object.version["$lt"]) === 'undefined')){  //отодвигаю только поля, где есть $gt и нет $lt
								if(_ltVersion > _object.version["$gt"]){
									_object.version["$lt"] = _ltVersion;
								} else {
									_object.version["$gt"] = "999999999999999999999999999999";	//вернет ноль документов
								}
							} else if((typeof(_object.version["$gt"]) === 'string') && (typeof(_object.version["$lt"]) === 'string')){  //отодвигаю поля где есть $gt и $lt
								if(_ltVersion > _object.version["$gt"]){
									if(_ltVersion < _object.version["$lt"]){
										_object.version["$lt"] = _ltVersion;
									}
								} else {
									_object.version["$gt"] = "999999999999999999999999999999";	//вернет ноль документов
									delete _object.version["$lt"];
								}
							} else {
								if(_ltVersion < _object.version["$lt"]){
									_object.version["$lt"] = _ltVersion;
								}
							}
						}
						for(const key in _object){
							if(typeof(_object[key]) === "object"){
								_object[key] = _addLTVersion(_object[key]);
							}
						}
					}
					return _object;
				}
				parsedDataclear.find = _addLTVersion(parsedDataclear.find);
			}
			LOGGER.debug(requesthash+' :'+JSON.stringify(parsedDataclear.find));
			let cursor = collection.find(parsedDataclear.find, {projection:requires}).sort(parsedDataclear.sort).limit(parsedDataclear.limit).skip(parsedDataclear.skip).maxTimeMS(600000);   //таймаут курсора 10 минут //.addCursorFlag('noCursorTimeout', true)
			resolve(cursor);
		} catch (e){
			LOGGER.error("Ошибка поискового запроса к mongodb:" + e);
			reject(e);
		}
	});
}

//аггрегация в Mongodb, возвращает курсор (грязная, за счет курсора collection)
function MongoDBAggregationCursor(collection, parsedData, requesthash){
	let parsedDataclear = LODASH.clone(parsedData);	//чистим объекты, т.е. превращаем ссылки на объекты в новые объекты на всякий случай 
	return new Promise(function (resolve, reject){
		try {
			LOGGER.debug(requesthash+' :'+JSON.stringify(parsedDataclear));
			let cursor = collection.aggregate(parsedDataclear).maxTimeMS(3600000);   //таймаут курсора 60 минут //.addCursorFlag('noCursorTimeout', true)
			resolve(cursor);
		} catch (e){
			LOGGER.error("Ошибка поискового запроса к mongodb:" + e);
			reject(e);
		}
	});
}

//функция обновления данных в бд (грязная, за счет коннекта к бд)
function MongoDBUpdHash(collection, hash, timeout){ 
	return new Promise(function (resolve, reject){
		collection.updateOne({"type":"hash", "hash":hash}, {$set: {"timeout" : timeout}}, function(err, data){
			try{
				if(err){
					throw err;
				} else {
					if(data.result.ok >1){
						LOGGER.error('Подозрение на коллизию в базе данных: ' + hash);
					} else {
						LOGGER.debug('Данные в базе обновлены.');
					}
					resolve('ok');
				}
			} catch(e){
				LOGGER.error('Ошибка обновления документа:' + e);
				reject(e);
			}
		});
	});
}

//функция обновления/добавления документа в коллекции
function MongoDBUpdate(collection, find, updates, deletes){
	return new Promise(function (resolve, reject){
		let query;
		if(typeof(deletes) !== 'undefined'){
			query = {$set: updates, $unset: deletes};
		} else {
			query = {$set: updates};
		}
		collection.updateOne(find, query, {upsert: true}, function(err, data){
			try{
				if(err){
					throw err;
				} else {						
					if(data.result.ok >1){
						LOGGER.error('Подозрение на коллизию в базе данных: ' + JSON.stringify(find));
					} else {
						LOGGER.debug('Данные в базе обновлены.');
					}
					resolve('ok');
				}
			} catch(e){
				LOGGER.error('Ошибка обновления документа:' + e);
				reject(e);
			}
		});
	});
}

function MongoClient(){
/*	let options = {
		useNewUrlParser: true,
		poolSize: 20,								//пул подключений для каждого процесса, т.е. умножаем это число на число рабочих процессов
		socketTimeoutMS: 10800000,					//таймаут соединения
		connectTimeoutMS: 30000,					//таймаут установки соединения
		autoReconnect: true,						//восстановление соединения при сбое
		reconnectTries: Number.MAX_SAFE_INTEGER,	//попыток подключения
		reconnectInterval: 1000,					//интервал попытки подключения
		keepAlive: 0,								//постоянное соединение с 0мс
		bufferMaxEntries: 1,						//кол-во операций для буферизации (монга не доступна). Если 0 (в идеале так и нужно, чтобы не "вешать" коннекты), то в режиме кластера валит ошибку.
		appname: 'iRetailCloudServer',				//имя приложения в консоли mongo (нужна версия mongo выше 3.4)
		useUnifiedTopology: false					// новый механизм обнаружения и мониторинга сервера (не работает в версия до 3.3.5)
	};*/
	let options = {
		useNewUrlParser: true,
		poolSize: 20,								//пул подключений для каждого процесса, т.е. умножаем это число на число рабочих процессов
		socketTimeoutMS: 10800000,					//таймаут соединения
		keepAliveInitialDelay: 15000,
		connectTimeoutMS: 10000,					//таймаут установки соединения
		keepAlive: 0,								//постоянное соединение с 0мс
		appname: 'iRetailCloudServer',				//имя приложения в консоли mongo (нужна версия mongo выше 3.4)
		useUnifiedTopology: true,					//новый механизм обнаружения и мониторинга сервера (не работает в версия до 3.3.5)
		serverSelectionTimeoutMS: 2000				//время отклика на ошибку в новой (унифицированной) топологии
	};
	return new Promise(function(resolve, reject){
		let client = new MONGOCLIENT(CONFIG.mongodb.url, options);
		client.on('close', () => {
			if(CLUSTER.worker)
				PROCSTORE.dispatch({type:'STATUS_MONGO', payload:{status: false, worker: CLUSTER.worker.id}});
			LOGGER.warn('MONGODB-> Соединение с MongoDB потеряно!');
			let _message = 'Соединение с MongoDB закрыто!';
			SENDMAIL.send({message:LOGGER.datetime()+_message, theme:_message});
		});
		client.on('reconnect', () => {
			if(CLUSTER.worker)
				PROCSTORE.dispatch({type:'STATUS_MONGO', payload:{status: true, worker: CLUSTER.worker.id}});
			LOGGER.log('MONGODB-> Соединение с MongoDB восстановлено!');
			let _message = 'Соединение с MongoDB восстановлено!';
			SENDMAIL.send({message:LOGGER.datetime()+_message, theme:_message});
		});
		client.connect(function(err, connect) {
			if(err){
				reject(err);
			} else {
				if(CLUSTER.worker)
					PROCSTORE.dispatch({type:'STATUS_MONGO', payload:{status: true, worker: CLUSTER.worker.id}});
				LOGGER.log('MONGODB-> Соединение с MongoDB установлено!');
				resolve(connect);
			}
		});
	});
}

module.exports.find = MongoDBFind;
module.exports.findcursor = MongoDBFindCursor;
module.exports.updhash = MongoDBUpdHash;
module.exports.update = MongoDBUpdate;
module.exports.client = MongoClient;
module.exports.bigdatacursor = MongoDBAggregationCursor;
