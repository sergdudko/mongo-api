/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	PROCESS
 *  START (MASTER)
 *	Мастер процесс, занимается контролем состояния соркеров веб-сервера и саб-воркеров
 *
 */

"use strict"

//подгружаемые библиотеки
var CLUSTER = require('cluster'),
	OS = require('os'),
	PATH = require('path');
	
//подгружаемые модули
var SENDMAIL = require(PATH.join(__dirname, 'module.sendmail.js')),
	LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js'));

//глобальные переменные
var VERSION = require(PATH.join(__dirname, 'package.json')).version;

//создаю класс, который самостоятельно отслеживает состояние воркеров
var IRetailCloudServer = function(proc, count){
	let self = this;
	
	if(typeof(count) !== 'number'){
		count = 1;
	}
	
	switch(proc){
		case 'web':
			self.path = './process.worker.web.js';
			self.name = "Web-worker";
			break;
		case 'sub':
			self.path = './process.worker.sub.js';
			self.name = "Sub-worker";
			break;
		default:
			throw new Error('Не задан тип процесса');
	}
	
	self.exitHandler = function(exit, signal){
		PROCSTORE.dispatch({type:'CLEAR_TASK', payload: {
			allworkers:Object.keys(CLUSTER.workers)
		}});
		CLUSTER.setupMaster({
			exec: self.path
		});
		let worker = CLUSTER.fork();
		self.listener(CLUSTER.workers[worker.id]);
		if(signal){
			LOGGER.error(self.name+' умер и был пересоздан!');
		}
	}
	
	self.listener = function(worker){
		PROCSTORE.dispatch({type:'STATUS_WORKERS', payload: {
			id:worker.id, 
			pid: worker.process.pid, 
			script:worker.process.spawnargs[worker.process.spawnargs.length-1],
			status:worker.process.connected,
			allworkers:Object.keys(CLUSTER.workers)
		}});
		CLUSTER.workers[worker.id].on('exit', self.exitHandler);
	}
	
	for(let i = 0; i < count; i++){
		self.exitHandler();
	}	
};

//проверяем целостность папок
FUNCTIONS.prestart([ 'cache', 'config', 'mail', 'node_modules', 'public', 'logs' ]).then(()=>{
	
	//мастер активирован
	LOGGER.log('iRetail Cloud Server v'+VERSION+' started.');
	//запуск процессов веб-сервера (кол-во)
	new IRetailCloudServer("web", OS.cpus().length);
	//запуск вспомогательных процессов (кол-во)
	new IRetailCloudServer("sub");

	//устанавливаю версию приложения
	PROCSTORE.dispatch({type:'SET_VERSION', payload: VERSION});	
	
	//извещаю по почте о запуске приложения (пока выключил, бесит при перезапуске)
	SENDMAIL.send({message:LOGGER.datetime()+'iRetail Cloud Server v'+VERSION+' started.', theme:'iRetail Cloud Server v'+VERSION+' started.'});
	
}, (rej)=>{LOGGER.error(rej);});