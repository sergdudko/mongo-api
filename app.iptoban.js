/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  APP.IPTOBAN
 *	Модуль защиты от брутфорса
 *
 */

"use strict"

//подгружаемые библиотеки
var EXPRESS = require('express'),
	CONFIG = require('config'),
	PATH = require('path');
	
//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	MONGODB = require(PATH.join(__dirname, 'module.mongodb.js')),
	FUNCTIONS = require(PATH.join(__dirname, 'module.functions.js'));

//проверка на блокировку
var iptobanTEST = EXPRESS.Router();
iptobanTEST.use(function (req, res, next) {
	let ThisSocketDatetime, ThisSocketAttemp;
	if(CONFIG.bantimeout){
		let _bantimeout = CONFIG.bantimeout * 60 * 1000; //мин в мсек
		if(typeof(PROCSTORE.getState().iptoban) === 'object'){
			if(typeof(PROCSTORE.getState().iptoban[FUNCTIONS.replacer(req.ip, true)]) === 'object') {
				ThisSocketAttemp = PROCSTORE.getState().iptoban[FUNCTIONS.replacer(req.connection.remoteAddress, true)].attemp;
				ThisSocketDatetime = PROCSTORE.getState().iptoban[FUNCTIONS.replacer(req.connection.remoteAddress, true)].datetime;
			}
		}
		if(typeof(ThisSocketAttemp) !== 'number'){
			ThisSocketAttemp = 1;
		}
		if(typeof(ThisSocketDatetime) !== 'number'){
			ThisSocketDatetime = 0;
		}
		ThisSocketAttemp++;
		if((ThisSocketAttemp > CONFIG.banattemp) && ((ThisSocketDatetime + _bantimeout) > Date.now())){
			LOGGER.warn("IP2BAN-> Запрос с заблокированного адреса: " + req.connection.remoteAddress);
			return res.status(403).json({
				status: "error",
				message: 'Your ip is locked, wellcome after '+parseInt((_bantimeout-(Date.now()-ThisSocketDatetime)) / 60000)+' minutes.'
			});
		} else {
			return next();
		}
	} else {
		return next();
	}
});

//авторизация
var iptobanAuthALL = EXPRESS.Router();
iptobanAuthALL.use(function (req, res, next) {
	(new Promise(function(resolve, reject){
		let auth = req.get('authorization');
		if(!auth){
			return resolve(true);
		}else {
			let tmp = auth.split(' ');
			let buf = Buffer.from(tmp[1], 'base64');
			let plain_auth = buf.toString();
			let creds = plain_auth.split(':'); 
			let username = FUNCTIONS.replacer(creds[0], true);
			res.locals.username = creds[0];
			let password = creds[1];
			if((typeof(PROCSTORE.getState().authdb[username]) === 'object') && (PROCSTORE.getState().authdb[username].password === password) && ((PROCSTORE.getState().authdb[username].datetime + 15*60*1000) > Date.now())){
				res.locals.permission = PROCSTORE.getState().authdb[username].permission;
				PROCSTORE.dispatch({type:'WRONG_PASS_CLEAR', payload: {address:FUNCTIONS.replacer(req.connection.remoteAddress, true)}});
				return resolve(false);
			} else {
				MONGODB.find(res.locals.mongodb.db(CONFIG.mongodb.database).collection('Auth'), {"find":{"username":username}, sort:{"_id": 1}, limit:1, skip:0}).then(function(data){
					if(Array.isArray(data) && (data.length !== 0)){
						PROCSTORE.dispatch({type:'AUTHDB_ADD', payload: {username: data[0].username, password: data[0].password, permission: data[0].permission}});
						if((data[0].username === username) && (data[0].password === password) && (typeof(data[0].permission) === 'object')){
							res.locals.permission = data[0].permission;
							PROCSTORE.dispatch({type:'WRONG_PASS_CLEAR', payload: {address:FUNCTIONS.replacer(req.connection.remoteAddress, true)}});
							return resolve(false);
						} else {
							PROCSTORE.dispatch({type:'WRONG_PASS', payload: {address:FUNCTIONS.replacer(req.connection.remoteAddress, true), bantimeout: CONFIG.bantimeout * 60 * 1000 }});
							return resolve(true);
						}
					} else {
						PROCSTORE.dispatch({type:'AUTHDB_DEL', payload: {username: username}});
						PROCSTORE.dispatch({type:'WRONG_PASS', payload: {address:FUNCTIONS.replacer(req.connection.remoteAddress, true), bantimeout: CONFIG.bantimeout * 60 * 1000 }});
						return resolve(true);
					}
				}).catch(function(err){
					return reject(err);
				});
			}
		}
	})).then(function(val){
		if(val){
			res.append('WWW-Authenticate', 'Basic realm="Secure Area"');
			return res.status(401).json({
				status: "error",
				message: "Permission denied"
			});
		} else {
			return next();
		}
	}).catch(function(err){
		LOGGER.error("IP2BAN-> Ошибка авторизации: "+err.message);
		return res.status(500).json({
			status: "error",
			message: "Internal Server Error"
		});
	});
});

module.exports.test = iptobanTEST;
module.exports.authall = iptobanAuthALL;