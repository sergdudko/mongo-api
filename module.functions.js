/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *	MODULE
 *  FUNCTIONS
 *	Функции приложения
 *
 */

"use strict"

//подгружаемые библиотеки
var CRYPTO = require('crypto'),
	LODASH = require('lodash'),
	HTTP = require('http'),
	HTTPS = require('https'),
	URL = require('url'),
	ZLIB = require('zlib'),
	STREAM = require('stream'),
	MKDIRP = require('mkdirp'),
	PATH = require('path');
	
//подгружаемые модули
var LOGGER = require(PATH.join(__dirname, 'module.logger.js'));

const hrtimeproc = process.hrtime.bigint();
var performance = {
	my:function(){
		try{
			const hrtimeprocnew = process.hrtime.bigint();
			const my = [parseInt(((hrtimeprocnew - hrtimeproc) / 1000000000n).toString()), parseInt(((hrtimeprocnew - hrtimeproc) % 1000000000n).toString())];
			return my;	//вернет массив из 2 аргументов оно же устаревшее process.hrtime()
		} catch(err){
			LOGGER.error(err);
		}
	},
	now:function(){
		try{
			const hrtimeprocnew = process.hrtime.bigint();
			const my = [parseInt(((hrtimeprocnew - hrtimeproc) / 1000000n).toString()), parseInt(((hrtimeprocnew - hrtimeproc) % 1000000n).toString())];
			return parseFloat(my[0]+"."+my[1]);
		} catch(err){
			LOGGER.error(err);
		}
	}
};

//добивает строку пробелами до нужной длинны или обрезает
function correcterString(str, len){
	try{
		let str_t;
		if(typeof(str) !== 'undefined'){
			if(typeof(str) !== 'string'){
				str_t = str.toString();
			} else{
				str_t = str;
			}
		} else {
			str_t = 'undefined';
		}
		if(str_t.length < len){
			for(let i=str_t.length; i < len; i++){
				str_t = str_t + ' ';
			}
		} else {
			str_t = str_t.substr(0, len);
		}
		return str_t;
	} catch(e){
		return str;
	}
}

//функция подсчета хэша
function Hasher(data, alg){
	try{
		if((typeof(alg) !== 'string') || (alg === ''))
			alg = 'sha1';
		const hash = CRYPTO.createHash(alg);
		hash.update(data);
		return(hash.digest('hex'));
	} catch(e){
		return "error";
	}
}

//подготовка к старту (проверка папок)
function PreStart(dirarr){
	return new Promise(function (resolve, reject){
		let Step = [];
		for(let i =0; i < dirarr.length; i++){
			Step.push(new Promise(function (_resolve, _reject){
				let thisdir = PATH.join(__dirname, dirarr[i]);
				MKDIRP(thisdir, function (err) {
					if (err){
						_reject('PRESTARTER-> директория '+thisdir+' не создана по причине: '+ err);
					} else {
						_resolve('ok');
					}
				});
			}));
		}
		Promise.all(Step).then(_resolve =>{resolve(_resolve);}, _reject =>{reject(_reject);});
	});
}

//миллисекунды в минуты
function MsToMin(val){
	try{
		return parseInt((val - (val % 60000)) / 60000);
	}catch(e){
		return 'err';
	}
}

//функция генерации UID
function generateUID() { 
	let d = new Date().getTime();
	if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
		d += performance.now(); 
	}
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		let r = (d + Math.random() * 16) % 16 | 0;
		d = Math.floor(d / 16);
		return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
	});
}

//функция генерации версии
function generateVersion(arg){
	let accuracy = 30;	//количество знаков в версии
	let _performancemy = performance.my();	//получаю время, которое запущен модуль
	let _performancems = _performancemy[0] * (10 ** 3) + _performancemy[1] / (10 ** 6);	//время, которое запущен модуль в мс
	let ProcStartDate = new Date(Date.now() - _performancems);	//дата, когда модуль был запущен
	if(typeof(arg) !== 'undefined'){
		ProcStartDate = new Date((new Date(arg)).getTime()- _performancems); //дата, когда модуль был запущен со сдвигом
	}
	let _dstr = (new Date(ProcStartDate.getTime()  + _performancemy[0] * (10**3))).toJSON().replace(/[-T:.Z]/g, "");	//метка (часть до мс, не используется)
	let version = parseInt(_dstr.substr(0, 14), 10); //метка (часть до секунды)
	let _nsstr = (parseInt(_dstr.substr(14, 3), 10) * (10 ** 6) + _performancemy[1]).toString();	//получаем наносекунды
	while(_nsstr.length < 10){	//теоретически сумма может дать на порядок больше, чтобы не городить огород добиваю нулями до 10 знаков
		_nsstr = "0"+_nsstr;
	}
	version = version.toString(); //преобразую в строку
	version = version + _nsstr;	//метка целиком (время + наносекунды)
	version = version.substr(0, accuracy); //обрезаем метку, если длинна больше заданной
	while(version.length < accuracy){	//добиваем нулями, если длинна метки меньше заданной
		version = version + "0";
	}
	return version;
}

//REST-запрос
/* 
Data:{
	url:"url",
	method:"method",
	data:"data"
	datatype:"string/object/null"
	headers:{"key":"value"}	
}
*/
function RestRequest(data){
	let _data = LODASH.clone(data);
	return new Promise(function(resolve, reject){
		const timeout = 120000;
		let getoptions = URL.parse(_data.url);	//создаем параметры запроса 
		getoptions.method = _data.method;
		getoptions.headers = {};
		getoptions.headers["User-Agent"] = "iRetailCloudServer";
		getoptions.headers["Keep-Alive"] = "120";
		getoptions.headers["Accept-Charset"] = 'utf-8';
		getoptions.headers["Content-type"] = 'application/json';
		getoptions.headers["Accept-Encoding"] = "gzip";
		if(typeof(_data.headers) === 'object'){
			for(const key in _data.headers){
				getoptions.headers[key] = _data.headers[key];
			}
		}
		let req;
		if (URL.parse(_data.url).protocol === null) {	//определяем тип сервера и используемую библиотеку
			req = HTTP;
		} else if (URL.parse(_data.url).protocol === 'https:') {
			req = HTTPS;
		} else {
			req = HTTP;
		}
		let this_request = req.request(getoptions, (response) => {
			let postdata = [];	//массив буфферов результата запроса
			const gunzipper = ZLIB.createGunzip();	//поток декомпрессии
			const Writable = STREAM.Writable();	//поток записи
			Writable._write = function (chunk, enc, next) {	//обработка потока
				postdata.push(chunk);	//пушим буффер в массив
				next();
			};
			const closerErrStream = function(this_data){
				gunzipper.close(); //закрываем gunzip (а то плюется unexpected end of file)
				response.unpipe(Writable); //отвязываем потоки	
				response.destroy(); //уничтожаем потоки
				Writable.destroy();
				if(this_data){
					reject('Ошибка обработки потоков: ' + this_data);
				}
			}
			gunzipper.on("error", function(err){ //обработка ошибок потоков
				closerErrStream(err);
			});
			Writable.on("error", function(err){ 
				closerErrStream(err);
			});
			response.on("error", function(err){ 
				closerErrStream(err);
			});
			Writable.on('finish', () => { 
				gunzipper.close(); //закрываем gunzip (а то плюется unexpected end of file)
				response.unpipe(); //отвязываем потоки
				try{
					resolve([response.statusCode, (Buffer.concat(postdata)).toString('utf8').replace(/[\ufffe\ufeff\uffef\uffbb\uffbf]/g, '')]); //херим спецификацию (нечитаемые символы)
				} catch(err){
					reject('Ошибка преобразования rest-запроса: ' + err);
				}
			});
			switch(response.statusCode){
				case 200:
					if(response.headers['content-encoding'] === 'gzip'){
						response.pipe(gunzipper).pipe(Writable);
					} else {
						response.pipe(Writable);
					}
					break;
				default:
					if((typeof(response.headers['content-type']) === 'string') && (response.headers['content-type'].indexOf("application/json") !== -1)){
						if(response.headers['content-encoding'] === 'gzip'){
							response.pipe(gunzipper).pipe(Writable);
						} else {
							response.pipe(Writable);
						}
					} else{
						resolve([response.statusCode, response.statusMessage]);
						closerErrStream();
					}
					break;
			}
		}); 
		if((typeof(_data.datatype) !== 'undefined') && (typeof(_data.data) !== 'undefined')){
			switch(_data.datatype){
				case 'object':
					this_request.write(JSON.stringify(_data.data));	//отправка post-данных
					break;
				case 'string':
					this_request.write(_data.data);	//отправка post-данных
					break;
			}
		}
		this_request.on('error', function (e) {	//обработка ошибок
			reject('Ошибка rest-запроса:'+e);
		});
		this_request.on('timeout', function () {	//обработка таймаута
			this_request.abort();
			reject('Таймаут rest-запроса!');
		});
		this_request.setTimeout(timeout);	//таймаут соединения
		this_request.end();
	});
}

//преобразователь FHIR к более человеческому виду
function fhirParser(data){
	return new Promise(function (resolve){
		let _data = {}, iter = 1, flag = 1;
		let parser = function(this_data, n, type){
			this_data.entry.forEach(function(x) {
				let _n;
				if(typeof(n) !== 'string'){
					_n = x.resource.resourceType;
				} else {
					_n = n+'.'+x.resource.resourceType;
				}
				switch(x.resource.resourceType){
					case "Bundle":
						parser(LODASH.clone(x.resource), _n, x.resource.resourceType);
						iter++;
						break;
					default:
						break;
				}
				if(x.resource.resourceType !== 'Bundle'){
					if(type === 'Bundle'){
						if(typeof(_data[_n]) !== 'object'){
							_data[_n] ={};
						}
						_data[_n][x.resource.id] = x.resource;
					} else {
						_data[_n] = LODASH.clone(x.resource);
					}
				}
			});
			if(flag === iter){
				resolve(_data);
			} else {
				flag++;
			}
		}
		parser(LODASH.clone(data));
	});
}

//функция замены "." на "_" и обратно
function replacer(data_val, value_val){
	try {
		if(typeof(data_val) === 'string'){
			if(value_val){
				return data_val.replace(/\./gi,"_");
			} else {
				return data_val.replace(/_/gi,".");
			}
		} else {
			return 'undefined';
		}
	} catch(e) {
		LOGGER.warn("REPLACE LODASH/DOT ERROR: "+e.message);
	}	
}

module.exports.correcterstr = correcterString;
module.exports.hasher = Hasher;
module.exports.prestart = PreStart;
module.exports.mstomin = MsToMin;
module.exports.uid = generateUID;
module.exports.version = generateVersion;
module.exports.rest = RestRequest;
module.exports.fhirparser = fhirParser;
module.exports.replacer = replacer;