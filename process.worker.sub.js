/**
 *	iRetailCloudServer
 *	(c) 2018 by Siarhei Dudko.
 *
 *  PROCESS
 *  SUB-WORKER
 *  Экземпляр процесса саб-воркера, занимается обслуживанием приложения. Например, отправкой email из очереди.
 *
 */

"use strict"

//подгружаемые библиотеки
var CONFIG = require('config'),
	FS = require('fs'),
	PATH = require('path');
	
//подгружаемые модули
var SENDMAIL = require(PATH.join(__dirname, 'module.sendmail.js')),
	LOGGER = require(PATH.join(__dirname, 'module.logger.js')),
	PROCSTORE = require(PATH.join(__dirname, 'procstore.js')),
	MONGODB = require(PATH.join(__dirname, 'module.mongodb.js')),
	EHR = require(PATH.join(__dirname, 'module.ehr.js'));
	
SENDMAIL.worker();
startSubWorker();

function startSubWorker(){
	MONGODB.client().then(function(client){
		if(CONFIG.primary && CONFIG.ehr && CONFIG.ehr.server){
			EHR.worker(client);
		}
		let GarbageCollector = function(){
			if(CONFIG.primary){
				//чистим коллекцию Request от устаревших хэшей
				try{
					client.db(CONFIG.mongodb.database).collection('Request').aggregate([{
						$match:{type:"hash"}
					},
					{
						$project:{
							difference:{$subtract:[
								Date.now(),
								{$add:["$timestamp","$timeout"]}
							]},
							remove: []
						}
					},
					{
						$match:{difference:{"$gt":86400000}}	//чищу хэши через сутки (чтобы индексы не росли)
					},
					{
						$group:{_id:"removable", remove:{$push:"$_id"}}
					}
					],
					{ allowDiskUse: true }).forEach(function(doc){
						client.db(CONFIG.mongodb.database).collection('Request').deleteMany({_id : {$in: doc.remove}});
					});
				} catch(e){
					LOGGER.error("GC->Ошибка очистки устаревших хэшей:" + e);
				}
				//затираем старые запросы (31+ дней)
				try{
					client.db(CONFIG.mongodb.database).collection('Request').updateMany({type:"EHRRequest", timestamp: {"$lt": (Date.now()-31*24*60*60*1000)}, body:{"$exists": true}, status:{"$in":["completed", "canceled"]}}, {$unset: {body: true}});
				} catch(e){
					LOGGER.error("GC->Ошибка обрезки EHR-запросов:" + e);
				}
			}
			//чистим устаревшие файловые кэши
			FS.readdir(CONFIG.cachedir, function(err, items) {
				try{
					if (err) {
						throw err;
					} else {				
						let summ = 0;
						for (let i=0; i<items.length; i++) {
							const filename = items[i];
							MONGODB.find(client.db(CONFIG.mongodb.database).collection('Request'), {"find":{"type":"hash", "hash":filename+'-'+CONFIG.servername}, sort:{"_id": 1}, limit:1, skip:0}).then(function(data){
								let unlink = true;
								if(data.length !== 0){
									if(((Date.now() - data[0].timeout) < data[0].timestamp) && ((((new Date(data[0].timestamp)).getHours() > 6) && ((new Date(data[0].timestamp)).getDay() === (new Date(Date.now())).getDay())) || ((new Date(Date.now())).getHours() < 6))){ //(проверяем что хэш не истек и час старше 6 (примерный период обменов) или текущее время меньше 6)
										unlink = false;
									}
								}
								if(PROCSTORE.getState().tasks.indexOf(filename) !== -1){
									unlink = false;
								}
								if(unlink){
									FS.unlink(PATH.join(CONFIG.cachedir, filename), (err) => {
										try{
											if (err) {
												throw err;
											}
										} catch(e){
											LOGGER.error("GC->Ошибка обработки сборщиком мусора файла: "  + filename);
										}
									});
								}
							}).catch(function(error){
								LOGGER.error("GC->Ошибка поискового запроса в mongodb: "  + error);
							}).finally(function(){
								summ++;
								if(summ === items.length){
									LOGGER.log('GC->очистка выполнена!');
								}
							});
						}
					}
				} catch(e){
					LOGGER.error("GC->Ошибка чтения сборщиком мусора директории с файлами: "  + e);
				}
			});	
		}
		
		GarbageCollector();
		setInterval(GarbageCollector, 3600000); //запускаю раз в час сборку мусора
	}).catch(function(err){
		LOGGER.error('GC-> Ошибка подключения к mongodb (сл.попытка через 30 сек): '+err);
		setTimeout(startSubWorker, 30000);
	});
}