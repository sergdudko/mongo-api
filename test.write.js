var FUNCTIONS = require(__dirname+'/module.functions.js'),
	LODASH = require('lodash');

var check = {
    "guid" : "e23c022e-ef06-11e2-af2d-782bcb1ff45d_1374073", 
    "type" : "Документ.ТЕСТ", 
    "date" : "2014-05-02T00:00:00", 
    "description" : "e23c022e-ef06-11e2-af2d-782bcb1ff45d", 
    "isactive" : true, 
    "requisites" : [
        {
            "type" : "Строка", 
            "id" : "ParentID", 
            "name" : "Идентификатор  организации", 
            "value" : "e23c0228-ef06-11e2-af2d-782bcb1ff45d"
        }, 
        {
            "type" : "Строка", 
            "id" : "PersonalID", 
            "name" : "Идентификатор  торгового объекта", 
            "value" : "e23c022e-ef06-11e2-af2d-782bcb1ff45d"
        }, 
        {
            "type" : "Булево", 
            "id" : "IsRetail", 
            "name" : "Признак розничного склада", 
            "value" : true
        }, 
        {
            "type" : "Число", 
            "id" : "Warehouse_ID", 
            "name" : "Идентификатор  склада", 
            "value" : 1
        }, 
        {
            "type" : "Строка", 
            "id" : "Remain_ID", 
            "name" : "Идентификатор партии", 
            "value" : "1374073"
        }, 
        {
            "type" : "Строка", 
            "id" : "Good_name", 
            "name" : "Наименование товара", 
            "value" : "Палочки ватные JOHNSON`S BABY №100"
        }, 
        {
            "type" : "Строка", 
            "id" : "Remain_Character", 
            "name" : "Описание (признак рецептурного отпуска)", 
            "value" : ""
        }, 
        {
            "type" : "Строка", 
            "id" : "Producer_name", 
            "name" : "Страна производства (производитель)", 
            "value" : "Италия (Johnson & Johnson S.p.A.)"
        }, 
        {
            "type" : "Строка", 
            "id" : "Seria", 
            "name" : "Серия товара", 
            "value" : ""
        }, 
        {
            "type" : "Дата", 
            "id" : "ExpireDate", 
            "name" : "Срок годности", 
            "value" : "2049-01-01T00:00:00"
        }, 
        {
            "type" : "Число", 
            "id" : "QtyParts", 
            "name" : "Количество дробных частей товара (в остатке, целое)", 
            "value" : 2
        }, 
        {
            "type" : "Число", 
            "id" : "PartsInGood", 
            "name" : "Делимость товара на части (целое)", 
            "value" : 1
        }, 
        {
            "type" : "Число", 
            "id" : "Price", 
            "name" : "Цена продажи товара (розничная для ТО)", 
            "value" : 2.73
        }, 
        {
            "type" : "Число", 
            "id" : "MinPrice", 
            "name" : "Минимальная цена продажи", 
            "value" : 2.15
        }, 
        {
            "type" : "Строка", 
            "id" : "BarCode", 
            "name" : "Штрих-код остатка", 
            "value" : "8002110313461"
        }, 
        {
            "type" : "Число", 
            "id" : "Color", 
            "name" : "Цвет для отображения на кассе", 
            "value" : 0
        }, 
        {
            "type" : "Строка", 
            "id" : "Good_ID", 
            "name" : "Идентификатор товара в учетной системе", 
            "value" : "4dd2abd7-ef09-11e2-af2d-782bcb1ff45d"
        }, 
        {
            "type" : "Строка", 
            "id" : "TradeGroup", 
            "name" : "Товарная группа", 
            "value" : "Материнство и детство"
        }, 
        {
            "type" : "Строка", 
            "id" : "TradeSubGroup", 
            "name" : "Товарная подгруппа", 
            "value" : "Ватные палочки детские"
        }, 
        {
            "type" : "Строка", 
            "id" : "TradeMark", 
            "name" : "Торговая марка", 
            "value" : "Johnson`s"
        }, 
        {
            "type" : "Строка", 
            "id" : "TradeLine", 
            "name" : "Торговая линия", 
            "value" : "Johnson`s Baby"
        }, 
        {
            "type" : "Строка", 
            "id" : "Producer_ID", 
            "name" : "Идентификатор производителя", 
            "value" : "4dd2abc3-ef09-11e2-af2d-782bcb1ff45d"
        }, 
        {
            "type" : "Булево", 
            "id" : "MustHaveInApt", 
            "name" : "Признак обязательного перечня", 
            "value" : false
        }, 
        {
            "type" : "Булево", 
            "id" : "MustHaveBelInApt", 
            "name" : "Признак обязательного белорусского перечня", 
            "value" : false
        }, 
        {
            "type" : "Строка", 
            "id" : "Group_ID", 
            "name" : "Идентификатор группы", 
            "value" : "37"
        }, 
        {
            "type" : "Строка", 
            "id" : "Nds_rate", 
            "name" : "Ставка НДС (строкой)", 
            "value" : "20"
        }, 
        {
            "type" : "Дата", 
            "id" : "IncomeInvoiceDate", 
            "name" : "Дата документа поступления", 
            "value" : "2014-05-02T00:00:00"
        }, 
        {
            "type" : "Число", 
            "id" : "BPrice", 
            "name" : "Себестоимость товара с НДС", 
            "value" : 2.04
        }, 
        {
            "type" : "Строка", 
            "id" : "Supply_NO", 
            "name" : "Код партии товара", 
            "value" : "1327782"
        }, 
        {
            "type" : "Строка", 
            "id" : "S_Ext_ID", 
            "name" : "Идентификатор партии для сегмента с ограничениями по партиям", 
            "value" : "91359FF7-CEA0-11E3-B161-782BCB1FF45D"
        }
    ], 
    "version" : "201908160512011834611461000000"
};

//начальные данные
var url = 'http://test:test@10.0.8.1:9999';
var testcount = 1;
var count = 0;
function starter(){
	let dt1 = Date.now();
	for(var i = 0; i < testcount; i++){
		var checkArr = [];
		for(var j = 0; j<10000; j++){
			var newcheck = LODASH.clone(check);
			newcheck.guid = FUNCTIONS.uid();
			checkArr.push(newcheck);
		}
		FUNCTIONS.rest({
			url:url+'/v3/data',
			method:"POST",
			data:JSON.stringify(checkArr),
			datatype:"string"
		}).then(function(resolve){
			if(resolve[0] === 200) count++;
			console.log('STATUS: '+resolve[0]+', ANSWER: '+ resolve[1]+', '+(Date.now() - dt1)+'ms');
		}).catch(function(err){
			console.log(''+err);
		}).finally(function(){
			if(i === 100){
				console.log('GOOD REQUEST: '+count);
			}
			starter();
		});
		checkArr = null;
	}
}

starter();


